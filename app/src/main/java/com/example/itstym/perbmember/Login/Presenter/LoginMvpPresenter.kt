package com.example.itstym.perbmember.Login.Presenter

import com.example.itstym.perbmember.Base.Activity.Presenter.MvpPresenter
import com.example.itstym.perbmember.Login.View.LoginMvpView

/**
 * Created by itstym on 6/12/17.
 */


interface LoginMvpPresenter<in V: LoginMvpView> : MvpPresenter<V> {

    fun openGmail(email: String, subject: String)

    fun makeACall(phoneNo: String)

}