package com.example.itstym.perbmember.Utils

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.net.ConnectivityManager
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.Login.AskOtpDialog.AskOtpDialog
import com.example.itstym.perbmember.Network.Model.CommonApiResponse
import com.example.itstym.perbmember.Network.Model.Member
import com.example.itstym.perbmember.Network.RetrofitHelper.ApiUtils
import com.example.itstym.perbmember.R
import com.squareup.picasso.Picasso
import me.zhanghai.android.materialprogressbar.MaterialProgressBar
import retrofit2.Call
import retrofit2.Response
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by itstym on 6/12/17.
 */

class HelperUtils{

    lateinit var progressbar: ProgressBar

    //lateinit var dismiss:AskPhoneView



    companion object {

        fun isInternetConnectionAvaliable(context: Context?): Boolean {
            val cm = context!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            return activeNetwork != null && activeNetwork.isConnectedOrConnecting
        }

        fun showSnackBar(message: String, activity: Activity) {
            val snackbar = Snackbar.make(activity.findViewById(android.R.id.content),
                    message, Snackbar.LENGTH_LONG)

            val sbView = snackbar.view
            sbView.setBackgroundColor(ContextCompat.getColor(activity as Context, android.R.color.holo_red_dark))
            val textView = sbView
                    .findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
            textView.setTextColor(ContextCompat.getColor(activity, android.R.color.white))
            snackbar.show()
        }

        fun showDataSendingDialog(context: Context, titleText: String): AlertDialog {

            val dialogBuilder = AlertDialog.Builder(context)
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val dialogView = inflater.inflate(R.layout.progress_layout, null)

            val title = dialogView.findViewById<TextView>(R.id.title)

            val progressbar = dialogView.findViewById<ProgressBar>(R.id.progressBar)

            title.text = titleText

            dialogBuilder.setView(dialogView)


            dialogBuilder.setCancelable(false)

            val alertDialog = dialogBuilder.create()
            val lp = WindowManager.LayoutParams()
            lp.copyFrom(alertDialog.getWindow().getAttributes())
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT
            alertDialog.getWindow().setAttributes(lp)


            alertDialog.show()

            return alertDialog
        }

        fun closeDialog(alertDialog: Dialog?) {
            if (alertDialog != null && alertDialog.isShowing)
                alertDialog.dismiss()
        }

        lateinit var loggingButNotVerifiedMemberDetails: Member

        fun sendOTP(phone: String, context: Context, dataManager: DataManager, callingFromOthers: Boolean, progressBarLogin: MaterialProgressBar) {

           // val alertDialog = showDataSendingDialog(context, "Sending OTP... ")

            val mApiService = ApiUtils.getAPIService()

            mApiService.sendOTP(phone).enqueue(object : retrofit2.Callback<CommonApiResponse> {
                override fun onFailure(call: Call<CommonApiResponse>?, t: Throwable?) {

                    //HelperUtils.closeDialog(alertDialog)
                    HelperUtils.showSnackBar("Network Issue", context as Activity)
                }

                override fun onResponse(call: Call<CommonApiResponse>?, response: Response<CommonApiResponse>?) {

                  //  HelperUtils.closeDialog(alertDialog)

                    if (response!!.isSuccessful) {

                        if (response.body()!!.status){

                            if(!callingFromOthers){
                                Log.e("Helper ","open data")
                                progressBarLogin.visibility = View.INVISIBLE
                                //open otp verify Screen
                                val askOTPDialog= AskOtpDialog(context, android.R.style.Theme_Material_Light_NoActionBar_Fullscreen, dataManager,phone)
                                askOTPDialog.setOnDismissListener(){


                                }
                                askOTPDialog.show()
                            }

                            //verifyOTPScreen(phone,context)
                        }else{

                        }
                    } else {
                        HelperUtils.showSnackBar("Network issue while sending otp", context as Activity)
                    }
                }
            })

        }

        fun loadImage(imageIcon: Int, context: Context, imageView: ImageView) {

            Picasso.with(context).load(imageIcon).into(imageView)
        }

        fun loadImage(imageUrl: String, context: Context, imageView: ImageView) {

            Picasso.with(context).load(imageUrl).into(imageView)
        }

        fun getDateInFormat(timeInMillis: Long, format: String): String {
            val formatter = SimpleDateFormat(format)

            // Create a calendar object that will convert the date and time value in milliseconds to date.
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = timeInMillis
            calendar.timeZone = TimeZone.getTimeZone("Asia/Calcutta")
            return formatter.format(calendar.getTime())
        }

        fun getDayOfMonthSuffix(n: Int): String {
            if (n in 11..13) {
                return "th"
            }
            return when (n % 10) {
                1 -> "st"
                2 -> "nd"
                3 -> "rd"
                else -> "th"
            }
        }


        fun getTimeInMills(dateInString:String, format: String): Long{

            val simpleDateFormat= SimpleDateFormat(format)

            val dateInRequiredFormat=simpleDateFormat.parse(dateInString)

            val calender=Calendar.getInstance()
            calender.time= dateInRequiredFormat
            calender.timeZone = TimeZone.getTimeZone("Asia/Calcutta")

            return calender.timeInMillis
        }

        fun isItTodayDate(waterDate: String, todayDate: Long): Boolean {

            val logsDate=getTimeInMills(waterDate.split("T")[0],"yyyy-MM-dd")

            val logsCalender= Calendar.getInstance()
            logsCalender.timeInMillis=logsDate

            val todayCalender= Calendar.getInstance()
            todayCalender.timeInMillis=todayDate

            return logsCalender.get(Calendar.DAY_OF_MONTH)== todayCalender.get(Calendar.DAY_OF_MONTH) && logsCalender.get(Calendar.MONTH)== todayCalender.get(Calendar.MONTH) &&logsCalender.get(Calendar.YEAR)== todayCalender.get(Calendar.YEAR)
        }


        fun getGenderById(gender_id:Int):String{

            return when(gender_id){

                1 -> "Male"

                2 -> "Female"

                else -> {
                    "Others"
                }
            }
        }

        fun getTodayDay(): Int {

            val todayDateCalender=Calendar.getInstance()
            return todayDateCalender.get(Calendar.DAY_OF_WEEK)

        }

        fun getWeekNo(date:String): Boolean{

           // var date1 = Calendar.getInstance().timeInMillis = date
            val todayCalender= Calendar.getInstance()
            var currentweek= todayCalender.get(Calendar.WEEK_OF_YEAR)
            todayCalender.timeInMillis = HelperUtils.getTimeInMills(date,"yyyy-mm-dd")

            return todayCalender.get(Calendar.WEEK_OF_YEAR)==currentweek
        }



        @Throws(InterruptedException::class, IOException::class)
        fun isConnected(): Boolean {
            val command = "ping -c 1 google.com"
            return Runtime.getRuntime().exec(command).waitFor() == 0
        }


    }


}