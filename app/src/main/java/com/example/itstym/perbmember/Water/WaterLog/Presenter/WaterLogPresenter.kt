package com.example.itstym.perbmember.Water.WaterLog.Presenter

import android.app.Activity
import android.content.Context
import android.util.Log
import android.widget.RelativeLayout
import android.widget.Toast
import com.example.itstym.perbmember.Base.Activity.Presenter.BasePresenter
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.Network.Model.CommonApiResponse
import com.example.itstym.perbmember.Network.Model.Water
import com.example.itstym.perbmember.Network.Model.WaterResponse
import com.example.itstym.perbmember.Network.RetrofitHelper.ApiUtils
import com.example.itstym.perbmember.Utils.HelperUtils
import com.example.itstym.perbmember.Water.WaterLog.View.WaterLogView
import retrofit2.Call
import retrofit2.Response
import java.util.*

/**
 * Created by itstym on 10/12/17.
 */


class WaterLogPresenter<V : WaterLogView>(dataManager: DataManager) : BasePresenter<V>(dataManager), WaterLogMvpPresenter<V> {


    override fun updateWaterLog(context: Context, numWaterGlass: Int, plusRelativeLayout: RelativeLayout) {

        //val alertDialog = HelperUtils.showDataSendingDialog(context, "Fetching Water Logs... ")

        val mApiService = ApiUtils.getAPIService()

        mApiService.waterLogs(dataManager.getMemberId(),numWaterGlass).enqueue(object : retrofit2.Callback<CommonApiResponse> {
            override fun onFailure(call: Call<CommonApiResponse>?, t: Throwable?) {

              //  HelperUtils.closeDialog(alertDialog)
                HelperUtils.showSnackBar("Network Issue", context as Activity)
            }

            override fun onResponse(call: Call<CommonApiResponse>?, response: Response<CommonApiResponse>?) {

               // HelperUtils.closeDialog(alertDialog)

                if (response!!.isSuccessful) {

                    Log.e(" OTP Response body ",response.body()!!.status.toString())

                    if(response.body()!!.status){
                        //otp match go to main activity

                        //update the view
                        plusRelativeLayout.isEnabled = true
                        getWaterLogs(context)
                        mvpView?.updateLog()

                    }else{
                        HelperUtils.showSnackBar("OTP NOT MATCHED", context as Activity)
                        plusRelativeLayout.isEnabled = true

                    }


                } else {
                    HelperUtils.showSnackBar("Network issue while verifying otp", context as Activity)
                    plusRelativeLayout.isEnabled = true

                }
            }
        })
    }


    override fun getWaterLogs(context: Context) {

       // val alertDialog = HelperUtils.showDataSendingDialog(context, "Fetching Water Logs... ")

        val mApiService = ApiUtils.getAPIService()

        mApiService.getAllWaterLogs(dataManager.getMemberId()).enqueue(object : retrofit2.Callback<WaterResponse> {
            override fun onFailure(call: Call<WaterResponse>?, t: Throwable?) {

               // HelperUtils.closeDialog(alertDialog)
                if (HelperUtils.isConnected()) {
                    Toast.makeText(context as Activity, "Network Issue", Toast.LENGTH_SHORT).show()
                    //  HelperUtils.showSnackBar("Network Issue", context as Activity)}
                }
                else {
                    Toast.makeText(context as Activity, "Not Connected to Internet", Toast.LENGTH_SHORT).show()

                    //      HelperUtils.showSnackBar("Not Connected to Internet", context as Activity)}
                }


            }

            override fun onResponse(call: Call<WaterResponse>?, response: Response<WaterResponse>?) {

               // HelperUtils.closeDialog(alertDialog)

                if (response!!.isSuccessful) {

                    Log.e(" OTP Response body ",response.body()!!.waterArrayList.toString()+"ohusun")

                    if(response.body()!!.status){
                        //otp match go to main activity
                        //update the view
                        val todayWaterGlass=getTodayWaterLogs(response.body()!!.waterArrayList)

                        Log.e("Total water Glass",todayWaterGlass.toString() +"hello")
                        mvpView?.updateView(todayWaterGlass)

                    }else{
                        HelperUtils.showSnackBar("OTP NOT MATCHED", context as Activity)
                    }


                } else {
                    HelperUtils.showSnackBar("Network issue while verifying otp", context as Activity)
                }
            }
        })

    }

    private fun getTodayWaterLogs(waterArrayList: ArrayList<Water>): Int {

        val calender=Calendar.getInstance()

        val numWaterGlass= (0 until waterArrayList.size)
                .firstOrNull { HelperUtils.isItTodayDate(waterArrayList[it].waterDate,calender.timeInMillis) }
                ?.let { waterArrayList[it].numWaterGlass }
                ?: 0

        return numWaterGlass

    }


}