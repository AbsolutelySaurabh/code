package com.example.itstym.perbmember.Network.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by mukul on 12/19/17.
 */

data class Weight(
        @SerializedName("weight_intake_date")
        @Expose
        val weightIntakeDate:String,

        @SerializedName("weight_id")
        @Expose
        val weight_id:Int,

        @SerializedName("weight")
        @Expose
        val weight:Int
)
