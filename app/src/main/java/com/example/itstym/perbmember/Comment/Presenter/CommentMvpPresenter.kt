package com.example.itstym.perbmember.Comment.Presenter

import android.content.Context
import android.support.v7.widget.RecyclerView
import com.example.itstym.perbmember.Base.Activity.Presenter.MvpPresenter
import com.example.itstym.perbmember.Comment.View.CommentView
import com.example.itstym.perbmember.Feed.View.FeedView

/**
 * Created by itstym on 8/12/17.
 */

interface CommentMvpPresenter<in V: CommentView> : MvpPresenter<V> {

    fun getAllPreviousComment(feedRecyclerView: RecyclerView, context: Context, blog_id:Int?)

    fun sendComment(member_id:Int, blog_id: Int, comment:String,context: Context)

}