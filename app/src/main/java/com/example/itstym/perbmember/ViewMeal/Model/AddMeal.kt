package com.example.itstym.perbmember.ViewMeal.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by mukul on 12/30/17.
 */
data class AddMeal(
        @SerializedName("member_id")
        @Expose
        val memberId:Int,

        @SerializedName("is_global_meal")
        @Expose
        val globalMeal:Int,


        @SerializedName("diet_plan_id")
        @Expose
        val dietPlanId:Int,

        @SerializedName("days")
        @Expose
        val days:ArrayList<Int>




)