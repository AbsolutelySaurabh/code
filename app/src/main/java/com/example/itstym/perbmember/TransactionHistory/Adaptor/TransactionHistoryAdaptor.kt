package com.example.itstym.perbmember.TransactionHistory.Adaptor

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.itstym.perbmember.Network.Model.Transaction
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.Utils.HelperUtils
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.transaction_item.view.*
import java.util.*

/**
 * Created by itstym on 11/12/17.
 */


class TransactionHistoryAdaptor(var context: Context, var trancsactionHistoryArrayList:ArrayList<Transaction>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int {
        return trancsactionHistoryArrayList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        // Log.i("Adaptor viewtype ", Integer.toString(viewType))

        val v = LayoutInflater.from(context).inflate(R.layout.transaction_item, parent, false)
        return Item(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {

        (holder as Item).bindData(position,context,trancsactionHistoryArrayList[position])

    }

}

class Item(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindData(position: Int, context: Context, transaction: Transaction) {

            itemView.amountPaid.text=transaction.paidAmount.toString()
            itemView.modeOfPayment.text=transaction.paymentMode
            itemView.trainerName.text=transaction.trainerName
            itemView.planName.text=transaction.planName

            val payment_date=transaction.dateOfPayment.split("T")[0]
            val paymentDateInLong=HelperUtils.getTimeInMills(payment_date,"yyyy-MM-dd")

        val paymentCalender= Calendar.getInstance()
         paymentCalender.timeInMillis= paymentDateInLong

        val dateOnly= (paymentCalender.get(Calendar.DAY_OF_MONTH) ).toString()+HelperUtils.getDayOfMonthSuffix( paymentCalender.get(Calendar.DAY_OF_MONTH))

        val paymentDateInFormat=dateOnly+" "+HelperUtils.getDateInFormat(paymentCalender.timeInMillis,"MMMM, yyyy")

        itemView.paymentDate.text=paymentDateInFormat
    }


}
