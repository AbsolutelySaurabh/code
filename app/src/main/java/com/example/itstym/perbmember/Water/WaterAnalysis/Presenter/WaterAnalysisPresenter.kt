package com.example.itstym.perbmember.Water.WaterAnalysis.Presenter

import android.app.Activity
import android.content.Context
import android.util.Log
import com.example.itstym.perbmember.Base.Activity.Presenter.BasePresenter
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.Network.Model.Water
import com.example.itstym.perbmember.Network.Model.WaterResponse
import com.example.itstym.perbmember.Network.RetrofitHelper.ApiUtils
import com.example.itstym.perbmember.Utils.HelperUtils
import com.example.itstym.perbmember.Water.WaterAnalysis.View.WaterAnalysisView
import retrofit2.Call
import retrofit2.Response
import java.util.*

/**
 * Created by itstym on 10/12/17.
 */


class WaterAnalysisPresenter<V : WaterAnalysisView>(dataManager: DataManager) : BasePresenter<V>(dataManager), WaterAnalysisMvpPresenter<V> {




    override fun getWaterLogs(context: Context) {

     //   val alertDialog = HelperUtils.showDataSendingDialog(context, "Fetching Water Logs... ")

        val mApiService = ApiUtils.getAPIService()

        mApiService.getAllWaterLogs(dataManager.getMemberId()).enqueue(object : retrofit2.Callback<WaterResponse> {
            override fun onFailure(call: Call<WaterResponse>?, t: Throwable?) {

             //   HelperUtils.closeDialog(alertDialog)
                HelperUtils.showSnackBar("Network Issue", context as Activity)
            }

            override fun onResponse(call: Call<WaterResponse>?, response: Response<WaterResponse>?) {

               // HelperUtils.closeDialog(alertDialog)

                if (response!!.isSuccessful) {

                    Log.e(" OTP Response body ",response.body()!!.status.toString())

                    if(response.body()!!.status){
                        //otp match go to main activity
                        //update the view
                        val weekWaterData=arrageTheData(response.body()!!.waterArrayList)

                        Log.e("Water Log",weekWaterData.values.toString())
                        mvpView?.updateData(weekWaterData)

                    }else{
                        HelperUtils.showSnackBar("OTP NOT MATCHED", context as Activity)
                    }


                } else {
                    HelperUtils.showSnackBar("Network issue while verifying otp", context as Activity)
                }
            }
        })

    }

    private fun arrageTheData(waterArrayList: ArrayList<Water>):  HashMap<Int, Water> {

        val waterLogs:HashMap<Int, Water> = HashMap()

        for (i in 0 until waterArrayList.size){

            val key=getKey(waterArrayList[i].waterDate)
            Log.e("Key is ",key.toString())
            waterLogs.put(key,waterArrayList[i])
        }

        return waterLogs
    }

    private fun getKey(waterDate: String):Int {

        val calender=Calendar.getInstance()

        val waterLogsDate=HelperUtils.getTimeInMills(waterDate.split("T")[0],"yyyy-MM-dd")

        calender.timeInMillis=waterLogsDate

        return calender.get(Calendar.DAY_OF_WEEK)-1
    }


}