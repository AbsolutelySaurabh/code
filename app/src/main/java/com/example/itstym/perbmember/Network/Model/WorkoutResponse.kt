package com.example.itstym.perbmember.Network.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by itstym on 6/12/17.
 */

data class WorkoutResponse(
        @SerializedName("status")
        @Expose
        val status:Boolean,

        @SerializedName("message")
        @Expose
        val message:String,


        @SerializedName("workout")
        @Expose
        val workout: ArrayList<Workout>

)