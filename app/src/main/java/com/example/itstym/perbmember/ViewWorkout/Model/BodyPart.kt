package com.example.itstym.perbmember.ViewWorkout.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by itstym on 7/12/17.
 */

data class BodyPart(

        @SerializedName("body_part_id")
        @Expose
        val body_part_id:Int,

        @SerializedName("body_part_name")
        @Expose
        val body_part_name:String,

        @SerializedName("body_part_url")
        @Expose
        val body_part_url:String,

        @SerializedName("selected")
        @Expose
        val selected:Boolean,

        @SerializedName("selected_days")
        @Expose
        val selected_days:ArrayList<Int>



)