package com.example.itstym.perbmember.Workout.Adaptor

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.itstym.perbmember.R
import kotlinx.android.synthetic.main.day_item.view.*

/**
 * Created by itstym on 7/12/17.
 */


class DaysBottomSheetAdapter(var context: Context, var allDaysList: ArrayList<String>, var daysSelected: DaysSelected, var selectedDay: Int): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    interface DaysSelected{

        fun onDaysSelected(position: Int)
    }

    override fun getItemCount(): Int {
        return allDaysList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        // Log.i("Adaptor viewtype ", Integer.toString(viewType))

        val v = LayoutInflater.from(context).inflate(R.layout.day_item, parent, false)
        return ItemBottom(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {

        (holder as ItemBottom).bindData(allDaysList.get(position), context,daysSelected, selectedDay)

    }

}

class ItemBottom(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindData(bottomItem: String, context: Context, daysSelected: DaysBottomSheetAdapter.DaysSelected, selectedDay: Int) {


        //set the name
        itemView.dayName.text=bottomItem

        itemView.relativeLayout.isSelected=false

        //we are working upon this day
        if (adapterPosition==(selectedDay-1)){
            itemView.relativeLayout.isSelected=true
            itemView.dayName.setTextColor(ContextCompat.getColor(context, android.R.color.white))
        }


        //on click listener
        itemView.setOnClickListener {

           if (itemView.relativeLayout.isSelected){
               Log.e("item","unselected")
               itemView.relativeLayout.isSelected=false
               itemView.dayName.setTextColor(ContextCompat.getColor(context, R.color.newColorIntroducedDesigner))
           }else{
               Log.e("Item ","selected")
               itemView.relativeLayout.isSelected=true
               itemView.dayName.setTextColor(ContextCompat.getColor(context, android.R.color.white))
           }
           daysSelected.onDaysSelected(adapterPosition)
        }

    }


}
