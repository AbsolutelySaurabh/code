package com.example.itstym.perbmember.Weight.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.itstym.perbmember.R

/**
 * Created by itstym on 11/12/17.
 */


class WeighingScaleAdaptor(var context: Context, var weightChangeListener:WeighChangeListener): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    interface WeighChangeListener {

        fun onWeightChanged(weight:Int)
    }
    override fun getItemCount(): Int {
        return 20
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        // Log.i("Adaptor viewtype ", Integer.toString(viewType))

        val v = LayoutInflater.from(context).inflate(R.layout.weight_scale_item, parent, false)
        return Item(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {

        (holder as Item).bindData(context,weightChangeListener)

    }

}

class Item(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val TAG="WeighingScaleAdaptor"

    fun bindData(context: Context, weightChangeListener: WeighingScaleAdaptor.WeighChangeListener) {

            Log.e(TAG,"adapter position"+adapterPosition)
            Log.e(TAG,"layout position"+layoutPosition)
            Log.e(TAG,"old position"+oldPosition)

            val totalWeightUserHasSeen=30 +(adapterPosition*5)-5
            val actualWeight=(totalWeightUserHasSeen-15)
            Log.e("weight mark ",(totalWeightUserHasSeen-15).toString())

//            itemView.bitsRecyclerview.layoutManager=LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
//            itemView.bitsRecyclerview.adapter=WeighingScaleBitsScaleAdapetor(context)
//            itemView.weight.text=(5*adapterPosition+30).toString()

          weightChangeListener.onWeightChanged(actualWeight)

    }


}
