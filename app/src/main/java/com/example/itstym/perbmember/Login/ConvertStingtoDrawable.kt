package com.example.itstym.perbmember.Login

import android.graphics.*
import android.graphics.drawable.Drawable

/**
 * Created by mukul on 12/27/17.
 */


class ConvertStingtoDrawable(private val text: String) : Drawable() {
    private val paint: Paint

    init {
        this.paint = Paint()
        paint.color = Color.BLACK
        paint.textSize = 16f
        paint.isAntiAlias = true
        paint.textAlign = Paint.Align.LEFT
    }

    override fun draw(canvas: Canvas) {
        canvas.drawText(text, 0f, 6f, paint)
    }

    override fun setAlpha(alpha: Int) {
        paint.alpha = alpha
    }

    override fun setColorFilter(cf: ColorFilter?) {
        paint.colorFilter = cf
    }

    override fun getOpacity(): Int {
        return PixelFormat.TRANSLUCENT
    }
}