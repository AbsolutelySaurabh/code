package com.example.itstym.perbmember.Workout.Adaptor

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.Workout.Model.Exercise
import kotlinx.android.synthetic.main.exerciseshimmer.view.*

/**
 * Created by itstym on 6/12/17.
 */


class ExerciseAdapterShimmer(var context: Context, var exerciseData:ArrayList<Exercise>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {



    override fun getItemCount(): Int {
        return exerciseData.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        // Log.i("Adaptor viewtype ", Integer.toString(viewType))

        val v = LayoutInflater.from(context).inflate(R.layout.exerciseshimmer, parent, false)
        return ItemCombatShim(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {


            (holder as ItemCombatShim).bindData(context,exerciseData.get(position),exerciseData.size)
    }

}

class ItemCombatShim(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindData( context: Context,exercise: Exercise, length:Int) {

        itemView.exerciseShimmer.startShimmerAnimation()



    }


}