package com.example.itstym.perbmember.Water.WaterAnalysis.Presenter

import android.content.Context
import android.widget.ImageView
import com.example.itstym.perbmember.Base.Activity.Presenter.MvpPresenter
import com.example.itstym.perbmember.Splash.View.SplashMvpView
import com.example.itstym.perbmember.Water.View.WaterView
import com.example.itstym.perbmember.Water.WaterAnalysis.View.WaterAnalysisView

/**
 * Created by itstym on 10/12/17.
 */


interface WaterAnalysisMvpPresenter<in V: WaterAnalysisView> : MvpPresenter<V> {

    fun getWaterLogs(context: Context)
}