package com.example.itstym.perbmember.Splash.Presenter

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.util.Log
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import com.example.itstym.perbmember.Base.Activity.Presenter.BasePresenter
import com.example.itstym.perbmember.BuildConfig
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.Network.Model.VersionResponse
import com.example.itstym.perbmember.Network.RetrofitHelper.ApiUtils
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.Splash.View.SplashMvpView
import com.example.itstym.perbmember.Utils.HelperUtils
import retrofit2.Call
import retrofit2.Response


/**
 * Created by itstym on 6/12/17.
 */
class SplashPresenter<V : SplashMvpView>(dataManager: DataManager) : BasePresenter<V>(dataManager), SplashMvpPresenter<V> {
    override fun sendFirebaseToken(context: Context,token:String) {



    }

    override fun showUpdateDailog(context: Context) {

        val mApiService = ApiUtils.getAPIService()

        mApiService.checkVersion(0,BuildConfig.VERSION_NAME.toString(),0)
                .enqueue(object : retrofit2.Callback<VersionResponse> {

                    override fun onFailure(call: Call<VersionResponse>?, t: Throwable?) {

                        Log.e("Issue ",t?.message)

//                        HelperUtils.closeDialog(alertDialog)
                        if(HelperUtils.isConnected()){
                        HelperUtils.showSnackBar("Network Issue", context as Activity)}
                        else{
                            HelperUtils.showSnackBar("No internet Available", context as Activity)}


                    }

                    override fun onResponse(call: Call<VersionResponse>?, response: Response<VersionResponse>?) {

                        if (response!!.isSuccessful) {

                            if (response.body()!!.status){
                                mvpView!!.versionChecked()
                                Log.e("version check", BuildConfig.VERSION_NAME)
                            }else {
                                val builder = AlertDialog.Builder(context)

                                builder.setMessage("By Downloading the latest you will get the latest features. \n" +
                                        "Change log : \n" +
                                        "Improvement and bug Fixes")
                                        .setCancelable(false)
                                        .setPositiveButton("Update", DialogInterface.OnClickListener { dialog, id ->  })


                                //Creating dialog box
                                val alert = builder.create()
                                //Setting the title manually
                                alert.setTitle("New Update available!")
                                alert.show()



                                 // HelperUtils.showSnackBar(response.message(), context as Activity)
                            }
                        } else {

                             HelperUtils.showSnackBar("Member Not Exists", context as Activity)
                        }

                    }
                })
        val versionCode = BuildConfig.VERSION_CODE
        val versionName = BuildConfig.VERSION_NAME


    }

    lateinit var imageView: ImageView

    override fun startAnimation(context: Context, imageView: ImageView) {

        this.imageView=imageView

        imageView.startAnimation(animZoomIn)
        //imageView.startAnimation(animZoomOut)
    }

    lateinit var animZoomIn: Animation
    lateinit var animZoomOut: Animation

    var animZoomInListener= object : Animation.AnimationListener{

        override fun onAnimationRepeat(p0: Animation?) {
        }

        override fun onAnimationEnd(p0: Animation?) {
            imageView.startAnimation(animZoomOut)
        }

        override fun onAnimationStart(p0: Animation?) {
        }


    }

    var animZoomOutListener = object : Animation.AnimationListener{

        override fun onAnimationRepeat(p0: Animation?) {
        }

        override fun onAnimationEnd(p0: Animation?) {
            imageView.startAnimation(animZoomIn)
        }

        override fun onAnimationStart(p0: Animation?) {
        }

    }

    override fun loadZoomInAnimation(context: Context) {
        animZoomIn= AnimationUtils.loadAnimation(context, R.anim.zoom_in)
        animZoomIn.setAnimationListener(animZoomInListener)
    }

    override fun loadZoomOutAnimation(context: Context) {
        animZoomOut= AnimationUtils.loadAnimation(context, R.anim.zoom_out)
        animZoomOut.setAnimationListener(animZoomOutListener)
    }

    override fun decideNextActivity() {

         Log.e("data member id",dataManager.getMemberId().toString())
         if (dataManager.getMemberId()!=0) {
             mvpView!!.openMainActivity()
         } else {
            mvpView!!.openLoginActivity()
         }
    }



}