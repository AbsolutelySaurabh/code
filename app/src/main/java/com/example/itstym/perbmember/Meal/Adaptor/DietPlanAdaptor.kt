package com.example.itstym.perbmember.Meal.Adaptor

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.EditText
import android.widget.ImageView
import com.example.itstym.perbmember.Meal.Model.DietPlanViewModel
import com.example.itstym.perbmember.Meal.Presenter.MealPresenter
import com.example.itstym.perbmember.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.diet_plan_data.view.*

/**
 * Created by itstym on 12/12/17.
 */

class DietPlanAdaptor(var context: Context, var dietPlanData: ArrayList<DietPlanViewModel>,var dietPlanClickListener:DietPlanAdaptor.DietPlanSelection): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    interface DietPlanSelection{

        fun onDietPlanSelection(dietPlan: DietPlanViewModel,position: Int,selected:Boolean)
    }
    interface Custom{

        fun onCustomClcik(dietPlan: DietPlanViewModel,position: Int,selected:Boolean)
    }


    override fun getItemCount(): Int {
        return dietPlanData.size+1
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        // Log.i("Adaptor viewtype ", Integer.toString(viewType))

        val v = LayoutInflater.from(context).inflate(R.layout.diet_plan_data, parent, false)
        return Item(v)
    }

    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {

        (holder as Item).bindData(context, dietPlanData,dietPlanClickListener,this@DietPlanAdaptor)

//        if(!holder.itemView.isSelected){
//            holder.itemView.setBackgroundColor(R.color.colorPrimary)
//            holder.itemView.diet_plan_name.setTextColor(R.color.background_floating_material_dark)
//        }else
//            if(holder.itemView.isSelected){
//                holder.itemView.setBackgroundColor(R.color.colorAccent)
//                holder.itemView.diet_plan_name.setTextColor(R.color.colorPrimary)
//            }
    }

}

class Item(itemView: View) : RecyclerView.ViewHolder(itemView) {

    lateinit var context:Context

    fun bindData(context: Context, dietPlan: ArrayList<DietPlanViewModel>, dietPlanClickListener: DietPlanAdaptor.DietPlanSelection, dietPlanAdaptor: DietPlanAdaptor) {
        this.context = context




        if(adapterPosition !=dietPlan.size){
        itemView.diet_plan_name.setText(dietPlan.get(adapterPosition).DietName)
            if(MealPresenter.currentSelecteddietPlan.equals(dietPlan.get(adapterPosition).DietName)){
                if(itemView.isSelected)
                itemView.isSelected = false
                else {
                    itemView.isSelected = true
                    dietPlanClickListener.onDietPlanSelection(dietPlan.get(adapterPosition), adapterPosition, itemView.isSelected)
                }

            }
            else{
                itemView.isSelected = false

            }
        itemView.setOnClickListener{


            if(itemView.isSelected){
               // MealPresenter.currentSelecteddietPlan = dietPlan.get(adapterPosition).DietName
                itemView.isSelected = false}
            else{
                dietPlan.get(adapterPosition)
                itemView.isSelected = true}




            dietPlanClickListener.onDietPlanSelection(dietPlan.get(adapterPosition),adapterPosition,itemView.isSelected)

        }}
        else{
            itemView.diet_plan_name.text = "Custom"

            itemView.setOnClickListener{
                itemView.isSelected =true
                showDialog(dietPlan,dietPlanAdaptor)
            }

        }






    }
    fun showDialog(dietPlan: ArrayList<DietPlanViewModel>, dietPlanAdaptor: DietPlanAdaptor) {

        val dialog = Dialog(context)
        //set layout custom
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.customaddmeal_bottom_sheet)

        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val plan_name_edit_text = dialog.findViewById<EditText>(R.id.textInputText)

        val submit = dialog.findViewById<ConstraintLayout>(R.id.addMealButton)
        submit.setOnClickListener {
            dietPlan.add(DietPlanViewModel(-1,plan_name_edit_text.text.toString(),ArrayList()

            ))

            MealPresenter.currentSelecteddietPlan = plan_name_edit_text.text.toString()
            dietPlanAdaptor.notifyItemInserted(adapterPosition)

            dialog.cancel()
        }

        val window = dialog.window
        val wlp = window!!.attributes
        wlp.gravity = Gravity.BOTTOM
        wlp.flags = wlp.flags and WindowManager.LayoutParams.FLAG_DIM_BEHIND.inv()
        window.attributes = wlp



        dialog.show()
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)

    }

    private fun loadImage(imageIcon: Int, context: Context, imageView: ImageView) {

        Picasso.with(context).load(imageIcon).into(imageView)
    }

}

