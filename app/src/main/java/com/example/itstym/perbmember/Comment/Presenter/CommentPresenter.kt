package com.example.itstym.perbmember.Comment.Presenter

import android.app.Activity
import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.example.itstym.perbmember.Base.Activity.Presenter.BasePresenter
import com.example.itstym.perbmember.Comment.Adapter.CommentAdaptor
import com.example.itstym.perbmember.Comment.View.CommentView
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.Network.Model.Comment
import com.example.itstym.perbmember.Network.Model.CommentResponse
import com.example.itstym.perbmember.Network.RetrofitHelper.ApiUtils
import com.example.itstym.perbmember.Utils.HelperUtils
import retrofit2.Call
import retrofit2.Response

/**
 * Created by itstym on 8/12/17.
 */


class CommentPresenter<V : CommentView>(dataManager: DataManager) : BasePresenter<V>(dataManager), CommentMvpPresenter<V> {


    lateinit var commentRecylerView:RecyclerView
    lateinit var commentAdaptor:CommentAdaptor
    lateinit var allComments:ArrayList<Comment>


    override fun getAllPreviousComment(commentRecyclerView: RecyclerView, context: Context, blog_id:Int?) {

        if (blog_id==null){
            return
        }

       // val alertDialog = HelperUtils.showDataSendingDialog(context, "Fetching Feeds ... ")

        val mApiService = ApiUtils.getAPIService()

        mApiService.getAllComment(blog_id).enqueue(object : retrofit2.Callback<CommentResponse> {
            override fun onFailure(call: Call<CommentResponse>?, t: Throwable?) {

               // HelperUtils.closeDialog(alertDialog)
                if(HelperUtils.isConnected()){
                    HelperUtils.showSnackBar("Network Issue", context as Activity)}
                else{
                    HelperUtils.showSnackBar("No internet Available", context as Activity)}            }

            override fun onResponse(call: Call<CommentResponse>?, response: Response<CommentResponse>?) {

               // HelperUtils.closeDialog(alertDialog)

                if (response!!.body()!!.status){

                    commentRecyclerView.layoutManager=LinearLayoutManager(context)
                    commentAdaptor=CommentAdaptor(context,response.body()!!.commentArrayList,false)
                    commentRecyclerView.adapter=commentAdaptor
                    commentRecylerView=commentRecyclerView
                    allComments=response.body()!!.commentArrayList


                }
            }
        })
    }

    override fun sendComment(member_id: Int, blog_id: Int, comment: String, context: Context) {

        //val alertDialog = HelperUtils.showDataSendingDialog(context, "Sending Comment ... ")

        val mApiService = ApiUtils.getAPIService()

        mApiService.sendComment(blog_id,dataManager.getMemberId(),comment).enqueue(object : retrofit2.Callback<CommentResponse> {
            override fun onFailure(call: Call<CommentResponse>?, t: Throwable?) {

                //HelperUtils.closeDialog(alertDialog)
                HelperUtils.showSnackBar("Network Issue", context as Activity)
            }

            override fun onResponse(call: Call<CommentResponse>?, response: Response<CommentResponse>?) {

               // HelperUtils.closeDialog(alertDialog)

                if (response!!.body()!!.status){

                    Log.e("All done ","now fuck off")

                    val commentObject= Comment(comment,dataManager.getMemberName(),dataManager.getMemberProfilePic())
                    allComments.add(commentObject)
                    commentAdaptor=CommentAdaptor(context,allComments,false)
                    commentRecylerView.adapter=commentAdaptor
                    commentAdaptor.notifyDataSetChanged()

                }
            }
        })

    }
}