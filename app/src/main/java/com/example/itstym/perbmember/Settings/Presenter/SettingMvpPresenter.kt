package com.example.itstym.perbmember.Profile.Presenter

import com.example.itstym.perbmember.Base.Activity.Presenter.MvpPresenter
import com.example.itstym.perbmember.Profile.View.SettingView

/**
 * Created by itstym on 11/12/17.
 */


interface SettingMvpPresenter<in V: SettingView> : MvpPresenter<V> {


}