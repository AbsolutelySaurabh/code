package com.example.itstym.perbmember.Network.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by itstym on 11/12/17.
 */

data class Water(

        @SerializedName("water_intake_date")
        @Expose
        val waterDate:String,

        @SerializedName("num_water_glass")
        @Expose
        val numWaterGlass:Int
        )