package com.example.itstym.perbmember.Login.Presenter

import android.content.Intent
import android.net.Uri
import com.example.itstym.perbmember.Base.Activity.Presenter.BasePresenter
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.Login.View.LoginMvpView

/**
 * Created by itstym on 6/12/17.
 */


class LoginPresenter<V : LoginMvpView>(dataManager: DataManager) : BasePresenter<V>(dataManager), LoginMvpPresenter<V> {

    override fun openGmail(email: String, subject: String) {

        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + email))
        intent.putExtra(Intent.EXTRA_SUBJECT, subject)
        mvpView?.openGmailApp(intent)
    }


    override fun makeACall(phoneNo: String) {

        val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phoneNo))
        mvpView?.openCallView(intent)
    }
}
