package com.example.itstym.perbmember.Weight

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.example.itstym.perbmember.Base.Activity.BaseActivity
import com.example.itstym.perbmember.PerbMemberApplication
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.Weight.Presenter.WeightPresenter
import com.example.itstym.perbmember.Weight.View.WeightView
import kotlinx.android.synthetic.main.weight_activity_layout.*

/**
 * Created by itstym on 11/12/17.
 */


class WeightActivity: BaseActivity(), WeightView {



    override fun setUpToolbar() {

    }

    override fun weightShown(weight: Int) {

        selectedWeight.text=weight.toString()
    }

    override fun setUp() {
        val dataManager = (application as PerbMemberApplication).getDataManager()

        mWeightPresenter = WeightPresenter(dataManager)
        mWeightPresenter.onAttach(this)

        mWeightPresenter.weightGraph(this@WeightActivity, chartLayout, progressBarWeight)

       //mWeightPresenter.weightRecyclerViewSetup(this@WeightActivity)
        mWeightPresenter.weightRularListener(this@WeightActivity,weightRular)

    }

    companion object {
        fun getStartIntent(context: Context) : Intent = Intent(context,WeightActivity::class.java)
    }


    lateinit var  mWeightPresenter: WeightPresenter<WeightView>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.weight_activity_layout)


        setUp()
    }

    override fun onResume() {
        super.onResume()

        closeActivity.setOnClickListener {
            finish()
        }

        weightSubmit.setOnClickListener {

            mWeightPresenter.submitWeight(this@WeightActivity,progressBarWeight,weightSubmit)
        }


    }



}