package com.example.itstym.perbmember.Splash

import android.os.Bundle
import android.os.Handler
import com.crashlytics.android.Crashlytics
import com.example.itstym.perbmember.Base.Activity.BaseActivity
import com.example.itstym.perbmember.Dashboard.DashboardActivity
import com.example.itstym.perbmember.Login.LoginActivity
import com.example.itstym.perbmember.PerbMemberApplication
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.Splash.Presenter.SplashPresenter
import com.example.itstym.perbmember.Splash.View.SplashMvpView
import com.google.firebase.crash.FirebaseCrash
import com.google.firebase.iid.FirebaseInstanceId
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.splash_activity_layout.*





/**
 * Created by itstym on 6/12/17.
 */
class SplashActivity: BaseActivity(), SplashMvpView {
    override fun versionChecked() {

        Handler().postDelayed( {
            mSplashPresenter.decideNextActivity()
        }, 1000)

    }

    override fun setUpToolbar() {

    }
    override fun setUp() {
        val dataManager = (application as PerbMemberApplication).getDataManager()

        mSplashPresenter = SplashPresenter(dataManager)

        mSplashPresenter.onAttach(this)

        //load and attach listener
        mSplashPresenter.loadZoomInAnimation(context = this@SplashActivity)
        mSplashPresenter.loadZoomOutAnimation(context = this@SplashActivity)

        FirebaseInstanceId.getInstance()
        Fabric.with(this, Crashlytics())
        //start animation
        mSplashPresenter.startAnimation(this@SplashActivity,splashImage)
       // mSplashPresenter.sendFirebaseToken(this@SplashActivity, FirebaseInstanceId.getInstance().getToken()!!)

        //check for the latest build
        mSplashPresenter.showUpdateDailog(this@SplashActivity)

    }


    lateinit var  mSplashPresenter: SplashPresenter<SplashMvpView>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_activity_layout)
        FirebaseCrash.log("Activity created")


        setUp()
    }

    override fun onResume() {
        super.onResume()

       ;
    }

    override fun openMainActivity() {
       val intent = DashboardActivity.getStartIntent(this)
       startActivity(intent)
      finish()
  }

    override fun openLoginActivity() {
        val intent = LoginActivity.getStartIntent(this)
        startActivity(intent)
        finish()
    }

}