package com.example.itstym.perbmember.Meal.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DeitPlan {

    @SerializedName("status")
    @Expose
    var status: Boolean? = null
    @SerializedName("diet_details")
    @Expose
    var dietDetails: List<DietDetail>? = null

}
