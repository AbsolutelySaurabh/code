package com.example.itstym.perbmember.Base.Activity.Presenter

import com.example.itstym.perbmember.Base.Activity.View.MvpView
import com.example.itstym.perbmember.DataManager

/**
 * Created by itstym on 6/12/17.
 */


open class BasePresenter<V : MvpView>(dataManager: DataManager) : MvpPresenter<V> {

    override fun onDetach() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun setUserAsLoggedOut() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    var mvpView: V? = null
        private set

    var dataManager: DataManager
        internal set

    init {
        this.dataManager = dataManager
    }

    override fun onAttach(mvpView: V) {
        this.mvpView = mvpView
    }




}
