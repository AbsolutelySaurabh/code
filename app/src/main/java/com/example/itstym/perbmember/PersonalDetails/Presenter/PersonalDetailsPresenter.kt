package com.example.itstym.perbmember.PersonalDetails.Presenter

import android.content.Context
import android.util.Log
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import com.example.itstym.perbmember.Base.Activity.Presenter.BasePresenter
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.PersonalDetails.View.PersonalDetailsView
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.Splash.Presenter.SplashMvpPresenter
import com.example.itstym.perbmember.Splash.View.SplashMvpView

/**
 * Created by itstym on 11/12/17.
 */


class PersonalDetailsPresenter<V : PersonalDetailsView>(dataManager: DataManager) : BasePresenter<V>(dataManager), PersonalDetailsMvpPresenter<V> {


    override fun setPersonalInfo() {

        val memberName=dataManager.getMemberName()
        val memberPhone=dataManager.getMemberPhone()
        val memberProfilePicUrl=dataManager.getMemberProfilePic()
        val memberEmail=dataManager.getEmailId()
        val memberGender=dataManager.getMemberGender()
        val memberDoj=dataManager.getMemberDoj()
        val memberAddress=dataManager.getMemberAddress()

        mvpView?.showPersonalInfo(memberName,memberPhone, memberProfilePicUrl, memberEmail, memberGender, memberDoj, memberAddress)
    }


}
