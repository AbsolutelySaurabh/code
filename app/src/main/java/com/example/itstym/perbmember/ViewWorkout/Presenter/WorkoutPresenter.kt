package com.example.itstym.perbmember.ViewWorkout.Presenter

import android.app.Activity
import android.content.Context
import android.support.design.widget.BottomSheetDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.TextView
import com.example.itstym.perbmember.Base.Activity.Presenter.BasePresenter
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.Network.Model.CommonApiResponse
import com.example.itstym.perbmember.Network.Model.Workout
import com.example.itstym.perbmember.Network.Model.WorkoutResponse
import com.example.itstym.perbmember.Network.RetrofitHelper.ApiUtils
import com.example.itstym.perbmember.Utils.HelperUtils
import com.example.itstym.perbmember.ViewWorkout.Adaptor.BodyPartAdapter
import com.example.itstym.perbmember.ViewWorkout.Adaptor.ExerciseAdapter
import com.example.itstym.perbmember.ViewWorkout.Model.BodyPart
import com.example.itstym.perbmember.ViewWorkout.Model.Exercise
import com.example.itstym.perbmember.ViewWorkout.View.WorkoutView
import me.zhanghai.android.materialprogressbar.MaterialProgressBar
import retrofit2.Call
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


/**
 * Created by itstym on 6/12/17.
 */

class WorkoutPresenter<V : WorkoutView>(dataManager: DataManager) : BasePresenter<V>(dataManager), WorkoutMvpPresenter<V> {


    lateinit var exerciseRecycleAdapter: ExerciseAdapter
    lateinit var bodyPartRecyclerAdaper: BodyPartAdapter
    lateinit var allSortedWorkoutData: HashMap<Int, HashMap<BodyPart, ArrayList<Exercise>>>
    lateinit var exerciseRecyclerViewGlobal: RecyclerView
    lateinit var bodyPartRecyclerViewGlobalObject: RecyclerView
    lateinit var errorEmpty: TextView


    lateinit var selectedExercise: ArrayList<Exercise>
    lateinit var selectedDays: ArrayList<Int>

    override fun getAllWorkout(memberId: Int, context: Context, bodyPartRecyclerView: RecyclerView, exerciseRecyclerView: RecyclerView, errorEmpty: TextView) {

        //val alertDialog = HelperUtils.showDataSendingDialog(context, "Fetching Workout Details... ")
        exerciseRecyclerViewGlobal = exerciseRecyclerView
        bodyPartRecyclerViewGlobalObject = bodyPartRecyclerView
        this.errorEmpty = errorEmpty

        val mApiService = ApiUtils.getAPIService()

        Log.e("member id", memberId.toString())

        mApiService.getWorkout(memberId).enqueue(object : retrofit2.Callback<WorkoutResponse> {
            override fun onFailure(call: Call<WorkoutResponse>?, t: Throwable?) {

                // HelperUtils.closeDialog(alertDialog)
                HelperUtils.showSnackBar("Network Issue", context as Activity)
            }

            override fun onResponse(call: Call<WorkoutResponse>?, response: Response<WorkoutResponse>?) {

                // HelperUtils.closeDialog(alertDialog)

                selectedExercise = ArrayList()
                selectedDays = ArrayList()
                selectedDays.clear()
                selectedExercise.clear()

                Log.e("Response ", response!!.body()!!.workout.toString())

                if (response.isSuccessful) {

                    if (response.body()!!.status) {

                        for (i in response.body()!!.workout) {

                            Log.d("Workout", i.exercise_name + " " + i.selected)
                        }

                        //sort the data
                        allSortedWorkoutData = separateData(response.body()!!.workout)

                        //get today day
                        val todayCalender = Calendar.getInstance()
                        val today = todayCalender.get(Calendar.DAY_OF_WEEK) - 1;

                        //get selected days bodypart-exercise combination
                        val getSelectedDayData = allSortedWorkoutData.get(today)

                        //now get body part data
                        val getSelectedDayBodyPartData = getSelectedDayBodyPartData(todayCalender.get(Calendar.DAY_OF_WEEK) - 1)

                        for(i in getSelectedDayBodyPartData){

                            Log.e("Data",i.selected_days.toString() + i.body_part_name )
                        }

                        //now get all exercise

                        val getSelectedDayExerciseData = getExerciseData(today, null)

                        //setting the body part recyclerview
                        bodyPartRecyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                        bodyPartRecyclerViewGlobalObject = bodyPartRecyclerView



                        bodyPartRecyclerView.adapter = BodyPartAdapter(context, getSelectedDayBodyPartData, object : BodyPartAdapter.BodyPartSelection {
                            override fun onbodyPartClicked(bodyPart: BodyPart, selected: Boolean) {
                                Log.e("Data referesh", "yes")
                                //refreash exercise data

                                //  Log.e("Data is",getSelectedDayData!!.get(bodyPart)!!.get)

                                for (i in getSelectedDayData!!.get(bodyPart)!!) {

                                    Log.d("TAG Mukul", i.exercise_name)

                                }
                                errorEmpty.visibility = View.GONE


                                if (selected) {
                                    selectedExercise.clear()

                                    exerciseRecycleAdapter = ExerciseAdapter(context, getSelectedDayData.get(bodyPart)!!, object : ExerciseAdapter.ExerciseUpdated {
                                        override fun actionOnExercise(exercise: Exercise, isSelected: Boolean) {

                                            Log.e("is Selected", isSelected.toString())

                                            if (isSelected == false) {

                                                selectedExercise.add(exercise)

                                            } else {
                                                selectedExercise.remove(exercise)

                                            }


                                            selectedDays.add(todayCalender.get(Calendar.DAY_OF_WEEK) - 1)

                                        }


                                    }, today)
                                    exerciseRecyclerView.adapter = exerciseRecycleAdapter
                                    exerciseRecycleAdapter.notifyDataSetChanged()
                                } else {
                                    errorEmpty.visibility = View.VISIBLE
                                    selectedExercise.clear()

                                }
                            }


                        }, today ,true,getTodayBodypartid(today))

                        errorEmpty.visibility = View.VISIBLE

                        //setting the exercise recyclerview
//                        exerciseRecyclerView.layoutManager=LinearLayoutManager(context)
//                        exerciseRecyclerViewGlobal=exerciseRecyclerView
//                        exerciseRecycleAdapter=ExerciseAdapter(context,getSelectedDayExerciseData!!, object: ExerciseAdapter.ExerciseUpdated{
//                            override fun actionOnExercise(exercise: Exercise, isSelected: Boolean) {
//
//                                selectedExercise.add(exercise)
//                                selectedDays.add(todayCalender.get(Calendar.DAY_OF_WEEK)-1)
//
//                            }
//
//
//                        },selectedDays = today )
//
//                        exerciseRecyclerView.adapter=exerciseRecycleAdapter

                    } else {
                        HelperUtils.showSnackBar("OTP NOT MATCHED", context as Activity)
                    }


                } else {
                    HelperUtils.showSnackBar("Network issue while verifying otp", context as Activity)
                }
            }
        })
    }

    private fun separateData(workOutDetails: ArrayList<Workout>): HashMap<Int, HashMap<BodyPart, ArrayList<Exercise>>> {


        val workoutHashMap: HashMap<Int, HashMap<BodyPart, ArrayList<Exercise>>> = HashMap()

        //add empty hashmap with day key
        for (i in 1..7) {
            val emptyBodyPartExerciseHashMap: HashMap<BodyPart, ArrayList<Exercise>> = HashMap()
            workoutHashMap.put(i, emptyBodyPartExerciseHashMap)
        }

        val bodyPartExerciseHashMap: HashMap<BodyPart, ArrayList<Exercise>> = HashMap()
        val bodyPartDebug:ArrayList<BodyPart> = ArrayList()


        //make key-value pair of global exercise
        for (i in 0 until workOutDetails.size) {

            var dayForThatUserHaveSelected = ArrayList<Int>()
            // Log.d("TAG 2",workOutDetails[i].);
            if (workOutDetails[i].selected) {
                dayForThatUserHaveSelected = workOutDetails[i].days
            }

            Log.e("days  have selected",dayForThatUserHaveSelected.toString())

            val bodyPart = BodyPart(workOutDetails[i].body_part_id, workOutDetails[i].body_part_name, workOutDetails[i].body_part_url, false, dayForThatUserHaveSelected)
            val exercise = Exercise(workOutDetails[i].exercise_id, workOutDetails[i].exercise_name, workOutDetails[i].exercise_url, workOutDetails[i].body_part_id, false, dayForThatUserHaveSelected)

            //check if body part key already added or not
            Log.e("TAG 2", isBodyPartAlreadyAdded(bodyPartExerciseHashMap, bodyPart).toString() + "\n" +

                    bodyPart.body_part_name)


            if (isBodyPartAlreadyAdded(bodyPartExerciseHashMap, bodyPart)) {

                Log.e("hello", exercise.exercise_name + i.toString())


                //get all exercise


                //val getPreviousBodyPart= getAlreadyAddedBodyPart(bodyPart,bodyPartExerciseHashMap)


                for (i in bodyPartExerciseHashMap) {

                    if (i.key.body_part_id == bodyPart.body_part_id) {
                        //    val alreadyAddedExercise= getalreadyaddedExercise(i.key,bodyPartExerciseHashMap)
                        val alreadyAddedExercise = bodyPartExerciseHashMap.get(i.key)

                        alreadyAddedExercise?.add(exercise)

//                        for(j in alreadyAddedExercise!!){
//
//                            if(j.selected_days.size>0){
//
//                            i.key.selected_days.clear()
//                                for(k in j.selected_days ){
//                                    i.key.selected_days.add(k)
//                                }
//
//                            }
//                        }
                        Log.e("already added","dic")
                        bodyPartExerciseHashMap.put(i.key, alreadyAddedExercise!!)


                    }
                }


            } else {
                //not added
                val exerciseArrayList: ArrayList<Exercise> = ArrayList()
                exerciseArrayList.add(exercise)
                bodyPartExerciseHashMap.put(bodyPart, exerciseArrayList)
            }

        }


        Log.e("Sorted Exercise", bodyPartExerciseHashMap.toString())

        for (i in 1..7) {
            workoutHashMap.put(i, bodyPartExerciseHashMap)
        }

        Log.e("Current workouthasmap", "" + workoutHashMap)


        //now find the selected exercise and add them




        return workoutHashMap
    }



    private fun getAlreadyAddedBodyPart(bodyPart: BodyPart, bodyPartExerciseHashMap: HashMap<BodyPart, ArrayList<Exercise>>): Any? {


        return null
    }

    fun getalreadyaddedExercise(bodyPart: BodyPart, bodyPartExerciseHashMap: HashMap<BodyPart, ArrayList<Exercise>>): ArrayList<Exercise>? {


        for (i in bodyPartExerciseHashMap) {

            if (i.key.body_part_id == bodyPart.body_part_id) {

                return bodyPartExerciseHashMap.get(i.key)
            }
        }



        return ArrayList()
    }

    private fun isBodyPartAlreadyAdded(bodyPartExerciseHashMap: HashMap<BodyPart, ArrayList<Exercise>>, bodyPart: BodyPart): Boolean {

        var isAlreadyAdded = false


        val allKeys = bodyPartExerciseHashMap.keys
        val i = allKeys.iterator()
        while (i.hasNext()) {
            val keyBodyPart = i.next()

            if (keyBodyPart.body_part_id == bodyPart.body_part_id) {
                isAlreadyAdded = true
            }
        }

        return isAlreadyAdded
    }

    override fun workOutSubmitButtonClicked() {

        if (selectedExercise.size > 0)
            mvpView?.addWorkouttButtonClicked()
        else
            mvpView?.showToast("Please select any exercise")
    }

    override fun getDays(): ArrayList<String> {
        val dayArrayList = ArrayList<String>()

        dayArrayList.add("Mon")
        dayArrayList.add("Tue")
        dayArrayList.add("Wed")
        dayArrayList.add("Thu")
        dayArrayList.add("Fri")
        dayArrayList.add("Sat")
        dayArrayList.add("Sun")

        return dayArrayList
    }

    override fun onConfirmButtonClicked(context: Context, distinctArray: ArrayList<Int>, progressbar: MaterialProgressBar, mBottomSheetDialog: BottomSheetDialog) {

        //send data to server

      //  val alertDialog = HelperUtils.showDataSendingDialog(context, "Checking Phone number... ")

        val mApiService = ApiUtils.getAPIService()

        val selectedExerciseId: ArrayList<Int> = ArrayList()

        (0 until selectedExercise.size).mapTo(selectedExerciseId) { selectedExercise[it].exercise_id }

        //remove duplicate days if any
        val dayUnique = HashSet<Int>()
        dayUnique.addAll(selectedDays)
        selectedDays.clear()
        selectedDays.addAll(dayUnique)

        mApiService.addWorkOut(memberId = dataManager.getMemberId(), exerciseDetails = selectedExerciseId, daysDetails = distinctArray)
                .enqueue(object : retrofit2.Callback<CommonApiResponse> {

                    override fun onFailure(call: Call<CommonApiResponse>?, t: Throwable?) {

                        Log.e("Issue ", t?.message)

                       // HelperUtils.closeDialog(alertDialog)
                        progressbar.visibility =View.GONE
                        HelperUtils.showSnackBar("Network Issue", context as Activity)
                    }

                    override fun onResponse(call: Call<CommonApiResponse>?, response: Response<CommonApiResponse>?) {

                       // HelperUtils.closeDialog(alertDialog)

                        progressbar.visibility =View.GONE


                        if (response!!.isSuccessful) {

                            if (response.body()!!.status) {
                                mBottomSheetDialog.dismiss()
                                mvpView?.showConfirmDialog()

                            } else {
                                HelperUtils.showSnackBar("Member Not Exists", context as Activity)
                            }
                        } else {
                            HelperUtils.showSnackBar("Member Not Exists", context as Activity)
                        }

                    }
                })

    }


    override fun setTodayDay() {

        val todayCalender = Calendar.getInstance()
        mvpView?.highLightTodayDay(todayCalender.get(Calendar.DAY_OF_WEEK))
    }

    fun getExerciseData(selectedDays: Int, bodyPart: BodyPart?): ArrayList<Exercise> {


        val getSelectedDayData = allSortedWorkoutData[selectedDays]

        if (selectedDays != 0) {
            //we know only selected days, then either one of the body part is selected or no one is selected
            //if nothing is selected we assume first is selected

            if (bodyPart != null) {
                return getSelectedDayData?.get(bodyPart)!!
            }
            val exerciseData = getSelectedDayData!!.keys.iterator()
            return getSelectedDayData.get(exerciseData.next())!!
        }

        return ArrayList()

        /*else{
            //we know body part
            return getSelectedDayData!!.get(bodyPart)!!
        }*/

    }

    override fun upDateWorkout(selectedDay: Int, context: Context) {

        this.errorEmpty.visibility = View.VISIBLE

        //update the workout

        bodyPartRecyclerAdaper = BodyPartAdapter(context, getSelectedDayBodyPartData(selectedDay), object : BodyPartAdapter.BodyPartSelection {


            override fun onbodyPartClicked(bodyPart: BodyPart, selected: Boolean) {
                Log.e("Data referesh", "yes")
                // refresh exercise data
                errorEmpty.visibility = View.GONE

                if (selected) {
                    selectedExercise.clear()

                    exerciseRecycleAdapter = ExerciseAdapter(context, getExerciseData(selectedDay, bodyPart), object : ExerciseAdapter.ExerciseUpdated {
                        override fun actionOnExercise(exercise: Exercise, isSelected: Boolean) {
                            Log.e("is Selected", exercise.exercise_name)


                            selectedExercise.add(exercise)
                            selectedDays.add(selectedDay)

                        }


                    }, selectedDay)
                    exerciseRecyclerViewGlobal.adapter = exerciseRecycleAdapter
                    exerciseRecycleAdapter.notifyDataSetChanged()
                } else {
                    errorEmpty.visibility = View.VISIBLE
                    selectedExercise.clear()
                }


            }


        }, selectedDay, true, getTodayBodypartid(selectedDay))



        bodyPartRecyclerViewGlobalObject.adapter = bodyPartRecyclerAdaper
        bodyPartRecyclerAdaper.notifyDataSetChanged()

        //refresh the exercise recyclerview
        exerciseRecycleAdapter = ExerciseAdapter(context, getExerciseData(selectedDay, bodyPart = null), object : ExerciseAdapter.ExerciseUpdated {
            override fun actionOnExercise(exercise: Exercise, isSelected: Boolean) {


                selectedExercise.add(exercise)
                selectedDays.add(selectedDay)

            }


        }, selectedDay)
        exerciseRecyclerViewGlobal.adapter = exerciseRecycleAdapter
        exerciseRecycleAdapter.notifyDataSetChanged()

        //update the bodypart recycler
    }

    fun getSelectedDayBodyPartData(selectedDay: Int): ArrayList<BodyPart> {

        //get selected days bodypart-exercise combination
        val getSelectedDayData = allSortedWorkoutData[selectedDay]


        //now get body part data

        val getSelectedDayBodyPartData: ArrayList<BodyPart> = ArrayList()

//        for(i in getSelectedDayData!!.keys){
//
//            getSelectedDayBodyPartData.add(i)
//
//
//        }

        val allKeys = getSelectedDayData!!.keys
        val bodyData = allKeys.iterator()
        while (bodyData.hasNext()) {
            val tempBodyPart = bodyData.next()
            tempBodyPart.selected_days
            Log.e("tempBodyData", tempBodyPart.selected_days.toString())



            getSelectedDayBodyPartData.add(tempBodyPart)
        }
      //  Log.e("get selected data",getSelectedDayBodyPartData.toString())


        return getSelectedDayBodyPartData
    }

    private fun getTodayBodypartid(today1:Int):Int{
        Log.e("Today BodyPArt",today1.toString())

        for(i in allSortedWorkoutData[today1]!!){
            for(j in i.value){
                Log.e("Today BodyPArt sele", j.selected_days.toString())

                for(k in j.selected_days!!){
                    if(k==today1){
                        return j.body_part_id
                    }

                }





            }


        }

        return -1
    }
}