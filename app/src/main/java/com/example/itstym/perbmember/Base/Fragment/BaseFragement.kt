package com.example.itstym.perbmember.Base.Fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import com.example.itstym.perbmember.Base.Activity.BaseActivity
import com.example.itstym.perbmember.Base.Activity.View.MvpView

/**
 * Created by itstym on 6/12/17.
 */

abstract class BaseFragment : Fragment(), MvpView {

    override fun onDetach() {
        mActivity=null
        super.onDetach()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup(view)
    }

    private var mActivity: BaseActivity? = null


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is BaseActivity) {
            this.mActivity = context
            context.onFragmentAttached()
        }
    }


    override fun onDestroy() {
        super.onDestroy()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
    }



    abstract fun setup(view: View?)

    interface Callback {

        fun onFragmentAttached()

        fun onFragmentDetached(tag: String)
    }
}