package com.example.itstym.perbmember.Base.Dialog.Presenter

import com.example.itstym.perbmember.Base.Dialog.View.DialogView

/**
 * Created by itstym on 6/12/17.
 */


open class DialogPresenter<V : DialogView>: DialogMvpPresenter<V> {


    var mvpView: V? = null
        private set

    override fun onAttach(mvpView: V) {
        this.mvpView = mvpView
    }
}