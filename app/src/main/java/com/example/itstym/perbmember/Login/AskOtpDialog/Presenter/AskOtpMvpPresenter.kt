package com.example.itstym.perbmember.Login.AskOtpDialog.Presenter

import android.content.Context
import com.example.itstym.perbmember.Base.Dialog.Presenter.DialogMvpPresenter
import com.example.itstym.perbmember.Login.AskOtpDialog.View.AskOtpView
import me.zhanghai.android.materialprogressbar.MaterialProgressBar

/**
 * Created by itstym on 6/12/17.
 */


interface AskOtpMvpPresenter<in V: AskOtpView> : DialogMvpPresenter<V> {

    fun verifyOtp(otp: Int, userPhoneNo: String, context: Context, progressBarOTP: MaterialProgressBar)
}