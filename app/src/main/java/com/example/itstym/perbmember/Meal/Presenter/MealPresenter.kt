package com.example.itstym.perbmember.Meal.Presenter

import android.app.Activity
import android.content.Context
import android.support.design.widget.BottomSheetDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.Toast
import com.example.absolutelysaurabh.perbmember.MealsData.CustomMealsDataAdapter
import com.example.itstym.perbmember.Base.Activity.Presenter.BasePresenter
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.Meal.Adaptor.CustomMealAdaptor
import com.example.itstym.perbmember.Meal.Adaptor.DietPlanAdaptor
import com.example.itstym.perbmember.Meal.Adaptor.FoodItemAdaptor
import com.example.itstym.perbmember.Meal.CustomMealDailog
import com.example.itstym.perbmember.Meal.MealActivity
import com.example.itstym.perbmember.Meal.Model.*
import com.example.itstym.perbmember.Meal.View.MealView
import com.example.itstym.perbmember.Meal.newmodel.SendMealModel
import com.example.itstym.perbmember.Network.Model.CommonApiResponse
import com.example.itstym.perbmember.Network.Model.Meal
import com.example.itstym.perbmember.Network.Model.MealResponse
import com.example.itstym.perbmember.Network.RetrofitHelper.ApiUtils
import com.example.itstym.perbmember.Utils.HelperUtils
import com.google.gson.GsonBuilder
import me.zhanghai.android.materialprogressbar.MaterialProgressBar
import retrofit2.Call
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by itstym on 12/12/17.
 */


 class MealPresenter<V : MealView>(dataManager: DataManager) : BasePresenter<V>(dataManager), MealMvpPresenter<V> {

    companion object {

        lateinit var datamanager:DataManager
        var customMealItemsArray: ArrayList<MealViewPlan> = ArrayList()
        val finalDietPlanArray:ArrayList<DietPlanViewModel> = ArrayList()
        lateinit var dietPlanRecyclerViewforrefresh:RecyclerView
        lateinit var dietMealRecyclerViewforrefresh:RecyclerView
        lateinit var context1:Context
        var currentSelecteddietPlan= ""
        var selecteddat:Int = 0


    }
    val customMealFoodItems:ArrayList<String> =  ArrayList()

    override fun mealSubmitClicked(mealActivity: MealActivity) {
        if(sendMealData!= null){
            mvpView!!.addMemberDietPlan(sendMealData)

        }else{
            Toast.makeText(mealActivity,"Null Object",Toast.LENGTH_SHORT).show()
        }
    }

    override fun sendMeal(context: Context, meal:SendMealModel, progressBar: MaterialProgressBar, mBottomSheetDialog: BottomSheetDialog) {

        val mApiService = ApiUtils.getAPIService()
        val gson = GsonBuilder().create()
        Log.e("Json array",meal.gymId.toString()+meal.dietName+meal.mealTypeArray!!.toString()+meal.memberId+meal.days)

        mApiService.sendAllMeal(meal.gymId!!, meal.dietName!!, meal.mealTypeArray!!, meal.memberId!!, meal.days!!)
                .enqueue(object : retrofit2.Callback<CommonApiResponse> {

                    override fun onFailure(call: Call<CommonApiResponse>?, t: Throwable?) {

                        Log.e("Issue A ",t?.message)


                    }

                    override fun onResponse(call: Call<CommonApiResponse>?, response: Response<CommonApiResponse>?) {

                        if (response!!.isSuccessful) {
                            Log.e("Issue B ", response!!.body().toString())


                            if(response.body()?.status!!){




                            }

                        } else {
                            Log.e("Issue C ", response!!.body().toString())


                            HelperUtils.showSnackBar("Member Not Exists", context as Activity)
                        }
                       // Log.e("Issue  D", response!!.body().toString())


                    }
                })

    }

    override fun getDays(): ArrayList<String> {

        customMealItemsArray = ArrayList()

        val dayArrayList = ArrayList<String>()

        dayArrayList.add("Mon")
        dayArrayList.add("Tue")
        dayArrayList.add("Wed")
        dayArrayList.add("Thu")
        dayArrayList.add("Fri")
        dayArrayList.add("Sat")
        dayArrayList.add("Sun")

        return dayArrayList
    }

    lateinit var sendMealData:SendMealModel


    override fun getAllMeals(context: Context, dietPlanRecyclerView: RecyclerView, dietFoodItemRecyclerView: RecyclerView, currentDay: Int) {
        //fetch meal

        val mApiService = ApiUtils.getAPIService()

        mApiService.getAllMeals(3, dataManager.getMemberId())
                .enqueue(object : retrofit2.Callback<MealResponse> {

                    override fun onFailure(call: Call<MealResponse>?, t: Throwable?) {

                        Log.e("Issue ",t?.message)


                    }

                    override fun onResponse(call: Call<MealResponse>?, response: Response<MealResponse>?) {

                        if (response!!.isSuccessful) {

                            if(response.body()?.status!!){

                                Log.e("All set ","out put is "+response.body()?.dietPlanArrayList)


                                showdata(dietPlanRecyclerView,dietFoodItemRecyclerView,  arrangeTheData(response.body()?.dietPlanArrayList!!),currentDay,context)

                            }

                        } else {

                            HelperUtils.showSnackBar("Member Not Exists", context as Activity)
                        }

                    }
                })
    }

    private fun showdata(dietPlanRecyclerView: RecyclerView, dietFoodItemRecyclerView: RecyclerView, arrangeTheData1: HashMap<Int, ArrayList<HashMap<DietPlanObject, ArrayList<HashMap<MealPlanObject, ArrayList<FoodItems>>>>>>, currentDay: Int, context: Context) {
        MealPresenter.context1 = context
        MealPresenter.selecteddat=currentDay
        MealPresenter.dietPlanRecyclerViewforrefresh = dietPlanRecyclerView
        MealPresenter.dietMealRecyclerViewforrefresh = dietFoodItemRecyclerView
        MealPresenter.datamanager= dataManager
        dietPlanRecyclerView.layoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
        var dietPlanRecylerViewArray:ArrayList<DietPlanViewModel> = ArrayList()
        var dietMealsRecylerViewArray:ArrayList<MealViewPlan> = ArrayList()
        var finaldietMealsRecylerViewArray:ArrayList<MealViewPlan> = ArrayList()
        var forMealSendDATA:ArrayList<MealSendDATA> = ArrayList()


        // Extracting data for diet plan recyler view
        for(i in 0.. arrangeTheData1.get(currentDay)!!.size-1){
            for(j in arrangeTheData1.get(currentDay)!!.get(i).keys){
                dietPlanRecylerViewArray.add(DietPlanViewModel(j.dietId,j.dietPlanName,

                        arrangeTheData1.get(currentDay)!!.get(i).get(j)
                        ))
            }

        }


        // Extracting data for Meals
        var previousMealid = -1

        for(i in dietPlanRecylerViewArray){
            for( j in 0 until i.mealItems!!.size-1){

                for ( k in i.mealItems!!.get(j).keys){

                    if(previousMealid!=k.mealId){


                        dietMealsRecylerViewArray.add(MealViewPlan(k.mealId,
                                k.mealType,k.alarmTime,
                                i.mealItems!!.get(j).get(k)

                        ))

                        previousMealid = k.mealId


                    }


                }

            }

        }


//        for(i in finaldietMealsRecylerViewArray){
//            Log.e("Meals",i.mealType)
//        }

        var selectedDays:ArrayList<Int> = ArrayList()
        selectedDays.add(currentDay)

        for(i in finalDietPlanArray){
            dietPlanRecylerViewArray.add(i)

        }

       // dietPlanRecylerViewArray.add(DietPlanViewModel(-1,))
        dietPlanRecyclerView.layoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL, false)
        dietPlanRecyclerView.adapter  = DietPlanAdaptor(context,dietPlanRecylerViewArray,object :DietPlanAdaptor.DietPlanSelection{
            override fun onDietPlanSelection(dietPlan: DietPlanViewModel, position: Int, selected: Boolean) {


                Log.e("dietplan",dietPlan.DietName)

                if(dietPlan.mealItems!!.size>0){
                if(selected){
                    finaldietMealsRecylerViewArray.clear()
                    dietMealsRecylerViewArray.clear()


                    var previousMealid = -1


                        for( j in 0 until dietPlan.mealItems!!.size-1){

                            for ( k in dietPlan.mealItems!!.get(j).keys){

                                if(previousMealid!=k.mealId){


                                    dietMealsRecylerViewArray.add(MealViewPlan(k.mealId,
                                            k.mealType,k.alarmTime,
                                            dietPlan.mealItems!!.get(j).get(k)

                                    ))

                                    previousMealid = k.mealId


                                }


                            }

                        }
                    Collections.sort(dietMealsRecylerViewArray, object : Comparator<MealViewPlan> {
                        override fun compare(p0: MealViewPlan?, p1: MealViewPlan?): Int {
                            return p0!!.mealId.compareTo(p1!!.mealId)
                        }

                    }
                    )
                    var tempdietMealsRecylerViewArraysize = dietMealsRecylerViewArray.size

                    var prevId =-1

                    for(i in dietMealsRecylerViewArray){

                        if(prevId!= i.mealId){
                            finaldietMealsRecylerViewArray.add(i)
                            prevId = i.mealId
                        }


                    }



                    dietFoodItemRecyclerView.layoutManager = LinearLayoutManager(context)
                    dietFoodItemRecyclerView.adapter = FoodItemAdaptor(context,finaldietMealsRecylerViewArray)

                    var sendableMealArray:ArrayList<String> = ArrayList()


                    for(i in finaldietMealsRecylerViewArray){

                        val foodItems:ArrayList<String> =ArrayList()

                        for(j in i.fooditems!!){
                            foodItems.add(j.foodName)
                        }
                        sendableMealArray.add(
                                GsonBuilder().create().toJson(MealSendDATA(i.mealType,i.alarmTime,foodItems))

                        )
                        Log.e("MEal Type",GsonBuilder().create().toJson(MealSendDATA(i.mealType,i.alarmTime,foodItems)))

                    }


                    Log.e("selected days",selectedDays.toString())


                    sendMealData = SendMealModel(3,dietPlan.DietName,sendableMealArray,dataManager.getMemberId(),selectedDays)

                }}
                else{
                    if(!finalDietPlanArray.contains(dietPlan))
                    finalDietPlanArray.add(dietPlan)

                    if(customMealItemsArray.size==0)
                        CustomMealDailog(context,dietPlan.DietName,dataManager).show()
                    else{
                        dietFoodItemRecyclerView.layoutManager = LinearLayoutManager(context)
//                        dietFoodItemRecyclerView.adapter = CustomMealAdaptor(context, object : CustomMealAdaptor.onAddMeal{
//                            override fun openAddMealDailog() {
//                                CustomMealDailog(context,dietPlan.DietName,dataManager).show()
//
//                            }
//
//
//                        },customMealItemsArray)

                        dietFoodItemRecyclerView.adapter = CustomMealsDataAdapter(context, object : CustomMealAdaptor.onAddMeal{
                            override fun openAddMealDailog() {
                                CustomMealDailog(context,dietPlan.DietName,dataManager).show()

                            }


                        },customMealItemsArray)


                      //  Log.e("Succesfully added","yeah")
                    }

                   // Log.e("Hello","Hello Whatsup")
                }

            }


        })










       // dietPlanRecyclerView.adapter =  DietPlanAdaptor()

    }

    private fun arrangeTheData(dietPlans: ArrayList<Meal>): HashMap<Int, ArrayList<HashMap<DietPlanObject, ArrayList<HashMap<MealPlanObject, ArrayList<FoodItems>>>>>> {

        Log.e("before data ",dietPlans.toString())

        val dietPlansSortedByDay:HashMap<Int,ArrayList<HashMap<DietPlanObject,ArrayList<HashMap<MealPlanObject,ArrayList<FoodItems>>>>>> = HashMap()

        val mealFoodHashMap:HashMap<MealPlanObject,ArrayList<FoodItems>> = HashMap()
        val mealFoodArrayList:ArrayList<HashMap<MealPlanObject,ArrayList<FoodItems>>> = ArrayList()
        val dietMealHashMap:HashMap<DietPlanObject,ArrayList<HashMap<MealPlanObject,ArrayList<FoodItems>>>> = HashMap()
        val dietMealArrayList:ArrayList<HashMap<DietPlanObject,ArrayList<HashMap<MealPlanObject,ArrayList<FoodItems>>>>> = ArrayList()
        val foodItemArrayList:ArrayList<FoodItems> = ArrayList()

        //add empty hashmap with day key
        for (i in 0..7) {
            val tempDietMealArrayList:ArrayList<HashMap<DietPlanObject,ArrayList<HashMap<MealPlanObject,ArrayList<FoodItems>>>>> = ArrayList()
            dietPlansSortedByDay.put(i, tempDietMealArrayList)
        }

        for (i in 0 until dietPlans.size){

            isUpdateDiet=false

            val dietPlanObject=dietPlans[i]

            Log.e("diet plan",dietPlanObject.toString());

           /* if (i==0){
                //very first element

                val tempDietObject = DietPlanObject(dietPlanObject.dietPlanId,dietPlanName = dietPlanObject.dietPlanName)
                val tempMealObject= MealPlanObject(dietPlanObject.mealId, dietPlanObject.mealName,dietPlanObject.mealTime)
                val tempFoodObject= FoodItems(dietPlanObject.foodId,dietPlanObject.foodItem)

                foodItemArrayList.add(tempFoodObject)
                mealFoodHashMap.put(tempMealObject,foodItemArrayList)
                mealFoodArrayList.add(mealFoodHashMap)
                dietMealHashMap.put(tempDietObject,mealFoodArrayList)
                dietMealArrayList.add(dietMealHashMap)

                //finally
                dietPlansSortedByDay.put(dietPlanObject.selectedDays,dietMealArrayList)
            }

            else{
*/
                val tempDaySelected=dietPlanObject.selectedDays

                //get all the diet content on that particular day
                val tempDietData = dietPlansSortedByDay.get(tempDaySelected)

                //Log.e("Sorted Data ","o"+tempDietData)

                val tempDietObject = DietPlanObject(dietPlanObject.dietPlanId,dietPlanName = dietPlanObject.dietPlanName)
                val tempMealObject= MealPlanObject(dietPlanObject.mealId, dietPlanObject.mealName,dietPlanObject.mealTime)
                val tempFoodObject= FoodItems(dietPlanObject.foodId,dietPlanObject.foodItem)

                //fetch old diet if any
                val oldDietData:HashMap<DietPlanObject, ArrayList<HashMap<MealPlanObject, ArrayList<FoodItems>>>> = getOldDiet(tempDietObject,tempDietData)

                //now get selected diet all meal
                 val oldMealTypeItems:ArrayList<HashMap<MealPlanObject, ArrayList<FoodItems>>> = getOldMealType(tempDietObject,oldDietData)

                Log.e("Meals ",oldMealTypeItems.toString())

                 //oldMealTypeItems.add(tempMealObject)
                // now get selected meal from all meals
               val oldMealContent:HashMap<MealPlanObject, ArrayList<FoodItems>> =getOldMeal(tempMealObject,oldMealTypeItems)

                // now fetch its food items
                val oldMealFoodItems:ArrayList<FoodItems> = getOldMealFoodItem(tempMealObject,oldMealContent)

                 oldMealFoodItems.add(tempFoodObject)
                 //now put the meal object
                 oldMealContent.put(tempMealObject,oldMealFoodItems)
                 oldMealTypeItems.add(oldMealContent)

                 oldDietData.put(tempDietObject,oldMealTypeItems)

                 Log.e("final one row ",oldDietData.toString())

                updateDietData(tempDietData!!,oldDietData)

                 if(!isUpdateDiet)
                     tempDietData.add(oldDietData)

                //now fetch old meals

                /*val oldMealTypeItems:ArrayList<HashMap<MealPlanObject, ArrayList<FoodItems>>> = getOldMealType(tempDietObject,oldDietData)

                    if (oldMealTypeItems.size!==0){
                        //i.e. old meal type has some food
                        //now add more food in it

                    }
               // oldMealTypeItems.add(tempMealObject)
               // dietMealHashMap.put(tempDietObject,oldMealTypeItems)

               // dietMealArrayList.add(dietMealHashMap)


                //fetch old food
                val oldMealFoodItems:ArrayList<FoodItems> = getOldMeal(tempMealObject,mealFoodHashMap)
                oldMealFoodItems.add(tempFoodObject)
                mealFoodHashMap.put(tempMealObject,oldMealFoodItems)



*/
                //finally



                dietPlansSortedByDay.put(dietPlanObject.selectedDays,tempDietData)

            /*}*/

        }

        Log.e("final data is ",""+dietPlansSortedByDay)

        return dietPlansSortedByDay
    }

    var isUpdateDiet=false

    private fun updateDietData(completeDayDiet: ArrayList<HashMap<DietPlanObject, ArrayList<HashMap<MealPlanObject, ArrayList<FoodItems>>>>>, oldDietData: HashMap<DietPlanObject, ArrayList<HashMap<MealPlanObject, ArrayList<FoodItems>>>>) {

        //complete day Diet have list of all the diets
        //oldDietData is particular diet for that particular day and
        if (completeDayDiet.isNotEmpty()){

            for (diet in 0 until completeDayDiet.size){

                val tempDataHashmap=completeDayDiet[diet]

                for(tempData in tempDataHashmap){

                    for (secondTempData in oldDietData){

                        if (tempData.key.dietId==secondTempData.key.dietId){
                            isUpdateDiet=true
                            completeDayDiet.remove(tempDataHashmap)
                            completeDayDiet.add(oldDietData)
                        }
                    }
                }
            }
        }else{
            isUpdateDiet=true
            completeDayDiet.add(oldDietData)
        }

    }

    private fun getOldMeal(tempMealObject: MealPlanObject, oldMealTypeItems: ArrayList<HashMap<MealPlanObject, ArrayList<FoodItems>>>): HashMap<MealPlanObject, ArrayList<FoodItems>> {

        for ( i in 0 until oldMealTypeItems.size){

            val tempMealHashMap=oldMealTypeItems[i]

            for (tempData in tempMealHashMap){

                if (tempMealObject.mealId==tempData.key.mealId)
                    return tempMealHashMap;
            }
        }
        return HashMap<MealPlanObject, ArrayList<FoodItems>>()
    }

    private fun getOldDiet(tempDietObject: DietPlanObject, tempDietData: ArrayList<HashMap<DietPlanObject, ArrayList<HashMap<MealPlanObject, ArrayList<FoodItems>>>>>?) : HashMap<DietPlanObject, ArrayList<HashMap<MealPlanObject, ArrayList<FoodItems>>>> {

        //return old diet meal if found

        for (i in 0 until tempDietData!!.size){

            val hashmapData=tempDietData[i];

            Log.e("diet data ",""+hashmapData);

            for (tempHashMapData in hashmapData){

                if (tempDietObject.dietId==tempHashMapData.key.dietId){

                    return hashmapData
                }
            }
        }

        return HashMap<DietPlanObject, ArrayList<HashMap<MealPlanObject, ArrayList<FoodItems>>>>()
    }

    private fun getOldMealType(tempDietObject: DietPlanObject, dietMealHashMap: HashMap<DietPlanObject, ArrayList<HashMap<MealPlanObject, ArrayList<FoodItems>>>>): ArrayList<HashMap<MealPlanObject, ArrayList<FoodItems>>> {

        //check if diet is already added or not , if added return that meal array list otherwise new one

        for(dietData in dietMealHashMap){

            if(dietData.key.dietId==tempDietObject.dietId){
                val tempData=dietMealHashMap.get(dietData.key)!!
                dietMealHashMap.remove(dietData.key)
                return tempData
            }
        }

        return ArrayList<HashMap<MealPlanObject, ArrayList<FoodItems>>>()

    }

    private fun getOldMealFoodItem(tempMealObject: MealPlanObject, mealFoodHashMap: HashMap<MealPlanObject, ArrayList<FoodItems>>) : ArrayList<FoodItems> {

       //check if meal is already added or not , if added return that food array list otherwise new one

        for(mealFood in mealFoodHashMap){

            if(mealFood.key.mealId==tempMealObject.mealId){
                //if old data found remove it and return it.
                val tempData=mealFoodHashMap.get(mealFood.key)
                mealFoodHashMap.remove(mealFood.key)
                return tempData!!
            }
        }

        return ArrayList<FoodItems>()
    }


    private fun checkIfDayExistsOrNot(tempDaySelected: Int, dietPlansSortedByDay: HashMap<Int, ArrayList<HashMap<DietPlanObject, ArrayList<HashMap<MealPlanObject, ArrayList<FoodItems>>>>>>) {

        //get all keys

    }


    fun onCustomMealClick(){

    }

    private lateinit var mMealPresenter: MealPresenter<MealView>

    override fun customMeal(context: Context,meal: MealViewPlan) {

        mMealPresenter = MealPresenter(dataManager)
        customMealItemsArray.add(meal)
        mMealPresenter.getAllMeals(MealPresenter.context1,MealPresenter.dietPlanRecyclerViewforrefresh,MealPresenter.dietMealRecyclerViewforrefresh,MealPresenter.selecteddat)

    }




}
