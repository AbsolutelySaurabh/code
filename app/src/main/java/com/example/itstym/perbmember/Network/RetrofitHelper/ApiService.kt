package com.example.itstym.perbmember.Network.RetrofitHelper

import com.example.itstym.perbmember.Network.Model.*
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

/**
 * Created by itstym on 22/11/17.
 */

interface ApiService{

    @POST("member/loginMember")
    @FormUrlEncoded
    fun checkMemberExists(@Field("phone") phonenumber: String):Call<Member>

    @POST("universal/sendOtp")
    @FormUrlEncoded
    fun sendOTP(@Field("phone") phonenumber: String):Call<CommonApiResponse>


    @POST("universal/verifyOtp")
    @FormUrlEncoded
    fun verifyOTp(@Field("phone") phonenumber: String,
                  @Field("otp") otp: Int):Call<CommonApiResponse>


    @POST("workout/getWorkout")
    @FormUrlEncoded
    fun getWorkout(@Field("member_id") memberId:Int):Call<WorkoutResponse>

    @POST("workout/addWorkOut")
    @FormUrlEncoded
    fun addWorkOut(@Field("member_id") memberId:Int,
                   @Field("exercise_details") exerciseDetails:ArrayList<Int>,
                   @Field("days") daysDetails:ArrayList<Int>
                  ):Call<CommonApiResponse>

    @GET("/blog/getAllBlogs")
    fun getAllBlogs():Call<BlogsResponse>

    @POST("/blog/getAllComment")
    @FormUrlEncoded
    fun getAllComment(@Field("blog_id") blogId:Int):Call<CommentResponse>

    @POST("/blog/addComment")
    @FormUrlEncoded
    fun sendComment(@Field("blog_id") blogId:Int,
                    @Field("member_id") memberId:Int,
                    @Field("comment") comment:String):Call<CommentResponse>


    @POST("/member/addWaterLogs")
    @FormUrlEncoded
    fun waterLogs(@Field("member_id") memberId:Int,
                    @Field("num_glass") numWater:Int):Call<CommonApiResponse>


    @POST("/member/getWaterLogs")
    @FormUrlEncoded
    fun getAllWaterLogs(@Field("member_id") memberId:Int):Call<WaterResponse>


    @POST("/member/addWeight")
    @FormUrlEncoded
    fun getAllWaterLogs(@Field("member_id") memberId:Int,
                        @Field("weight") weight:Int):Call<CommonApiResponse>

    @POST("/member/transactionHistory")
    @FormUrlEncoded
    fun getTransactionHistory(@Field("member_id") memberId:Int):Call<TransactionResponse>


    @POST("/meal/getAllMealPlan")
    @FormUrlEncoded
    fun getAllMeals(@Field("gym_id") gymId:Int,@Field("member_id") memberId:Int):Call<MealResponse>

    @POST("/member/getWeightLogs")
    @FormUrlEncoded
    fun getAllWeight(@Field("member_id") memberId:Int
                     ):Call<WeightResponse>

    @POST("meal/addMealPlanForGym")
    @FormUrlEncoded
    fun sendAllMeal(@Field("gym_id") gym_id:Int,
                    @Field("diet_name") dietName:String,
                    @Field("meal_type_array") Meal_type_array: ArrayList<String>,
                    @Field("member_id") memberId: Int,
                    @Field("days") days:ArrayList<Int>
    ):Call<CommonApiResponse>

    @POST("universal/checkAppVersion")
    @FormUrlEncoded
    fun checkVersion(@Field("is_mobile_os_android") mobileType:Int,
                    @Field("app_version") versionCode:String,
                    @Field("is_trainer_app") typeApp:Int

    ):Call<VersionResponse>

    @POST("universal/addToken")
    @FormUrlEncoded
    fun addToken(@Field("token") Token:String,
                     @Field("is_trainer") appType:Int,
                     @Field("logging_user_id") userId:Int

    ):Call<FirebaseResponse>






    /*@POST("/member/transactionHistory")
    @FormUrlEncoded
    fun getTransactionHistory(@Field("member_id") memberId:Int):Call<TransactionResponse>

    */







}