package com.example.itstym.perbmember.Feed

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.itstym.perbmember.Base.Fragment.BaseFragment
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.Feed.Adapter.ShimmerAdaptor
import com.example.itstym.perbmember.Feed.Presenter.FeedPresenter
import com.example.itstym.perbmember.Feed.View.FeedView
import com.example.itstym.perbmember.Network.Model.Blog
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.SharedPrefsHelper

/**
 * Created by itstym on 8/12/17.
 */

class FeedFragment: BaseFragment(), FeedView,SwipeRefreshLayout.OnRefreshListener {

    lateinit var feedRecyclerView:RecyclerView
    lateinit var swipetoRefresh:SwipeRefreshLayout

    interface feedOptions{

        fun onCommentSectionClicked(blog: Blog)
        fun onBlogClicked(blog: Blog)
    }
    override fun openCommentFragment(blog: Blog) {

        //comment section clicked
        (context as feedOptions).onCommentSectionClicked(blog)
    }


    override fun openBlogTextActivity(blog: Blog) {

        (context as feedOptions).onBlogClicked(blog)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_feed, container, false)
        setup(view)


        return view
    }

    lateinit var mFeedPresenter:FeedPresenter<FeedView>

    override fun setup(view: View?) {

        Log.e(TAG,"setup")

       val dataManager= DataManager(SharedPrefsHelper(context!!))


        mFeedPresenter = FeedPresenter(dataManager)
        mFeedPresenter.onAttach(this)

        val toolbar = activity!!.findViewById<View>(R.id.toolbar) as Toolbar
        toolbar.setTitle("Home")

        val demoData:ArrayList<Blog> = ArrayList()
        for(i in 1..2){
        demoData.add(Blog(0,"","","","",""
        ,"",""))
        }

        swipetoRefresh = view!!.findViewById<SwipeRefreshLayout>(R.id.swiperefreshlayout) as SwipeRefreshLayout

        swipetoRefresh.setOnRefreshListener(this)
        feedRecyclerView=view!!.findViewById<View>(R.id.feedRecyclerView) as RecyclerView
        feedRecyclerView.layoutManager=LinearLayoutManager(context)
       // feedRecyclerView.layoutManager=LinearLayoutManager(context)
        feedRecyclerView.adapter= ShimmerAdaptor(context!!, demoData)

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        swipetoRefresh.post(Runnable {
            swipetoRefresh.setRefreshing(true)
            mFeedPresenter.getAllFeed(feedRecyclerView,context!!,swipetoRefresh)


        })
        mFeedPresenter.getAllFeed(feedRecyclerView, context!!, swipetoRefresh)





    }

    override fun onRefresh() {
        mFeedPresenter.getAllFeed(feedRecyclerView, context!!, swipetoRefresh)

    }




    companion object {

        fun newInstance():FeedFragment{
            val args= Bundle()
            val feedFragment:FeedFragment = FeedFragment();
            feedFragment.arguments=args
            return feedFragment
        }

        val TAG="FeedFragment"

    }



}