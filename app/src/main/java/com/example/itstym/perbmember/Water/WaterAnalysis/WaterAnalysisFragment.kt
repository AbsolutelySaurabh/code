package com.example.itstym.perbmember.Water.WaterAnalysis

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import com.example.itstym.perbmember.Base.Fragment.BaseFragment
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.Network.Model.Water
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.SharedPrefsHelper
import com.example.itstym.perbmember.Utils.HelperUtils
import com.example.itstym.perbmember.Water.WaterActivity
import com.example.itstym.perbmember.Water.WaterAnalysis.Presenter.WaterAnalysisPresenter
import com.example.itstym.perbmember.Water.WaterAnalysis.View.WaterAnalysisView
import com.example.itstym.perbmember.Water.WaterLog.View.WaterLogView
import com.example.itstym.perbmember.Water.WaterLog.WaterLogFragment
import java.util.*

/**
 * Created by itstym on 10/12/17.
 */


class WaterAnalysisFragment: BaseFragment(), WaterAnalysisView ,WaterLogView{
    override fun updateLog() {

        mWaterAnalysisPresenter.getWaterLogs(context!!)

        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun updateData(){

        mWaterAnalysisPresenter.getWaterLogs(context!!)

    }

    override fun updateView(numWaterGlass: Int) {
        TODO("not implemented")
        Log.e("hello","here")//To change body of created functions use File | Settings | File Templates.
    }


    override fun updateData(waterWeekdata: HashMap<Int, Water>) {

        val allKeys= waterWeekdata.keys
        val i=allKeys.iterator()

        while (i.hasNext()){

            val weekDay=i.next()
            val weekWaterConsumption=waterWeekdata.get(weekDay)

        //   Log.e("Week No", HelperUtils.getWeekNo(Calendar.getInstance().timeInMillis).toString()    )


            when(weekDay){

                1 -> {
                    Log.e("TAG water", weekWaterConsumption!!.toString())
                    if(HelperUtils.getWeekNo(weekWaterConsumption.waterDate)) {
                        mondayWaterProgressBar.progress = weekWaterConsumption!!.numWaterGlass * 10

                        // if(weekWaterConsumption!=null)
                         mondayWaterText.setText(weekWaterConsumption!!.numWaterGlass.toString())
                    }
                }

                2 -> {
                    Log.e("TAG water", weekWaterConsumption!!.toString())
                    if(HelperUtils.getWeekNo(weekWaterConsumption.waterDate)) {
                        tuesdayWaterProgressBar.progress = weekWaterConsumption!!.numWaterGlass * 10
                        tuesdayWaterText.setText(weekWaterConsumption!!.numWaterGlass.toString())
                    }

                }

                3 -> {
                    Log.e("TAG water", weekWaterConsumption!!.toString())

                    if(HelperUtils.getWeekNo(weekWaterConsumption!!.waterDate)) {

                    wednesdayWaterProgressBar.progress=weekWaterConsumption!!.numWaterGlass*10
                    wednesdayWaterText.setText(weekWaterConsumption!!.numWaterGlass.toString())}
                }

                4 -> {
                    if(HelperUtils.getWeekNo(weekWaterConsumption!!.waterDate)) {
                    thursdayWaterProgressBar.progress=weekWaterConsumption!!.numWaterGlass*10
                    thrusdayWaterText.text = weekWaterConsumption!!.numWaterGlass.toString()}
                }

                5 -> {
                    if(HelperUtils.getWeekNo(weekWaterConsumption!!.waterDate)) {
                    fridayWaterProgressBar.progress=weekWaterConsumption!!.numWaterGlass*10
                    fridayWaterText.text = weekWaterConsumption!!.numWaterGlass.toString()}
                }

                6 -> {
                    if(HelperUtils.getWeekNo(weekWaterConsumption!!.waterDate)) {
                    satardayWaterProgressBar.progress=weekWaterConsumption!!.numWaterGlass*10
                    saturdayWaterText.text = weekWaterConsumption!!.numWaterGlass.toString()}
                }

                7 -> {
                    if(HelperUtils.getWeekNo(weekWaterConsumption!!.waterDate)) {
                    sundayWaterProgressBar.progress=weekWaterConsumption!!.numWaterGlass*10
                    sundayWaterText.text = weekWaterConsumption!!.numWaterGlass.toString()}
                }




            }

        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.water_analysis_fragment_layout, container, false)
        setup(view)
        (activity as WaterActivity).tag = this!!.tag!!

        return view
    }

    lateinit var mWaterAnalysisPresenter:WaterAnalysisPresenter<WaterAnalysisView>

    lateinit var mondayWaterProgressBar:ProgressBar
    lateinit var tuesdayWaterProgressBar:ProgressBar
    lateinit var wednesdayWaterProgressBar:ProgressBar
    lateinit var thursdayWaterProgressBar:ProgressBar
    lateinit var fridayWaterProgressBar:ProgressBar
    lateinit var satardayWaterProgressBar:ProgressBar
    lateinit var sundayWaterProgressBar:ProgressBar


    lateinit var tuesdayWaterText: TextView
    lateinit var mondayWaterText: TextView
    lateinit var wednesdayWaterText: TextView
    lateinit var thrusdayWaterText: TextView
    lateinit var fridayWaterText: TextView
    lateinit var saturdayWaterText: TextView
    lateinit var sundayWaterText: TextView






    override fun setup(view: View?) {

        Log.e(WaterLogFragment.TAG,"setup")

        val dataManager= DataManager(SharedPrefsHelper(context!!))

        mWaterAnalysisPresenter = WaterAnalysisPresenter<WaterAnalysisView>(dataManager)
        mWaterAnalysisPresenter.onAttach(this)

        mondayWaterProgressBar= view?.findViewById<ProgressBar>(R.id.mondayWaterProgress) as ProgressBar
        tuesdayWaterProgressBar= view.findViewById<ProgressBar>(R.id.tuesDayWaterProgress) as ProgressBar
        wednesdayWaterProgressBar= view.findViewById<ProgressBar>(R.id.wednesdayWaterProgress) as ProgressBar
        thursdayWaterProgressBar= view.findViewById<ProgressBar>(R.id.thursdayWaterProgress) as ProgressBar
        fridayWaterProgressBar= view.findViewById<ProgressBar>(R.id.fridayWaterProgress) as ProgressBar
        satardayWaterProgressBar= view.findViewById<ProgressBar>(R.id.saturdayWaterProgress) as ProgressBar
        sundayWaterProgressBar= view.findViewById<ProgressBar>(R.id.sundayWaterProgress) as ProgressBar
        tuesdayWaterText = view.findViewById<TextView>(R.id.tuesdayWaterText) as TextView
        mondayWaterText = view.findViewById<TextView>(R.id.mondayWaterText) as TextView
        wednesdayWaterText = view.findViewById<TextView>(R.id.wednesdayWaterText) as TextView
        thrusdayWaterText = view.findViewById<TextView>(R.id.thrusdayWaterText) as TextView
        fridayWaterText = view.findViewById<TextView>(R.id.fridayWaterText) as TextView
        saturdayWaterText = view.findViewById<TextView>(R.id.saturdayWaterText) as TextView
        sundayWaterText = view.findViewById<TextView>(R.id.sundayWaterText) as TextView







        mWaterAnalysisPresenter.getWaterLogs(context!!)

    }

    companion object {

        fun newInstance(): WaterAnalysisFragment {
            val args= Bundle()
            val waterAnalysisFragement: WaterAnalysisFragment = WaterAnalysisFragment();
            waterAnalysisFragement.arguments=args
            return waterAnalysisFragement
        }

        val TAG="WaterAnalysisFragment"

    }

}