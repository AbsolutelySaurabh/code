package com.example.itstym.perbmember.Feed.Adapter

import android.app.Activity
import android.content.Context
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.itstym.perbmember.Feed.YoutubeConfig
import com.example.itstym.perbmember.Network.Model.Blog
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.Utils.HelperUtils
import com.google.android.youtube.player.*
import kotlinx.android.synthetic.main.blog_item.view.*


/**
 * Created by itstym on 8/12/17.
 */

class FeedAdaptor(var context: Context, var blogDetails:ArrayList<Blog>, var blogsInterface: feedItem?): RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    private val YPlayer: YouTubePlayer? = null
    private val YoutubeDeveloperKey = "xyz"
    private val RECOVERY_DIALOG_REQUEST = 1

    override fun getItemCount(): Int {
        return blogDetails.size
    }

    interface feedItem{

        fun onClickBlog(blogData: Blog)

        fun onCommentClicked(blogData: Blog)

    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        // Log.i("Adaptor viewtype ", Integer.toString(viewType))

        val v = LayoutInflater.from(context).inflate(R.layout.blog_item, parent, false)
        return Item(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {

        (holder as Item).bindData(blogDetails[position]!!,context, this!!.blogsInterface!!)

    }

    class Item(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindData(blogData: Blog, context: Context, blogsInterface: feedItem) {

            Log.e("Blog type ", blogData.blog_type)

            if (blogData.blog_type.equals("")) {
                itemView.blogThumbNailImage.visibility = View.VISIBLE
                itemView.blogTitle.text = blogData.title
                itemView.blogType.text = blogData.blog_type


            } else {

                if (blogData.blog_type.trim() == "videos") {

                    itemView.blogType.text = "Fitness Videos"
                    itemView.blogThumbNailImage.visibility = View.INVISIBLE
                    itemView.blogVideoView.visibility = View.VISIBLE
                    itemView.blogTypeImage.setImageResource(R.drawable.ic_fitnessvideo)

                    //set the video view url
                    val uri = Uri.parse(blogData.video_url)
                    Log.e("TAG VIDEO", uri.toString())
//                itemView.blogThumbNailVideoImage.setVideoURI(uri)
//                itemView.blogThumbNailVideoImage.seekTo(100)

                    //created a youtube thumbnail view which will show video thumbnails
                    val onThumbnailLoadedListener = object : YouTubeThumbnailLoader.OnThumbnailLoadedListener {
                        override fun onThumbnailError(youTubeThumbnailView: YouTubeThumbnailView, errorReason: YouTubeThumbnailLoader.ErrorReason) {

                        }

                        override fun onThumbnailLoaded(youTubeThumbnailView: YouTubeThumbnailView, s: String) {
                            youTubeThumbnailView.visibility = View.VISIBLE
                            itemView.youtubeVideoThumbnail.setVisibility(View.VISIBLE)
                        }
                    }

                    itemView.youtubeVideoThumbnail.initialize(YoutubeConfig.getID(), object : YouTubeThumbnailView.OnInitializedListener {
                        override fun onInitializationSuccess(youTubeThumbnailView: YouTubeThumbnailView, youTubeThumbnailLoader: YouTubeThumbnailLoader) {

                            youTubeThumbnailLoader.setVideo(YoutubeConfig.getyouTubeVideokey(blogData.video_url))
                            youTubeThumbnailLoader.setOnThumbnailLoadedListener(onThumbnailLoadedListener)

                            //  youTubeThumbnailLoader.set
                        }

                        override fun onInitializationFailure(youTubeThumbnailView: YouTubeThumbnailView, youTubeInitializationResult: YouTubeInitializationResult) {
                            //write something for failure
                        }
                    })

                    itemView.youtubeVideoThumbnail.setOnClickListener {


                        val intent = YouTubeStandalonePlayer.createVideoIntent((context) as Activity, "1", YoutubeConfig.getyouTubeVideokey(blogData.video_url))
                        context.startActivity(intent)
                        Log.e("Set on click", "hello")
                    }

                    // FeedAdaptor.YoutuubeInitialise(itemView.youtubeVideoThumbnail)

//                YoutubeInitialise.setYouTubePlayerView(
//                        itemView.youtubeVideoThumbnail,
//                        YoutubeConfig.getyouTubeVideokey(blogData.video_url),
//                        YoutubeConfig.getID())


//                itemView.youtubeVideoThumbnail.initialize(YoutubeConfig.getID(),object :YouTubePlayer.OnInitializedListener{
//                    override fun onInitializationSuccess(p0: YouTubePlayer.Provider?, p1: YouTubePlayer?, p2: Boolean) {
//
//                        if (p1 != null) {
//                            p1.loadVideo(YoutubeConfig.getyouTubeVideokey(blogData.video_url))
//                        }
//
//
//                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//                    }
//
//                    override fun onInitializationFailure(p0: YouTubePlayer.Provider?, p1: YouTubeInitializationResult?) {
//                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//                    }
//
//                })


//
//                itemView.youtubeVideo.initialize(YoutubeConfig.getID(), object : YouTubePlayer.OnInitializedListener{
//                    override fun onInitializationSuccess(p0: YouTubePlayer.Provider?, p1: YouTubePlayer?, p2: Boolean) {
//
//                        Log.e("Youtube Sucess",p2.toString())
//
//                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//                    }
//
//                    override fun onInitializationFailure(p0: YouTubePlayer.Provider?, p1: YouTubeInitializationResult?) {
//                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//                    }
//
//
//                })


                } else {
                    //text and images type blog

                    itemView.blogThumbNailImage.visibility = View.VISIBLE
                    itemView.blogVideoView.visibility = View.INVISIBLE


                    if (blogData.blog_type.trim() == "images") {
                        itemView.blogType.text = "Fitness Blog Images"
                        itemView.blogTypeImage.setImageResource(R.drawable.ic_photo100)

                    } else {
                        itemView.blogType.text = "Fitness Blog "
                        itemView.blogTypeImage.setImageResource(R.drawable.fitness_blog)


                        itemView.setOnClickListener {

                            blogsInterface.onClickBlog(blogData)

                        }
                    }


                    Log.e("Url  ", blogData.thumbnail_image + blogData.title)
                    //load thumnail
                    HelperUtils.loadImage(blogData.thumbnail_image, context, itemView.blogThumbNailImage)

                    //title
                    itemView.blogTitle.text = blogData.title
                    //comment
                    itemView.commentSection.setOnClickListener {

                        //open comment fragment
                        blogsInterface.onCommentClicked(blogData = blogData)
                    }
                }

                val date = blogData.blog_publish_date.split("T").get(0)

                Log.e("DAte", date)


                val milliseconds = HelperUtils.getTimeInMills(date, "yyyy-mm-dd")



                itemView.blogDate.text = HelperUtils.getDateInFormat(milliseconds, "dd-MMM").toString()


            }


        }

    }

}


