package com.example.itstym.perbmember.Water.Adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.util.Log
import com.example.itstym.perbmember.Water.WaterAnalysis.WaterAnalysisFragment
import com.example.itstym.perbmember.Water.WaterLog.WaterLogFragment

/**
 * Created by itstym on 11/12/17.
 */

class ViewPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {
    override fun getPageTitle(position: Int): CharSequence? {
        super.getPageTitle(position)

        return when(position){

            0 -> "Logs"

            1 -> "Analysis"
            else -> "Logs"
        }
    }

    override fun getItem(position: Int): Fragment {
            Log.e("position",position.toString())
        return when(position){

            0 -> {
                WaterLogFragment.newInstance()
            }

            1 -> {

                WaterAnalysisFragment.newInstance()
            }

            else -> {
                WaterLogFragment.newInstance()
            }
        }


    }

    override fun getCount(): Int {
        return 2;
    }


}