package com.example.itstym.perbmember.DashboardBottomDialog

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Gravity
import com.example.itstym.perbmember.Dashboard.Adaptor.BottomSheetDialogAdaptor
import com.example.itstym.perbmember.Meal.MealActivity
import com.example.itstym.perbmember.Network.Model.BottomSheetModel
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.Utils.HelperUtils
import com.example.itstym.perbmember.Water.WaterActivity
import com.example.itstym.perbmember.Weight.WeightActivity
import com.example.itstym.perbmember.Workout.WorkoutActivity
import kotlinx.android.synthetic.main.dashboard_bottom_sheet_dialog.*

class DashboardDialog: Dialog {


    var mContext: Context? = null

    constructor(context: Context) : super(context) {
        mContext = context

    }

    constructor(context: Context, themeResId: Int) : super(context, themeResId) {
        mContext = context
    }

//    var selectedPromotion: Promotion? = null

//    constructor(context: Context, themeResId: Int) : super(context, themeResId) {
//
//        //selectedPromotion = promotion
//        mContext = context
//
//    }

    var isCustomText = false

    constructor(context: Context, themeResId: Int, isCustomText: Boolean) : super(context, themeResId) {

        mContext = context
        this.isCustomText = isCustomText

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
        super.onCreate(savedInstanceState)

        setContentView(R.layout.dashboard_bottom_sheet_dialog)// use your layout in place of this.
        setUp()

    }
    var message: String? = null
    private fun setUp() {

        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));

        val windowlp = window.attributes

        windowlp.gravity = Gravity.BOTTOM
        window.attributes = windowlp

        window.setWindowAnimations(R.style.dialog_animation_fade)
        itemRecyclerView.layoutManager = LinearLayoutManager(mContext) as RecyclerView.LayoutManager?
        itemRecyclerView.adapter= BottomSheetDialogAdaptor(context!!, getBottomSheetitem(),  object :BottomSheetDialogAdaptor.BottomSheetListener{
            override fun onBottomItemClick(position: Int) {

                Log.e("Item clicked ",position.toString())

                when(position){

                    0 -> openWorkOutActivity()

                    1 -> openMealActivity()

                    2 -> openTrackWaterActivity()

                    3 -> openWeightActivity()
                }
            }


        })


        fabDone.setOnClickListener {
            dismiss()
        }


    }

    private fun openWeightActivity() {
        Log.e("Weight","Opened")
        val intent = WeightActivity.getStartIntent(context!!)

        mContext!!.startActivity(intent)
        dismiss()
    }

    private fun openTrackWaterActivity() {
        Log.e("Water","Opened")
        val intent = WaterActivity.getStartIntent(context!!)

        mContext!!.startActivity(intent)
        dismiss()


    }

    private fun openMealActivity() {
        Log.e("Meal","Opened")
        val intent = MealActivity.getStartIntent(context!!)

        mContext!!.startActivity(intent)
        HelperUtils.showSnackBar("Meal Section Comming Soon", mContext as Activity)
        dismiss()

    }

    private fun openWorkOutActivity() {
        Log.e("Workout","Opened")
       val intent = WorkoutActivity.getStartIntent(context!!)
        HelperUtils.showSnackBar("Workout Section Comming Soon", mContext as Activity)

        mContext!!.startActivity(intent)
        dismiss()

    }

    private fun getBottomSheetitem(): ArrayList<BottomSheetModel> {

        var arrayList = ArrayList<BottomSheetModel>()

        val BottomSheetItemOne = BottomSheetModel(1, "Add/Edit Workout")
        arrayList.add(BottomSheetItemOne)

        val BottomSheetItemTwo = BottomSheetModel(2, "Add Meal")
        arrayList.add(BottomSheetItemTwo)

        val BottomSheetItemThree = BottomSheetModel(3, "Track Water")
        arrayList.add(BottomSheetItemThree)

        val BottomSheetItemFour = BottomSheetModel(4, "Track Weight")
        arrayList.add(BottomSheetItemFour)


        return arrayList;
    }

    override fun onDetachedFromWindow() {
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
        super.onDetachedFromWindow()
    }
}




