package com.example.itstym.perbmember.TransactionHistory

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.example.itstym.perbmember.Base.Activity.BaseActivity
import com.example.itstym.perbmember.Network.Model.Transaction
import com.example.itstym.perbmember.PerbMemberApplication
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.TransactionHistory.Adaptor.TransactionAdaptorShimmer
import com.example.itstym.perbmember.TransactionHistory.Presenter.TransactionHistoryPresenter
import com.example.itstym.perbmember.TransactionHistory.View.TransactionHistoryView
import kotlinx.android.synthetic.main.transaction_history_layout.*

/**
 * Created by itstym on 11/12/17.
 */


class TransactionHistoryActivity: BaseActivity(), TransactionHistoryView {

    override fun setUpToolbar() {

    }
    override fun setUp() {
        val dataManager = (application as PerbMemberApplication).getDataManager()

        mTransactionPresenter = TransactionHistoryPresenter(dataManager)

        mTransactionPresenter.onAttach(this)
        transactionHistoryRecyclerView.layoutManager= LinearLayoutManager(this@TransactionHistoryActivity)
        var demoTransaction:ArrayList<Transaction> = ArrayList()
        for(i in 1..2){

            demoTransaction.add(Transaction("",0,"",0,"","",0))

        }
        transactionHistoryRecyclerView.adapter = TransactionAdaptorShimmer(this@TransactionHistoryActivity,demoTransaction)

        mTransactionPresenter.setUpRecyclerView(this@TransactionHistoryActivity,transactionHistoryRecyclerView,verticleBar)
        imageView12.setOnClickListener {
            onBackPressed()
        }
        closeActivity.setOnClickListener {
            finish()
        }
    }

    companion object {
        fun getStartIntent(context: Context) : Intent = Intent(context, TransactionHistoryActivity::class.java)
    }

    lateinit var  mTransactionPresenter: TransactionHistoryPresenter<TransactionHistoryView>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.transaction_history_layout)

        setUp()
    }

    override fun onResume() {
        super.onResume()

    }



}
