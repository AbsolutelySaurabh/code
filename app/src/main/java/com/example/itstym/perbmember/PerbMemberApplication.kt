package com.example.itstym.perbmember

import android.app.Activity
import android.app.Application
import android.content.pm.ActivityInfo
import android.os.Bundle
import com.facebook.stetho.Stetho

/**
 * Created by itstym on 6/12/17.
 */

class PerbMemberApplication: Application(){

    lateinit var dataManger: DataManager

    override fun onCreate() {
        super.onCreate()

        Stetho.initializeWithDefaults(this);

        val sharedPrefHelper: SharedPrefsHelper = SharedPrefsHelper(applicationContext)
        dataManger=DataManager(sharedPrefHelper)

        registerActivityLifecycleCallbacks(object :ActivityLifecycleCallbacks{
            override fun onActivityResumed(p0: Activity?) {
            }

            override fun onActivityStarted(p0: Activity?) {
            }

            override fun onActivityDestroyed(p0: Activity?) {
            }

            override fun onActivitySaveInstanceState(p0: Activity?, p1: Bundle?) {
            }

            override fun onActivityStopped(p0: Activity?) {
            }

            override fun onActivityPaused(p0: Activity?) {
            }

            override fun onActivityCreated(p0: Activity?, p1: Bundle?) {
                p0?.requestedOrientation= ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            }

        })
    }

    fun getDataManager():DataManager{

        return dataManger
    }

}