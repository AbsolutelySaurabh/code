package com.example.itstym.perbmember.ViewWorkout.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by itstym on 7/12/17.
 */

data class Exercise(


        @SerializedName("exercise_id")
        @Expose
        val exercise_id:Int,

        @SerializedName("exercise_name")
        @Expose
        val exercise_name:String,


        @SerializedName("exercise_url")
        @Expose
        val exercise_url:String,

        @SerializedName("body_part_id")
        @Expose
        val body_part_id:Int,

        @SerializedName("selected")
        @Expose
        val selected:Boolean,

        @SerializedName("selected_days")
        @Expose
        val selected_days:ArrayList<Int>

)