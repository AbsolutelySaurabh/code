package com.example.itstym.perbmember.Base.Dialog.View

import com.example.itstym.perbmember.Base.Activity.View.MvpView

/**
 * Created by itstym on 6/12/17.
 */


interface DialogView:MvpView{

    fun setup()
}