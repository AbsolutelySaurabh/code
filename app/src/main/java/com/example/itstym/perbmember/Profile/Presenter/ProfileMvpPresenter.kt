package com.example.itstym.perbmember.Profile.Presenter

import android.content.Context
import android.support.v7.widget.RecyclerView
import com.example.itstym.perbmember.Base.Activity.Presenter.MvpPresenter
import com.example.itstym.perbmember.Feed.View.FeedView
import com.example.itstym.perbmember.Profile.View.ProfileView

/**
 * Created by itstym on 11/12/17.
 */


interface ProfileMvpPresenter<in V: ProfileView> : MvpPresenter<V> {


}