package com.example.itstym.perbmember.Blog.Presenter

import android.content.Context
import android.widget.ImageView
import com.example.itstym.perbmember.Base.Activity.Presenter.MvpPresenter
import com.example.itstym.perbmember.Blog.View.BlogView
import com.example.itstym.perbmember.Splash.View.SplashMvpView

/**
 * Created by itstym on 10/12/17.
 */


interface BlogMvpPresenter<in V: BlogView> : MvpPresenter<V> {

}