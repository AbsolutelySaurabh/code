package com.example.itstym.perbmember.Network.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by mukul on 1/4/18.
 */
data class Version(

        @SerializedName("is_mobile_os_android")
        @Expose
        val mobileType:Int,

        @SerializedName("app_version")
        @Expose
        val appVersion:String,

        @SerializedName("is_trainer_app")
        @Expose
        val trainerApp:Int

)