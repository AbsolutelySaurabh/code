package com.example.itstym.perbmember.Workout.Adaptor

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.Workout.Model.BodyPart
import kotlinx.android.synthetic.main.bodypartshimmer.view.*

/**
 * Created by itstym on 6/12/17.
 */


class BodyPartShimmer(var context: Context, var bodyPartData:ArrayList<BodyPart>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun getItemCount(): Int {
        return bodyPartData.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        // Log.i("Adaptor viewtype ", Integer.toString(viewType))

        val v = LayoutInflater.from(context).inflate(R.layout.bodypartshimmer, parent, false)
        return ItemShimmer(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {

        (holder as ItemShimmer).bindData(context, bodyPartData[position])
    }

}

class ItemShimmer(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindData(context: Context, bodyPart: BodyPart) {

        itemView.bodypartShimmer.startShimmerAnimation()


    }



}