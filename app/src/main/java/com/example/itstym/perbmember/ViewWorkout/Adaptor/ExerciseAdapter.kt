package com.example.itstym.perbmember.ViewWorkout.Adaptor

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.ViewWorkout.Model.Exercise
import com.example.itstym.perbmember.ViewWorkout.WorkoutImage
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.exercise_item.view.*

/**
 * Created by itstym on 6/12/17.
 */


class ExerciseAdapter(var context: Context, var exerciseData:ArrayList<Exercise>, var exerciseUpdated: ExerciseUpdated,var selectedDays:Int): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    interface ExerciseUpdated{

        fun actionOnExercise(exercise: Exercise,isSelected:Boolean)
    }

    override fun getItemCount(): Int {
        return exerciseData.size+2
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        // Log.i("Adaptor viewtype ", Integer.toString(viewType))

        val v = LayoutInflater.from(context).inflate(R.layout.exercise_item, parent, false)
        return ItemCombat(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {

        if (position<exerciseData.size)
            (holder as ItemCombat).bindData(context,exerciseData.get(position),exerciseData.size,exerciseUpdated,selectedDays)
        else
            (holder as ItemCombat).bindData(context, Exercise(0,"djsf","dsjfhg",1,false, ArrayList()),100,exerciseUpdated,selectedDays)
    }

}

class ItemCombat(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindData( context: Context,exercise: Exercise, length:Int, exerciseUpdated: ExerciseAdapter.ExerciseUpdated, selectedDay: Int) {

        if (adapterPosition<length){
            itemView.exerciseName.text=exercise.exercise_name
            itemView.action.visibility = View.GONE



            if (!exercise.selected_days.contains(selectedDay)){
                itemView.imageView6.visibility = View.GONE
                itemView.exerciseName.visibility = View.GONE


            }


            loadImage(exercise.exercise_url,context,itemView.imageView6)


            itemView.imageView6.setOnClickListener{
                Log.e("Clicked","YES")
                WorkoutImage(context,exercise.exercise_url,R.style.ThemeDialog).show()


            }
        }else{

            itemView.exerciseName.visibility=View.INVISIBLE

        }



    }

    private fun loadImage(imageIcon: String, context: Context, imageView: ImageView) {

        Picasso.with(context).load(imageIcon).into(imageView)
    }

}