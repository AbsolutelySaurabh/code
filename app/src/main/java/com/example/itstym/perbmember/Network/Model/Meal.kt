package com.example.itstym.perbmember.Network.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by itstym on 13/12/17.
 */

data class Meal(
        @SerializedName("selected_day")
        @Expose
        val selectedDays:Int,

        @SerializedName("diet_id")
        @Expose
        val dietPlanId:Int,

        @SerializedName("diet_name")
        @Expose
        val dietPlanName:String,

        @SerializedName("meal_id")
        @Expose
        val mealId:Int,

        @SerializedName("meal_name")
        @Expose
        val mealName:String,

        @SerializedName("alarm_time")
        @Expose
        val mealTime:String,

        @SerializedName("food_id")
        @Expose
        val foodId:Int,

        @SerializedName("food_item")
        @Expose
        val foodItem:String,


        @SerializedName("gym_id")
        @Expose
        val gymId:Int

)
