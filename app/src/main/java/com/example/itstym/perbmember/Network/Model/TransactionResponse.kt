package com.example.itstym.perbmember.Network.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by itstym on 11/12/17.
 */


data class TransactionResponse(

        @SerializedName("status")
        @Expose
        val status:Boolean,

        @SerializedName("transaction_history")
        @Expose
        val transactionHistoryArrayList:ArrayList<Transaction>
)