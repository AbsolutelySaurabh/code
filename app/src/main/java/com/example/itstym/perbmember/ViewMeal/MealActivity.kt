//package com.example.itstym.perbmember.ViewMeal
//
//import android.content.Context
//import android.content.Intent
//import android.os.Bundle
//import android.support.v4.content.ContextCompat
//import android.support.v7.widget.LinearLayoutManager
//import android.util.Log
//import android.view.MenuItem
//import android.view.View
//import android.widget.Button
//import android.widget.TextView
//import android.widget.Toast
//import com.example.itstym.perbmember.Base.Activity.BaseActivity
//import com.example.itstym.perbmember.DataManager
//import com.example.itstym.perbmember.Network.Model.Diet
//import com.example.itstym.perbmember.Network.Model.Meal
//import com.example.itstym.perbmember.PerbMemberApplication
//import com.example.itstym.perbmember.R
//import com.example.itstym.perbmember.Utils.HelperUtils
//import com.example.itstym.perbmember.ViewMeal.Adaptor.DietPlanShimmer
//import com.example.itstym.perbmember.ViewMeal.Adaptor.FoodItemAdaptor
//import com.example.itstym.perbmember.ViewMeal.Presenter.MealPresenter1
//import com.example.itstym.perbmember.ViewMeal.View.MealView1
//import kotlinx.android.synthetic.main.days.*
//import kotlinx.android.synthetic.main.meal_activity_inside_layout.*
//import kotlinx.android.synthetic.main.meal_activity_layout.*
//
///**
// * Created by itstym on 12/12/17.
// */
//
//
//class MealActivity: BaseActivity(), MealView1 {
//
//    override fun showToast(message: String) {
//
//        Toast.makeText(this@MealActivity, message,Toast.LENGTH_LONG).show()
//
//    }
//
//
//
//
//
//    override fun onResume() {
//        super.onResume()
//
//
//
//        //
//        mon.setOnClickListener {
//            setAllDaysDisable()
//            setUpTextColor(mon, R.color.colorAccent)
//            mMealPresenter.getAllMeals(this@MealActivity,recyclerViewDietPlan,recyclerViewMealType,1)
//
//            //mMealPresenter.upDateDietPlanView(1,this@MealActivity)
//        }
//
//        tue.setOnClickListener {
//            setAllDaysDisable()
//            setUpTextColor(tue, R.color.colorAccent)
//            mMealPresenter.getAllMeals(this@MealActivity,recyclerViewDietPlan,recyclerViewMealType,2)
//
//            // mMealPresenter.upDateDietPlanView(2,this@MealActivity)
//        }
//
//        wed.setOnClickListener {
//            setAllDaysDisable()
//            setUpTextColor(wed, R.color.colorAccent)
//            mMealPresenter.getAllMeals(this@MealActivity,recyclerViewDietPlan,recyclerViewMealType,3)
//
//            // mMealPresenter.upDateDietPlanView(3,this@MealActivity)
//        }
//
//        thu.setOnClickListener {
//            setAllDaysDisable()
//            mMealPresenter.getAllMeals(this@MealActivity,recyclerViewDietPlan,recyclerViewMealType,4)
//
//            setUpTextColor(thu, R.color.colorAccent)
//                    //  mMealPresenter.upDateDietPlanView(4,this@MealActivity)
//        }
//
//        fri.setOnClickListener {
//            setAllDaysDisable()
//            setUpTextColor(fri, R.color.colorAccent)
//            mMealPresenter.getAllMeals(this@MealActivity,recyclerViewDietPlan,recyclerViewMealType,5)
//
//            // mMealPresenter.upDateDietPlanView(5,this@MealActivity)
//        }
//
//        sat.setOnClickListener {
//            setAllDaysDisable()
//            setUpTextColor(sat, R.color.colorAccent)
//            mMealPresenter.getAllMeals(this@MealActivity,recyclerViewDietPlan,recyclerViewMealType,6)
//
//
//            //  mMealPresenter.upDateDietPlanView(6,this@MealActivity)
//        }
//
//        sun.setOnClickListener {
//            setAllDaysDisable()
//            setUpTextColor(sun, R.color.colorAccent)
//            mMealPresenter.getAllMeals(this@MealActivity,recyclerViewDietPlan,recyclerViewMealType,7)
//
//            //   mMealPresenter.upDateDietPlanView(7,this@MealActivity)
//        }
//
//
//    }
//
//    private fun setAllDaysDisable() {
//
//        setUpTextColor(mon, R.color.newColorIntroducedDesigner)
//        setUpTextColor(tue, R.color.newColorIntroducedDesigner)
//        setUpTextColor(wed, R.color.newColorIntroducedDesigner)
//        setUpTextColor(thu, R.color.newColorIntroducedDesigner)
//        setUpTextColor(fri, R.color.newColorIntroducedDesigner)
//        setUpTextColor(sat, R.color.newColorIntroducedDesigner)
//        setUpTextColor(sun, R.color.newColorIntroducedDesigner)
//
//    }
//
//
//
//    lateinit var dataManager: DataManager
//    private lateinit var mMealPresenter: MealPresenter1<MealView1>
//
//    override fun setUp() {
//
//        setUpToolbar()
//
//
//
//        dataManager = (application as PerbMemberApplication).getDataManager()
//        mMealPresenter = MealPresenter1(dataManager)
//        mMealPresenter.onAttach(this)
//        val dietplandemo : ArrayList<Diet> = ArrayList()
//        val mealdemo : ArrayList<Meal> = ArrayList()
//        for(i in 1..3){
//            dietplandemo.add(Diet(0,""))
//            mealdemo.add(Meal(0,0,"","","",""))
//        }
//        recyclerViewDietPlan.layoutManager = LinearLayoutManager(this@MealActivity,LinearLayoutManager.HORIZONTAL,false)
//        recyclerViewDietPlan.adapter = DietPlanShimmer(this@MealActivity,dietplandemo)
//        recyclerViewMealType.layoutManager = LinearLayoutManager(this@MealActivity)
//        recyclerViewMealType.adapter  = FoodItemAdaptor(this@MealActivity,mealdemo)
//
//
//
//       // Log.e("Today Day Meal",HelperUtils.getTodayDay().toString())
//        mMealPresenter.getAllMeals(this@MealActivity,recyclerViewDietPlan,recyclerViewMealType,HelperUtils.getTodayDay()-1)
//        var days:ArrayList<Int> = ArrayList()
//        days.add(1)
//        days.add(2)
//
//
//
//        mMealPresenter.setTodayDay()
//    }
//
//
//
//    override fun highLightTodayDay(todayDays: Int) {
//        Log.e("Today day",todayDays.toString())
//
//        when(todayDays-1){
//
//            0 -> setUpTextColor(sun, R.color.colorAccent)
//
//            1 -> setUpTextColor(mon, R.color.colorAccent)
//
//            2 -> setUpTextColor(tue, R.color.colorAccent)
//
//            3 -> setUpTextColor(wed, R.color.colorAccent)
//
//            4 -> setUpTextColor(thu, R.color.colorAccent)
//
//            5 -> setUpTextColor(fri, R.color.colorAccent)
//
//            6 -> setUpTextColor(sat, R.color.colorAccent)
//        }
//
//    }
//
//    private fun setUpTextColor(textView: TextView, colorCode: Int){
//
//        textView.setTextColor(ContextCompat.getColor(this@MealActivity, colorCode))
//    }
//
//
//    override fun setUpToolbar() {
//
//        toolbarmeal.title="Meal".toUpperCase()
//        setSupportActionBar(toolbarmeal)
//
//        supportActionBar?.setDisplayHomeAsUpEnabled(true)
//        // supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
//        supportActionBar?.setHomeButtonEnabled(true)
//    }
//
//
//
//    companion object {
//        fun getStartIntent(context: Context) : Intent = Intent(context, MealActivity::class.java)
//    }
//
//    lateinit var submitButton:Button
//
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.meal_activity_layout)
//        submitButton = findViewById<Button>(R.id.workoutSubmitButton) as Button
//        submitButton.visibility = View.GONE
//
//
//
//        setUp()
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        when (item.getItemId()) {
//            android.R.id.home -> {
//                // API 5+ solution
//                onBackPressed()
//                return true
//            }
//
//            else -> return super.onOptionsItemSelected(item)
//        }
//    }
//}