package com.example.itstym.perbmember.Meal

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.constraint.ConstraintLayout
import android.support.design.widget.BottomSheetDialog
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.example.itstym.perbmember.Base.Activity.BaseActivity
import com.example.itstym.perbmember.Dashboard.DashboardActivity
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.Meal.Adaptor.DaysAdaptorMeal
import com.example.itstym.perbmember.Meal.Model.MealViewPlan
import com.example.itstym.perbmember.Meal.Presenter.MealPresenter
import com.example.itstym.perbmember.Meal.View.MealView
import com.example.itstym.perbmember.Meal.newmodel.SendMealModel
import com.example.itstym.perbmember.PerbMemberApplication
import com.example.itstym.perbmember.R
import kotlinx.android.synthetic.main.days.*
import kotlinx.android.synthetic.main.meal_activity_inside_layout.*
import kotlinx.android.synthetic.main.meal_activity_layout.*
import me.zhanghai.android.materialprogressbar.MaterialProgressBar
import java.util.*


/**
 * Created by itstym on 12/12/17.
 */


class MealActivity : BaseActivity(), MealView {

    override fun addMemberDietPlan(selectedDietPlanGlobalObject: SendMealModel?) {
        val mBottomSheetDialog = BottomSheetDialog(this@MealActivity)
        var todayDay: Int
        daysSelected.clear()

        Log.e("Selcetd days",selectedDietPlanGlobalObject?.days.toString())

        for (i in selectedDietPlanGlobalObject!!.days!!) {

            daysSelected.add(i as Int)
            Log.e("position", i.toString())


        }

        if (selectedDietPlanGlobalObject.days!!.size >= 1) {
            Log.d("Today day ", selectedDietPlanGlobalObject.days!!.get(selectedDietPlanGlobalObject.days!!.size - 1).toString() + "vgvug")
            todayDay = selectedDietPlanGlobalObject.days!!.get(selectedDietPlanGlobalObject.days!!.size - 1) as Int
        } else {
            todayDay = 0
        }

        val sheetView = layoutInflater.inflate(R.layout.workout_bottom_sheet, null)

        val dayRecyclerView = sheetView.findViewById<View>(R.id.daysRecyclerView) as RecyclerView
        val numDaysSelected = sheetView.findViewById<View>(R.id.numSelectedDays) as TextView
        val bottomSubTitle = sheetView.findViewById<View>(R.id.bottomSheetSubTitle) as TextView
        val progressBar = sheetView.findViewById<View>(R.id.progressBarbottomsheet) as MaterialProgressBar

        //val dayRecyclerView=sheetView.findViewById<View>(R.id.daysRecyclerView) as RecyclerView

        bottomSubTitle.text = "Select the days you wish  to follow \n the same diet plan"

        //val numdays=(mWorkoutPresenter.selectedDays.size)-1

        dayRecyclerView.setHasFixedSize(true)
        dayRecyclerView.layoutManager = LinearLayoutManager(this@MealActivity, LinearLayoutManager.HORIZONTAL, false)

        dayRecyclerView.adapter = DaysAdaptorMeal(this@MealActivity, mMealPresenter.getDays(), object : DaysAdaptorMeal.DaysSelectedMeal {
            override fun onDaysSelected(position: Int, selected: Boolean) {


                if (daysSelected.contains(position)!!) {
                    daysSelected.remove(position)
                }
                else{
                    daysSelected.add(position)
                }


//                for(i in daysSelected){
//                    selectedDietPlanGlobalObject?.days.add(i)
//                }

                for (i in daysSelected) {
                    Log.e("position", i.toString())
                }


                numDaysSelected.text = "Selected " + daysSelected.size + " Days"
            }

        }, todayDay)

        val confirmButton = sheetView.findViewById<ConstraintLayout>(R.id.confirmButton) as ConstraintLayout

        confirmButton.setOnClickListener {
            if (daysSelected.size > 0) {
                selectedDietPlanGlobalObject?.days!!.clear()


                for (i in daysSelected) {

                    selectedDietPlanGlobalObject?.days!!.add(i + 1)
                    Log.e("send data ", i.toString())


                }
                //  Log.e("send data ",selectedDietPlanGlobalObject.dietPlanId.toString())
                // move to next bottom
                mMealPresenter.sendMeal(this@MealActivity, selectedDietPlanGlobalObject, progressBar, mBottomSheetDialog)
                //  mBottomSheetDialog.dismiss()

            } else
                Toast.makeText(baseContext, "No days selected", Toast.LENGTH_SHORT).show()
            //

        }

        mBottomSheetDialog.setContentView(sheetView)

        mBottomSheetDialog.show()

    }





/* override fun addCustomMeal() {

     val customMealDailog= BottomSheetDialog(this@MealActivity)



     val customSheetView= layoutInflater.inflate(R.layout.customaddmeal_bottom_sheet,null)

     val getMealText=customSheetView.findViewById<EditText>(R.id.textInputText) as EditText

     val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
     imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)


     val confirmButton=customSheetView.findViewById<ConstraintLayout>(R.id.addMealButton) as ConstraintLayout
     confirmButton.isEnabled = false
     getMealText.addTextChangedListener(object : TextWatcher {

         override fun afterTextChanged(s: Editable) {}

         override fun beforeTextChanged(s: CharSequence, start: Int,
                                        count: Int, after: Int) {
         }

         override fun onTextChanged(s: CharSequence, start: Int,
                                    before: Int, count: Int) {
             if (s.toString().length>0){


                 confirmButton.isEnabled = true
                 confirmButton.setBackgroundColor(resources.getColor(R.color.colorAccent))


             }
             else{
                 confirmButton.isEnabled = false
                 confirmButton.setBackgroundColor(resources.getColor(R.color.shimmersColor))


             }
         }
     })

     var currentCustomDietPlanid = -2

     confirmButton.setOnClickListener {

         mMealPresenter.addMeal(context = this@MealActivity, addMeal = getMealText.text.toString(), customDietPlanid = --currentCustomDietPlanid, alreadyAdded = false, mealItem = null)
         customMealDailog.dismiss()

     }
//        getMealText.setOnEditorActionListener { v, actionId, event ->
//            if(actionId == EditorInfo.IME_ACTION_GO){
//                true
//            } else {
//                false
//            }
//        }


     customMealDailog.setContentView(customSheetView)


     customMealDailog.show()

     customMealDailog.setOnDismissListener{
         getWindow().setSoftInputMode(
                 WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
         )
         val view = this.currentFocus
         if (view != null) {
             val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
             imm.hideSoftInputFromWindow(view.windowToken, 0)
         }
     }


 }*/

override fun showToast(message: String) {

    Toast.makeText(this@MealActivity, message, Toast.LENGTH_LONG).show()

}


override fun showConfirmDialog(selectedDietPlanGlobalObject: MealViewPlan?) {

    val mBottomSheetDialog = BottomSheetDialog(this@MealActivity)

    val sheetView = layoutInflater.inflate(R.layout.workout_confirm_layout, null)

    mBottomSheetDialog.setContentView(sheetView)

    Handler().postDelayed({
        mBottomSheetDialog.dismiss()

        startActivity(DashboardActivity.getStartIntent(this@MealActivity))
        finish()
    }, 3000);

    mBottomSheetDialog.show()
}


val daysSelected: ArrayList<Int> = ArrayList()

/* override fun addMemberDietPlan(selectedDietPlanGlobalObject: SendDATA?) {


 } */

 override fun onResume() {
     super.onResume()

     submitButton.setOnClickListener {

         //Log.e("Selected days ",mMealPresenter.selectedDays.toString())
         mMealPresenter.mealSubmitClicked(this@MealActivity)
     }

     //
     mon.setOnClickListener {
         setAllDaysDisable()
         setUpTextColor(mon, R.color.colorAccent)
         mMealPresenter.getAllMeals(this@MealActivity,recyclerViewDietPlan,recyclerViewMealType,1)

         //mMealPresenter.upDateDietPlanView(1,this@MealActivity)
     }

     tue.setOnClickListener {
         setAllDaysDisable()
         setUpTextColor(tue, R.color.colorAccent)
         mMealPresenter.getAllMeals(this@MealActivity,recyclerViewDietPlan,recyclerViewMealType,2)

         // mMealPresenter.upDateDietPlanView(2,this@MealActivity)
     }

     wed.setOnClickListener {
         setAllDaysDisable()
         setUpTextColor(wed, R.color.colorAccent)
         mMealPresenter.getAllMeals(this@MealActivity,recyclerViewDietPlan,recyclerViewMealType,3)

         // mMealPresenter.upDateDietPlanView(3,this@MealActivity)
     }

     thu.setOnClickListener {
         setAllDaysDisable()
         mMealPresenter.getAllMeals(this@MealActivity,recyclerViewDietPlan,recyclerViewMealType,4)

         setUpTextColor(thu, R.color.colorAccent)
                 //  mMealPresenter.upDateDietPlanView(4,this@MealActivity)
     }

     fri.setOnClickListener {
         setAllDaysDisable()
         setUpTextColor(fri, R.color.colorAccent)
         mMealPresenter.getAllMeals(this@MealActivity,recyclerViewDietPlan,recyclerViewMealType,5)

         // mMealPresenter.upDateDietPlanView(5,this@MealActivity)
     }

     sat.setOnClickListener {
         setAllDaysDisable()
         setUpTextColor(sat, R.color.colorAccent)
         mMealPresenter.getAllMeals(this@MealActivity,recyclerViewDietPlan,recyclerViewMealType,6)


         //  mMealPresenter.upDateDietPlanView(6,this@MealActivity)
     }

     sun.setOnClickListener {
         setAllDaysDisable()
         setUpTextColor(sun, R.color.colorAccent)
         mMealPresenter.getAllMeals(this@MealActivity,recyclerViewDietPlan,recyclerViewMealType,7)

         //   mMealPresenter.upDateDietPlanView(7,this@MealActivity)
     }


 }

 private fun setAllDaysDisable() {

     setUpTextColor(mon, R.color.newColorIntroducedDesigner)
     setUpTextColor(tue, R.color.newColorIntroducedDesigner)
     setUpTextColor(wed, R.color.newColorIntroducedDesigner)
     setUpTextColor(thu, R.color.newColorIntroducedDesigner)
     setUpTextColor(fri, R.color.newColorIntroducedDesigner)
     setUpTextColor(sat, R.color.newColorIntroducedDesigner)
     setUpTextColor(sun, R.color.newColorIntroducedDesigner)

 }



 lateinit var dataManager: DataManager
 private lateinit var mMealPresenter: MealPresenter<MealView>
 lateinit var submitButton:Button

 override fun setUp() {

     setUpToolbar()


     dataManager = (application as PerbMemberApplication).getDataManager()
     mMealPresenter = MealPresenter(dataManager)

     submitButton = findViewById<Button>(R.id.workoutSubmitButton) as Button

     mMealPresenter.onAttach(this)

     /*val dietplandemo : ArrayList<Diet> = ArrayList()
     val mealdemo : ArrayList<Meal> = ArrayList()
     for(i in 1..3){
         dietplandemo.add(Diet(0,""))
         mealdemo.add(Meal(0,0,"","","",""))
     }
     recyclerViewDietPlan.layoutManager = LinearLayoutManager(this@MealActivity,LinearLayoutManager.HORIZONTAL,false)
     recyclerViewDietPlan.adapter = DietPlanShimmer(this@MealActivity,dietplandemo)
     recyclerViewMealType.layoutManager = LinearLayoutManager(this@MealActivity)
     recyclerViewMealType.adapter  = FoodItemshimmer(this@MealActivity,mealdemo)
*/


    /*// Log.e("Today Day Meal",HelperUtils.getTodayDay().toString())
     mMealPresenter.getAllMeals(this@MealActivity,recyclerViewDietPlan,recyclerViewMealType,HelperUtils.getTodayDay()-1)
     var days:ArrayList<Int> = ArrayList()
     days.add(1)
     days.add(2)

*/

     //mMealPresenter.setTodayDay()

     mMealPresenter.getAllMeals(this@MealActivity,recyclerViewDietPlan,recyclerViewMealType,0)
 }



 override fun highLightTodayDay(todayDays: Int) {
     Log.e("Today day",todayDays.toString())

     when(todayDays-1){

         0 -> setUpTextColor(sun, R.color.colorAccent)

         1 -> setUpTextColor(mon, R.color.colorAccent)

         2 -> setUpTextColor(tue, R.color.colorAccent)

         3 -> setUpTextColor(wed, R.color.colorAccent)

         4 -> setUpTextColor(thu, R.color.colorAccent)

         5 -> setUpTextColor(fri, R.color.colorAccent)

         6 -> setUpTextColor(sat, R.color.colorAccent)
     }

 }

 private fun setUpTextColor(textView: TextView, colorCode: Int){

     textView.setTextColor(ContextCompat.getColor(this@MealActivity, colorCode))
 }


 override fun setUpToolbar() {

     toolbarmeal.title="Add/Edit Meal".toUpperCase()
     setSupportActionBar(toolbarmeal)

     supportActionBar?.setDisplayHomeAsUpEnabled(true)
     // supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
     supportActionBar?.setHomeButtonEnabled(true)
 }



 companion object {
     fun getStartIntent(context: Context) : Intent = Intent(context, MealActivity::class.java)
 }


 override fun onCreate(savedInstanceState: Bundle?) {
     super.onCreate(savedInstanceState)
     setContentView(R.layout.meal_activity_layout)



     setUp()
 }

 override fun onOptionsItemSelected(item: MenuItem): Boolean {
     when (item.getItemId()) {
         android.R.id.home -> {
             // API 5+ solution
             onBackPressed()
             return true
         }

         else -> return super.onOptionsItemSelected(item)
     }
 }
}

