package com.example.itstym.perbmember.Base.Dialog.Presenter

import com.example.itstym.perbmember.Base.Activity.Presenter.MvpPresenter
import com.example.itstym.perbmember.Base.Dialog.View.DialogView

/**
 * Created by itstym on 6/12/17.
 */


interface DialogMvpPresenter<in V: DialogView> {

    fun onAttach(mvpView: V)

}
