package com.example.itstym.perbmember.Splash.Presenter

import android.content.Context
import android.widget.ImageView
import com.example.itstym.perbmember.Base.Activity.Presenter.MvpPresenter
import com.example.itstym.perbmember.Splash.View.SplashMvpView

/**
 * Created by itstym on 6/12/17.
 */

interface SplashMvpPresenter<in V: SplashMvpView> : MvpPresenter<V> {

    fun decideNextActivity();

    fun loadZoomInAnimation(context: Context)

    fun loadZoomOutAnimation(context: Context)

    fun startAnimation(context: Context, imageView: ImageView)

    fun showUpdateDailog(context: Context)

    fun sendFirebaseToken(context: Context,token:String)
}