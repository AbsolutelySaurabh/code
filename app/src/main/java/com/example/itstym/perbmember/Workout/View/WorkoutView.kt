package com.example.itstym.perbmember.Workout.View

import com.example.itstym.perbmember.Base.Activity.View.MvpView
import com.example.itstym.perbmember.Workout.Model.Exercise

/**
 * Created by itstym on 6/12/17.
 */

interface WorkoutView : MvpView {

    fun openDaysBottomSheet()

    fun addWorkouttButtonClicked(selectedExercise: ArrayList<Exercise>, selectedDays: ArrayList<Int>)

    fun showConfirmDialog()

    fun highLightTodayDay(todayDays: Int)

    fun showToast(message:String)
}