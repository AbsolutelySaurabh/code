package com.example.itstym.perbmember.Network.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by itstym on 11/12/17.
 */

data class Transaction(

        @SerializedName("trainer_name")
        @Expose
        val trainerName:String,

        @SerializedName("payment_id")
        @Expose
        val paymentId:Int,

        @SerializedName("date_of_payment")
        @Expose
        val dateOfPayment:String,

        @SerializedName("paid_amount")
        @Expose
        val paidAmount:Int,

        @SerializedName("payment_mode")
        @Expose
        val paymentMode:String,

        @SerializedName("plan_name")
        @Expose
        val planName:String,

        @SerializedName("trainer_id")
        @Expose
        val trainerId:Int
)