package com.example.itstym.perbmember.TransactionHistory.Adaptor

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.itstym.perbmember.Network.Model.Transaction
import com.example.itstym.perbmember.R
import kotlinx.android.synthetic.main.transactionshimmer.view.*
import java.util.*

/**
 * Created by mukul on 12/29/17.
 */
class TransactionAdaptorShimmer(var context: Context, var trancsactionHistoryArrayList:ArrayList<Transaction>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int {
        return trancsactionHistoryArrayList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        // Log.i("Adaptor viewtype ", Integer.toString(viewType))

        val v = LayoutInflater.from(context).inflate(R.layout.transactionshimmer, parent, false)
        return ItemShimmerTrans(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {

        (holder as ItemShimmerTrans).bindData(position,context,trancsactionHistoryArrayList[position])

    }

}

class ItemShimmerTrans(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindData(position: Int, context: Context, transaction: Transaction) {


        itemView.shimmerTransaction.startShimmerAnimation()
    }


}
