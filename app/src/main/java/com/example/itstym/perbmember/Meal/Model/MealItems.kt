package com.example.itstym.perbmember.Meal.Model

/**
 * Created by mukul on 1/10/18.
 */
class MealItems {
    var mealName:String? = null
    var mealId:Int? = null
    var foodid:Int? = null
    var foodItems:ArrayList<String> = ArrayList()
    var mealTime:String? = null
    var mealday:ArrayList<Int>  = ArrayList()

    constructor(mealName: String?, mealId: Int?, foodid: Int?, foodItems: ArrayList<String>?, mealTime: String?, mealday: ArrayList<Int>?) {
        this.mealName = mealName
        this.mealId = mealId
        this.foodid = foodid
        this.foodItems = foodItems!!
        this.mealTime = mealTime
        this.mealday = mealday!!
    }
}