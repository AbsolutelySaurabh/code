package com.example.itstym.perbmember.Login.AskPhoneDialog.Presenter

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.LayerDrawable
import android.graphics.drawable.ScaleDrawable
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import com.example.itstym.perbmember.Base.Dialog.Presenter.DialogPresenter
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.Login.AskPhoneDialog.AskPhoneDialog
import com.example.itstym.perbmember.Login.AskPhoneDialog.View.AskPhoneView
import com.example.itstym.perbmember.Network.Model.Member
import com.example.itstym.perbmember.Network.RetrofitHelper.ApiUtils
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.SharedPrefsHelper
import com.example.itstym.perbmember.Utils.HelperUtils
import me.zhanghai.android.materialprogressbar.MaterialProgressBar
import retrofit2.Call
import retrofit2.Response



/**
 * Created by itstym on 6/12/17.
 */


class AskPhoneDialogPresenter<V : AskPhoneView> : DialogPresenter<V>(), AskPhoneDialogMvpPresenter<V> {


    override fun attemptLogin(userPhone: EditText, context: Context, progressBarLogin: MaterialProgressBar, askPhoneDialog: AskPhoneDialog) {


        if (userPhone.text.toString().isEmpty()) {
            Log.e("Button","isEmpty")

            userPhone.error = context.getString(R.string.empty)
            userPhone.requestFocus()
        } else if (userPhone.text.toString().length != 14) {
            Log.e("Button","phone_length_error")
            userPhone.error = context.getString(R.string.phone_length_error)
            userPhone.requestFocus()
        } else {

            if (HelperUtils.isInternetConnectionAvaliable(context)){

                Log.e("Button","isInternetConnectionAvaliable")

                if(userPhone.text.startsWith("+91 ")) {


                    checkIfMemberExists(userPhone.text.toString().split("+91")[1].trim(), context, progressBarLogin, askPhoneDialog)
                }
                else{
                    HelperUtils.showSnackBar("Error Country Code",context as Activity)
                }
            }else{
                Log.e("Button","showSnackBar")


                HelperUtils.showSnackBar("No Internet Available",context as Activity)
            }

        }
    }

    override fun checkIfMemberExists(phone: String, context: Context, progressBarLogin: MaterialProgressBar, askPhoneDialog: AskPhoneDialog) {


        Log.e("Phone is ", phone)
      //  val alertDialog = HelperUtils.showDataSendingDialog(context, "Checking Phone number... ")

        progressBarLogin.visibility=View.VISIBLE

        val bckgrndDr = ColorDrawable(Color.RED)
        val secProgressDr = ColorDrawable(Color.GRAY)
        val progressDr = ScaleDrawable(ColorDrawable(Color.BLUE), Gravity.LEFT, 1f, -1f)
        val resultDr = LayerDrawable(arrayOf(bckgrndDr, secProgressDr, progressDr))

        resultDr.setId(0, android.R.id.background)
        resultDr.setId(1, android.R.id.secondaryProgress)
        resultDr.setId(2, android.R.id.progress)

        progressBarLogin.setProgressDrawable(resultDr)


        val mApiService = ApiUtils.getAPIService()

        mApiService.checkMemberExists(phone).enqueue(object : retrofit2.Callback<Member> {
            override fun onFailure(call: Call<Member>?,  t: Throwable?) {

                Log.e("Issue ",t?.message)

               // HelperUtils.closeDialog(alertDialog)
                progressBarLogin.visibility=View.INVISIBLE
                HelperUtils.showSnackBar("Network Issue", context as Activity)
            }

            override fun onResponse(call: Call<Member>?, response: Response<Member>?) {

               // HelperUtils.closeDialog(alertDialog)
               // progressBarLogin.visibility=View.INVISIBLE


                if (response!!.isSuccessful) {

                    if (response.body()!!.status){
                        //trainer exist
                        //send Otp

                        val dataManager:DataManager= DataManager(sharedPrefsHelper = SharedPrefsHelper(context))

                        HelperUtils.loggingButNotVerifiedMemberDetails= response.body()!!
                        HelperUtils.sendOTP(phone,context,dataManager,false,progressBarLogin)
                        askPhoneDialog.dismiss()

                    }else {
                        HelperUtils.showSnackBar("Member Not Exists", context as Activity)
                    }
                } else {
                    HelperUtils.showSnackBar("Member Not Exists", context as Activity)
                }

            }
        })

    }
}