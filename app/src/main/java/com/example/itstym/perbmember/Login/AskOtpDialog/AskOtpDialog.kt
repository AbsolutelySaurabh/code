package com.example.itstym.perbmember.Login.AskOtpDialog

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import com.example.itstym.perbmember.Base.Dialog.BaseDialog
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.Login.AskOtpDialog.Presenter.AskOtpPresenter
import com.example.itstym.perbmember.Login.AskOtpDialog.View.AskOtpView
import com.example.itstym.perbmember.Login.Receiver.SmsListener
import com.example.itstym.perbmember.Login.Receiver.SmsReceiver
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.Utils.FocusSwitchingTextWatcher
import com.example.itstym.perbmember.Utils.HelperUtils
import kotlinx.android.synthetic.main.otp_dialog_layout.*



/**
 * Created by itstym on 6/12/17.
 */

class AskOtpDialog: BaseDialog, AskOtpView {

    override fun setup(){
        super.setup()

        mAskOtpDialogPresenter= AskOtpPresenter()
        mAskOtpDialogPresenter.onAttach(this)
        val progressDrawable = progressBarOTP.getProgressDrawable().mutate()
        progressDrawable.setColorFilter(Color.RED, android.graphics.PorterDuff.Mode.SRC_IN)
        progressBarOTP.getProgressDrawable().setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN)


        phoneNo.text="+91 "+userPhoneNo

        resendOtp.setOnClickListener {
            //sending otp again

            HelperUtils.sendOTP(userPhoneNo, mContext, mDataManager, true, progressBarOTP)

        }

        otp1.addTextChangedListener(FocusSwitchingTextWatcher(otp2))
        otp2.addTextChangedListener(FocusSwitchingTextWatcher(otp3))
        otp3.addTextChangedListener(FocusSwitchingTextWatcher(otp4))
        otp4.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

                if (otp4.text.toString().length == 1) {

                    dismiss()
                    val otp = otp1.text.toString() + otp2.text.toString() + otp3.text.toString() + otp4.text.toString()
                    mAskOtpDialogPresenter.verifyOtp(otp.toInt(), userPhoneNo, mContext,progressBarOTP)
                }
            }

            override fun afterTextChanged(editable: Editable) {

            }
        })
        SmsReceiver.bindListener(object : SmsListener {
            override fun onMessageRecevied(msg: String, sender: String) {

                Log.e("Message ", msg)

                val msgArray = msg.split("OTP-")
                Log.e("OTP is ", msgArray[1].trim())

                val otpIs = msgArray[1].trim().toCharArray()
                Log.e("otp1 ", otpIs[0].toString())
                Log.e("otp2 ", otpIs[1].toString())
                Log.e("otp3 ", otpIs[2].toString())
                Log.e("otp4 ", otpIs[3].toString())

                otp1.setText(otpIs[0].toString())
                otp2.setText(otpIs[1].toString())
                otp3.setText(otpIs[2].toString())
                otp4.setText(otpIs[3].toString())
               // progressBarOTP.visibility = View.GONE

            }

        })

    }


    lateinit var mAskOtpDialogPresenter: AskOtpPresenter<AskOtpView>

    constructor(context: Context) : super(context) {}

    lateinit var userPhoneNo:String

    constructor(context: Context, themeResId: Int, dataManager: DataManager, userPhoneNo:String) : super(context, themeResId, dataManager){
        this.userPhoneNo=userPhoneNo;
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.otp_dialog_layout)// use your layout in place of this.

        setup()

    }

}