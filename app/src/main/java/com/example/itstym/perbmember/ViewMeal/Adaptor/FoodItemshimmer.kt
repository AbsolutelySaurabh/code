package com.example.itstym.perbmember.ViewMeal.Adaptor

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.example.itstym.perbmember.Network.Model.Meal
import com.example.itstym.perbmember.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.food_item_shimmer.view.*

class FoodItemshimmer(var context: Context, var foodItemArrayList:ArrayList<Meal>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int {
        return foodItemArrayList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        // Log.i("Adaptor viewtype ", Integer.toString(viewType))

        val v = LayoutInflater.from(context).inflate(R.layout.food_item_shimmer, parent, false)
        return ItemCombatFoodShimmer(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {

        (holder as ItemCombatFoodShimmer).bindData(context,foodItemArrayList[position])
    }

}

class ItemCombatFoodShimmer(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindData(context: Context, mealItem: Meal) {

        itemView.shimmerFoodItem.startShimmerAnimation()
    }

    private fun loadImage(imageIcon: Int, context: Context, imageView: ImageView) {

        Picasso.with(context).load(imageIcon).into(imageView)
    }

}