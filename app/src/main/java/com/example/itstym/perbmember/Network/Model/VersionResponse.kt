package com.example.itstym.perbmember.Network.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by mukul on 1/4/18.
 */
data class VersionResponse(

        @SerializedName("status")
        @Expose
        val status:Boolean,

        @SerializedName("message")
        @Expose
        val message:String
)