package com.example.itstym.perbmember.Feed.Presenter

import android.app.Activity
import android.content.Context
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.example.itstym.perbmember.Base.Activity.Presenter.BasePresenter
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.Feed.Adapter.FeedAdaptor
import com.example.itstym.perbmember.Feed.View.FeedView
import com.example.itstym.perbmember.Network.Model.Blog
import com.example.itstym.perbmember.Network.Model.BlogsResponse
import com.example.itstym.perbmember.Network.RetrofitHelper.ApiUtils
import com.example.itstym.perbmember.Utils.HelperUtils
import retrofit2.Call
import retrofit2.Response

/**
 * Created by itstym on 8/12/17.
 */

class FeedPresenter<V : FeedView>(dataManager: DataManager) : BasePresenter<V>(dataManager), FeedMvpPresenter<V> {


    override fun getAllFeed(feedRecyclerView: RecyclerView, context: Context, swipetoRefresh: SwipeRefreshLayout) {

        //val alertDialog = HelperUtils.showDataSendingDialog(context, "Fetching Feeds ... ")

        val mApiService = ApiUtils.getAPIService()

        mApiService.getAllBlogs().enqueue(object : retrofit2.Callback<BlogsResponse> {
            override fun onFailure(call: Call<BlogsResponse>?, t: Throwable?) {

              //  HelperUtils.closeDialog(alertDialog)
                HelperUtils.showSnackBar("Network Issue", context as Activity)
            }

            override fun onResponse(call: Call<BlogsResponse>?, response: Response<BlogsResponse>?) {

              //  HelperUtils.closeDialog(alertDialog)

                if (response!!.body()!=null){

                if (response!!.body()!!.status){

                    if(swipetoRefresh.isRefreshing)
                        swipetoRefresh.isRefreshing=false

                    feedRecyclerView.layoutManager= LinearLayoutManager(context)
                    feedRecyclerView.adapter= FeedAdaptor(context, response.body()!!.blogArrayList, object : FeedAdaptor.feedItem{
                        override fun onClickBlog(blogData: Blog) {
                            //open the activtity if type is text
                            mvpView?.openBlogTextActivity(blog = blogData)
                        }

                        override fun onCommentClicked(blogData: Blog) {
                            mvpView!!.openCommentFragment(blogData)
                        }


                    })

                }}
            }
        })
    }

}