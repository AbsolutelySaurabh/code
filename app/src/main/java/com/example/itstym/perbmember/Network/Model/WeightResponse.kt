package com.example.itstym.perbmember.Network.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by mukul on 12/19/17.
 */

data class WeightResponse(


        @SerializedName("status")
        @Expose
        val status:Boolean,

        @SerializedName("weight_logs")
        @Expose
        val weightArrayList:ArrayList<Weight>
)
