package com.example.absolutelysaurabh.perbmember.MealsData

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.example.itstym.perbmember.Meal.Adaptor.AddMeal
import com.example.itstym.perbmember.Meal.Adaptor.Custom.AddMealViewHolder
import com.example.itstym.perbmember.Meal.Adaptor.CustomMealAdaptor
import com.example.itstym.perbmember.Meal.Adaptor.ItemCombatmeal.Companion.context
import com.example.itstym.perbmember.Meal.Model.MealPlanObject
import com.example.itstym.perbmember.Meal.Model.MealViewPlan
import com.example.itstym.perbmember.R
import kotlinx.android.synthetic.main.addmealtextview.view.*

import java.util.ArrayList

class CustomMealsDataAdapter(private val mContext: Context, var openAddMeal: CustomMealAdaptor.onAddMeal, private val al_food_list: ArrayList<MealViewPlan>?) : RecyclerView.Adapter<CustomMealsDataViewHolder>() {
    private val editTextList = ArrayList<EditText>()

    interface onAddMeal {
        fun openAddMealDailog()

    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): CustomMealsDataViewHolder {


        Log.e("View Type", position.toString())
       // if (position == 0) {

            //return AddMealViewHolder(LayoutInflater.from(viewGroup.context), viewGroup, position, al_food_list!!)

            //return AddMeal(LayoutInflater.from(context).inflate(R.layout.addmealtextview, viewGroup, false))

       // } else {
            return CustomMealsDataViewHolder(LayoutInflater.from(viewGroup.context), viewGroup, position, al_food_list!!)

        //}
    }
    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: CustomMealsDataViewHolder, position: Int) {

        holder.add_more_items.setOnClickListener {
            holder.linearLayout_editText.visibility = View.VISIBLE
            createOneFullRow(holder.linearLayout_editText)
        }

        holder.done.setOnClickListener {
            holder.linearLayout_editText.visibility = View.GONE
            holder.linearLayout_textview.visibility = View.VISIBLE
            setTextViews(holder.linearLayout_textview)
            editTextList.clear()
        }

        holder.food_name_first.visibility = View.VISIBLE
        //holder.edit_text.visibility = View.GONE
        //setting the heading, time entered in previous dialog
        //holder.meal_type.text = MainActivity.first_food
        //holder.meal_time.text = MainActivity.meal_time_selected
        holder.meal_time.setTextColor(R.color.colorPrimaryText)
        holder.food_name_first.setTextColor(R.color.colorPrimaryText)
    }

    private fun setTextViews(linearLayout_textview: LinearLayout) {
        var i = 0;
        while (i < editTextList.size) {
            val textView = TextView(mContext)
            textView.text = editTextList[i].text.toString() + ", "
            linearLayout_textview.addView(textView)
            i++;
        }
    }

    private fun createOneFullRow(linearLayout: LinearLayout): LinearLayout {
        linearLayout.addView(editText(rowId.toString()))
        return linearLayout
    }

    private fun editText(hint: String): EditText {
        val editText = EditText(mContext)
        editText.id = Integer.valueOf(hint)!!
        editText.hint = "Enter Food"
        editTextList.add(editText)
        return editText
    }

    override fun getItemCount(): Int {
        return al_food_list?.size ?: 0
    }

    companion object {
        var EXTRA_MESSAGE = ""
        var num = 1

        var rowId = 1
    }
}


class AddMeal(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindData(context: Context, openAddMeal: CustomMealAdaptor.onAddMeal) {
            itemView.addcustomMeal.setOnClickListener {

                openAddMeal.openAddMealDailog()

            }
        }
}

