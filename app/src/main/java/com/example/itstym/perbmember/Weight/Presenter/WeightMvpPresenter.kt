package com.example.itstym.perbmember.Weight.Presenter

import android.content.Context
import android.widget.Button
import com.example.itstym.perbmember.Base.Activity.Presenter.MvpPresenter
import com.example.itstym.perbmember.Weight.View.WeightView
import com.github.mikephil.charting.charts.LineChart
import com.qindachang.widget.RulerView
import me.zhanghai.android.materialprogressbar.MaterialProgressBar

/**
 * Created by itstym on 11/12/17.
 */


interface WeightMvpPresenter<in V: WeightView> : MvpPresenter<V> {

    fun weightRecyclerViewSetup(context: Context)

    fun submitWeight(context: Context, progressBarWeight: MaterialProgressBar, weightSubmit: Button)

    fun weightRularListener(context:Context,ruler:RulerView)

    fun weightGraph(context: Context, weightGraph: LineChart, progressBarWeight: MaterialProgressBar)
}
