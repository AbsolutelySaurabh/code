package com.example.itstym.perbmember.ViewWorkout.Presenter

import android.content.Context
import android.support.design.widget.BottomSheetDialog
import android.support.v7.widget.RecyclerView
import android.widget.TextView
import com.example.itstym.perbmember.Base.Activity.Presenter.MvpPresenter
import com.example.itstym.perbmember.ViewWorkout.View.WorkoutView
import me.zhanghai.android.materialprogressbar.MaterialProgressBar

/**
 * Created by itstym on 6/12/17.
 */


interface WorkoutMvpPresenter<in V: WorkoutView> : MvpPresenter<V> {

    fun getAllWorkout(memberId: Int, context: Context, bodyPartRecyclerView: RecyclerView, exerciseRecyclerView: RecyclerView, errorEmpty: TextView)

    fun workOutSubmitButtonClicked()

    fun getDays():ArrayList<String>

    fun onConfirmButtonClicked(context: Context, distinctArray: ArrayList<Int>, progressbar: MaterialProgressBar, mBottomSheetDialog: BottomSheetDialog)

    fun setTodayDay()

    fun upDateWorkout(selectedDay:Int,context: Context)


}