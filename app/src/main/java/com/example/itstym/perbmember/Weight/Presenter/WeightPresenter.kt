package com.example.itstym.perbmember.Weight.Presenter

import android.app.Activity
import android.content.Context
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.View
import android.widget.Button
import com.example.itstym.perbmember.Base.Activity.Presenter.BasePresenter
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.Network.Model.CommonApiResponse
import com.example.itstym.perbmember.Network.Model.Weight
import com.example.itstym.perbmember.Network.Model.WeightResponse
import com.example.itstym.perbmember.Network.RetrofitHelper.ApiUtils
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.Utils.HelperUtils
import com.example.itstym.perbmember.Weight.View.WeightView
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.qindachang.widget.RulerView
import kotlinx.android.synthetic.main.custommarker.view.*
import me.zhanghai.android.materialprogressbar.MaterialProgressBar
import retrofit2.Call
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by itstym on 11/12/17.
 */

class WeightPresenter<V : WeightView>(dataManager: DataManager) : BasePresenter<V>(dataManager), WeightMvpPresenter<V> {

    val random = Random()
    var xElements: ArrayList<Float> = ArrayList()
    var yElements: ArrayList<Float> = ArrayList()
    var dates: ArrayList<String> = ArrayList()
    var graphelements: ArrayList<Entry> = arrayListOf()

    lateinit var mContext: Context
    lateinit var chart:LineChart
    lateinit var  mWeightPresenter: WeightPresenter<WeightView>


    override fun weightGraph(context: Context, lineChart: LineChart, progressBarWeight: MaterialProgressBar) {
        progressBarWeight.visibility = View.VISIBLE

        mContext = context


        mWeightPresenter = WeightPresenter(dataManager)

        chart = lineChart





        lineChart.getXAxis().setDrawGridLines(false)
      //  lineChart.getXAxis().setDrawAxisLine(true)
        lineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM)
        lineChart.getXAxis().setAxisMinimum(-1f)
        lineChart.getXAxis().granularity = 1f


      //  lineChart.getAxisRight().setEnabled(false)
        lineChart.getAxisRight().setDrawZeroLine(true)
        lineChart.getAxisRight().setDrawGridLines(false)
        lineChart.getAxisRight().setDrawAxisLine(false)
        lineChart.getAxisRight().setDrawLabels(false)
        lineChart.getAxisLeft().setAxisMinimum(30f)
        lineChart.getAxisLeft().setAxisMaximum(150f)
        lineChart.marker = CustomMarkerView(context,R.layout.custommarker)

//
       // lineChart.getAxisLeft().setEnabled(true)
        lineChart.getAxisLeft().setDrawGridLines(false)
       // lineChart.getAxisLeft().setDrawAxisLine(true)
        // lineChart.getAxisLeft().setDrawLabels(false)
        lineChart.getDescription().setText("");

//
//        lineChart.setTouchEnabled(false)
//        lineChart.setScaleEnabled(false)
//        lineChart.setNoDataText("Couldn't get your Weight!")
//        lineChart.setNoDataTextColor(R.color.colorPrimary);

        lineChart.getLegend().setEnabled(false)
        //  lineChart.invalidate();

        val mApiService = ApiUtils.getAPIService()



        mApiService.getAllWeight(dataManager.getMemberId()).enqueue(object : retrofit2.Callback<WeightResponse> {
            override fun onFailure(call: Call<WeightResponse>?, t: Throwable?) {

                Log.e("Tag", t.toString())
                progressBarWeight.visibility = View.GONE

                if(HelperUtils.isConnected()){
                    HelperUtils.showSnackBar("Network Issue", context as Activity)


                }
                else{
                    HelperUtils.showSnackBar("Not connected to internet", context as Activity)

                }


//                HelperUtils.closeDialog(alertDialog)
            }

            override fun onResponse(call: Call<WeightResponse>?, response: Response<WeightResponse>?) {

                // HelperUtils.closeDialog(alertDialog)

                if (response!!.body()!!.status) {
                    Log.e("Tag", response!!.body()!!.status.toString())

                    setData(lineChart, response!!.body()!!.weightArrayList,progressBarWeight)


//                    commentRecyclerView.layoutManager= LinearLayoutManager(context)
//                    commentAdaptor= CommentAdaptor(context,response.body()!!.commentArrayList)
//                    commentRecyclerView.adapter=commentAdaptor
//                    commentRecylerView=commentRecyclerView
//                    allComments=response.body()!!.commentArrayList


                }
            }
        })

        //To change body of created functions use File | Settings | File Templates.
    }

    fun setData(lineChart: LineChart, weightLog: ArrayList<Weight>, progressBarWeight: MaterialProgressBar) {


        var yMax = 0f

        graphelements.clear()
        xElements.clear()
        yElements.clear()
        dates.clear()


        var counter: Int = 0

        lineChart.getXAxis().setAxisMinimum((counter).toFloat())
        for (i in weightLog) {
            counter++

            val date = i.weightIntakeDate.split("T").get(0)

            //Log.e("DAtewww", date)
//            val arrOfStr = i.weightIntakeDate.split("T").toMutableList()
//
//            val milliseconds = HelperUtils.getTimeInMills(i.weightIntakeDate,"yyyy-mm-dd")

            xElements.add(counter.toFloat())
            yElements.add(i.weight.toFloat())
            dates.add(date)

        }

        lineChart.getXAxis().setAxisMaximum((counter  + 1).toFloat())


        for (i in xElements) {



            graphelements.add(Entry(i, yElements.get(xElements.indexOf(i))))
           // Log.e("Crash Here","jeif")


        }

        for (f in yElements) {


            Log.e("TAG", f.toString())
        }


        val set1: LineDataSet
        set1 = LineDataSet(graphelements, "")



        set1.mode = LineDataSet.Mode.CUBIC_BEZIER
        set1.setColor(ContextCompat.getColor(mContext, R.color.colorAccent))




        set1.setDrawValues(false)


        val data = LineData(set1)


        var counterDate: Int = 0
        val xAxis = lineChart.getXAxis()
        if(dates.size>0){
        xAxis.setValueFormatter(object : IAxisValueFormatter {
            override fun getFormattedValue(value: Float, axis: AxisBase?): String {


               Log.e("Value", value.toString())

             //   for(i in 0..dates.)

//                if (dates.size > counterDate) {
//                    return dates.get(++counterDate)
//
//                } else
//                    return ""
                //   Log.e("Index1",index.toString())

                if(value.toInt()==0 || value.toInt()==dates.size+1){
                    return ""
                }
                else
                return HelperUtils.getDateInFormat(HelperUtils.getTimeInMills(dates.get(value.toInt() - 1),"yyyy-mm-dd"),"dd-mm-yyyy")


            }


        })}

        Log.e("Crash Here","jeif")

        if(dates.size>0){

        data.addDataSet(set1)






        lineChart.data = data
        //  lineChart.axisLeft.axisMaximum = (yMax.toInt() + 10).toFloat()
        lineChart.invalidate()
        progressBarWeight.visibility = View.GONE
        }
    }


    override fun weightRularListener(context: Context, weightRular: RulerView) {

        weightRular.setOnValueChangeListener { value ->

            Log.e("Selectedweight ", value.toString())

            userSelectedWeight = value.toInt()
            mvpView?.weightShown(value.toInt())

        }

    }


    var userSelectedWeight = 0
    override fun weightRecyclerViewSetup(context: Context) {

        //setup recyclerview
//        weighingScale.layoutManager= LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false)
//        weighingScale.adapter= WeighingScaleAdaptor(context = context, weightChangeListener = object : WeighingScaleAdaptor.WeighChangeListener{
//            override fun onWeightChanged(weight: Int) {
//
//                Log.e("weight ",weight.toString())
//                //selectedWeight.setText(weight)

//            }
//
//        })

    }


    override fun submitWeight(context: Context, progressBarWeight: MaterialProgressBar, weightSubmit: Button) {

        progressBarWeight.visibility = View.VISIBLE
        weightSubmit.isEnabled = false


        val mApiService = ApiUtils.getAPIService()

        mApiService.getAllWaterLogs(dataManager.getMemberId(), weight = userSelectedWeight).enqueue(object : retrofit2.Callback<CommonApiResponse> {
            override fun onFailure(call: Call<CommonApiResponse>?, t: Throwable?) {

               // HelperUtils.closeDialog(alertDialog)
                HelperUtils.showSnackBar("Network Issue", context as Activity)
                weightSubmit.isEnabled = true

            }

            override fun onResponse(call: Call<CommonApiResponse>?, response: Response<CommonApiResponse>?) {

              //  HelperUtils.closeDialog(alertDialog)

                if (response!!.isSuccessful) {

                    Log.e(" OTP Response body ", response.body()!!.status.toString())

                    if (response.body()!!.status) {
                        //update the view
                        //update the graph also

                        mWeightPresenter.weightGraph(context,chart, progressBarWeight

                        )
                        weightSubmit.isEnabled = false


                    } else {
                        HelperUtils.showSnackBar("OTP NOT MATCHED", context as Activity)
                        progressBarWeight.visibility = View.GONE
                        weightSubmit.isEnabled = false


                    }


                } else {
                    HelperUtils.showSnackBar("Network issue while verifying otp", context as Activity)
                    progressBarWeight.visibility = View.GONE
                    weightSubmit.isEnabled = false


                }
            }
        })
    }


    fun rand(from: Int, to: Int): Int {
        return random.nextInt(to - from) + from
    }


    inner class CustomMarkerView(context: Context, layoutResource: Int) : MarkerView(context, layoutResource) {

        override fun refreshContent(e: Entry?, highlight: Highlight?) {


            if (e != null) {
                weightMarker.setText("Weight =" + e.y.toString() )
            }

            super.refreshContent(e, highlight)
        }



    }


}