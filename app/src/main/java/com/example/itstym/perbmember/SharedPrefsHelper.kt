package com.example.itstym.perbmember

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by itstym on 6/12/17.
 */


class SharedPrefsHelper (context: Context){

    var mSharedPreference: SharedPreferences

    init {
        mSharedPreference=context.getSharedPreferences(MY_PREFS, Context.MODE_PRIVATE)
    }
    companion object {

        var MY_PREFS="PERB_MEMBER"
        var EMAIL="EMAIL"
        var IS_LOGGED_IN="IS_LOGGED_IN"
        var MEMBER_ID="MEMBER_ID"
        var MEMBER_NAME="MEMBER_NAME"
        var MEMBER_PROFILE_URL="MEMBER_PROFILE_URL"
        var MEMBER_PHONE="MEMBER_PHONE"
        var MEMBER_GENDER="MEMBER_GENDER"
        var MEMEBR_DOJ="MEMBER_DATE_OF_JOINING"
        var MEMBER_ADDRESS="MEMBER_ADDRESS"
    }


    fun putMemberGender(gender:String){
        mSharedPreference.edit().putString(MEMBER_GENDER,gender).apply()
    }

    fun getMemeberGender(): String {
        return mSharedPreference.getString(MEMBER_GENDER, "male")
    }

    fun putDOJ(doj:String){
        mSharedPreference.edit().putString(MEMEBR_DOJ,doj).apply()
    }

    fun getDOJ(): String {
        return mSharedPreference.getString(MEMEBR_DOJ, "00-00-0000")
    }

    fun putMemberAddress(address:String){
        mSharedPreference.edit().putString(MEMBER_ADDRESS,address).apply()
    }

    fun getMemberAddress(): String {
        return mSharedPreference.getString(MEMBER_ADDRESS, "n/a")
    }



    fun clear(){
        mSharedPreference.edit().clear().apply()
    }

    fun putMemberId(id:Int){
        mSharedPreference.edit().putInt(MEMBER_ID,id).apply()
    }

    fun putEmail(email:String){
        mSharedPreference.edit().putString(EMAIL,email).apply()
    }

    fun getEmail(): String {
        return mSharedPreference.getString(EMAIL, null)
    }

    fun getLoggedInMode(): Boolean {
        return mSharedPreference.getBoolean(IS_LOGGED_IN, false)
    }

    fun getMemberId():Int{
        return mSharedPreference.getInt(MEMBER_ID, 0)
    }

    fun setLoggedInMode(loggedIn: Boolean) {
        mSharedPreference.edit().putBoolean(IS_LOGGED_IN, loggedIn).apply()
    }


    fun putMemberName(memberName:String){
        mSharedPreference.edit().putString(MEMBER_NAME, memberName).apply()
    }

    fun getMembername():String{
        return mSharedPreference.getString(MEMBER_NAME, null)
    }

    fun putMemberProfilePic(profile_pic_url:String){
        mSharedPreference.edit().putString(MEMBER_PROFILE_URL, profile_pic_url).apply()
    }

    fun getMemberProfilePic():String{
        return mSharedPreference.getString(MEMBER_PROFILE_URL, null)
    }

    fun setLogOut(){
        mSharedPreference.edit().putInt(MEMBER_ID,0).apply()
    }


    fun putMemberPhone(memberPhone:String){
        return mSharedPreference.edit().putString(MEMBER_PHONE,memberPhone).apply()
    }

    fun getMemberPhone():String{
        return mSharedPreference.getString(MEMBER_PHONE,"+91")

    }

}