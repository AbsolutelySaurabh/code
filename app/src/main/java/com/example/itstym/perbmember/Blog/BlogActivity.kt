package com.example.itstym.perbmember.Blog

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import com.example.itstym.perbmember.Base.Activity.BaseActivity
import com.example.itstym.perbmember.Blog.Presenter.BlogPresenter
import com.example.itstym.perbmember.Blog.View.BlogView
import com.example.itstym.perbmember.PerbMemberApplication
import com.example.itstym.perbmember.R
import kotlinx.android.synthetic.main.text_blog_layout.*





/**
 * Created by itstym on 10/12/17.
 */

class BlogActivity: BaseActivity(), BlogView {

    override fun setUpToolbar() {

        setSupportActionBar(toolbar)

        supportActionBar!!.title= getString(R.string.blog)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        // supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

    }
    override fun setUp() {

        val dataManager = (application as PerbMemberApplication).getDataManager()
        mBlogPresenter = BlogPresenter(dataManager)
        mBlogPresenter.onAttach(this)

        setUpToolbar()

    }


    lateinit var  mBlogPresenter: BlogPresenter<BlogView>

    lateinit var blogTitleText:Any
    lateinit var blogContentText:Any
    lateinit var blogImageUrl:Any

    companion object {

        fun getStartIntent(context: Context) : Intent = Intent(context,BlogActivity::class.java)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.text_blog_layout)

        setUp()

        //get data from previous Activity
        blogTitleText=intent.extras.get("blog_title")
        blogContentText=intent.extras.get("blog_content")
        blogImageUrl=intent.extras.get("blog_image_url")
        Log.e("Blog Conent",blogContentText.toString())

        //blogTitleText="text"
        //blogContentText="dfj"

        blogwebview.loadUrl(blogContentText.toString())
//        blogwebview.setWebViewClient(object : WebViewClient() {
//            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap) {
//
//                progressBarwebView.visibility = View.VISIBLE
//
//
//                super.onPageStarted(view, url, favicon)
//            }
//
//            override fun onPageFinished(view: WebView, url: String) {
//
//                progressBarwebView.visibility = View.GONE
//
//
//                super.onPageFinished(view, url)
//            }
//        })

        blogwebview.setWebChromeClient(object : WebChromeClient() {
            private var mProgress: ProgressDialog? = null

            override fun onProgressChanged(view: WebView, progress: Int) {
               Log.e("Progress",progress.toString())

                if(progress<100){
                progressBarwebView.visibility = View.VISIBLE
                progressBarwebView.progress = progress}
                else{
                    progressBarwebView.visibility = View.GONE

                }
            }
        })



        Log.e("Content ",blogContentText.toString())
        Log.e("blogTitleText ",blogTitleText.toString())



     //   HelperUtils.loadImage(blogImageUrl.toString(),this@BlogActivity,blogImage)
       // blogTitle.text=blogTitleText.toString()
       // blogContent.text=blogContentText.toString()


    }

    override fun onResume() {
        super.onResume()


    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                // API 5+ solution
                onBackPressed()
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }



}