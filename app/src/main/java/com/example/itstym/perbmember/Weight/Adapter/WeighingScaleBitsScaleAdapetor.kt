package com.example.itstym.perbmember.Weight.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.itstym.perbmember.R

/**
 * Created by itstym on 11/12/17.
 */


class WeighingScaleBitsScaleAdapetor(var context: Context): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int {
        return 5
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        // Log.i("Adaptor viewtype ", Integer.toString(viewType))

        val v = LayoutInflater.from(context).inflate(R.layout.weight_scale_bit_item, parent, false)
        return ItemC(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {

        (holder as ItemC).bindData(context)

    }

}

class ItemC(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindData(context: Context) {



    }


}