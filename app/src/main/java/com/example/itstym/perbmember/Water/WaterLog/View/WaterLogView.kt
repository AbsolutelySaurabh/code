package com.example.itstym.perbmember.Water.WaterLog.View

import com.example.itstym.perbmember.Base.Activity.View.MvpView

/**
 * Created by itstym on 10/12/17.
 */


interface WaterLogView : MvpView{

    fun updateView(numWaterGlass: Int)

    fun updateLog()
}