package com.example.itstym.perbmember.Meal.Adaptor

import android.content.Context
import android.opengl.Visibility
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.example.itstym.perbmember.Meal.Model.MealViewPlan
import com.example.itstym.perbmember.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.addmealtextview.view.*
import kotlinx.android.synthetic.main.customfooditem.view.*
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by mukul on 1/13/18.
 */
class CustomMealAdaptor(var context: Context, var openAddMeal:CustomMealAdaptor.onAddMeal, var foodItemArrayList:ArrayList<MealViewPlan>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    interface onAddMeal{
        fun openAddMealDailog()

    }
    override fun getItemCount(): Int {
        return foodItemArrayList.size+1
    }

    open val editTextList = ArrayList<EditText>()

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {

        // Log.i("Adaptor viewtype ", Integer.toString(viewType))

        Log.e("View Type",viewType.toString())
            if(viewType==0){

                return AddMeal(LayoutInflater.from(context).inflate(R.layout.addmealtextview, parent, false))
            }
        else{
        return ItemCombatmeal(LayoutInflater.from(context).inflate(R.layout.customfooditem, parent, false), context, editTextList)}
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        Log.e("View Type position",position.toString())

        if(position==foodItemArrayList.size)
            (holder as AddMeal).bindData(context,openAddMeal)
        else

        (holder as ItemCombatmeal).bindData(context,foodItemArrayList[position])
    }

//    companion object {
//        var EXTRA_MESSAGE = ""
//        var num = 1
//        lateinit var al_food_items: ArrayList<String>
//        var rowId = 1
//    }
}

class ItemCombatmeal(itemView: View, context: Context, editTextList: ArrayList<EditText>) : RecyclerView.ViewHolder(itemView) {

    fun bindData(mContext: Context, mealItem:MealViewPlan) {

        al_food_items = ArrayList()
        context = mContext

        itemView.mealType.text=mealItem.mealType
        itemView.mealTime.text=mealItem.alarmTime
        var foodItems:String = ""

        for(i in mealItem.fooditems!!){
            if(mealItem.fooditems!!.indexOf(i)==mealItem.fooditems!!.size-1)
                foodItems = foodItems +" "+i.foodName
            else{
                foodItems = foodItems +i.foodName+", "

            }
        }
//        if(!foodItems.equals(""))
//            itemView.foodItems.text = foodItems

        itemView.editordone.setOnClickListener{
            itemView.EditTextContainer.visibility = View.GONE
            itemView.linear_layout_textview.visibility = View.VISIBLE

            setTextViews(itemView.linear_layout_textview)
            editTextList.clear()
        }

        itemView.addfooditems.setOnClickListener{

            itemView.EditTextContainer.visibility = View.VISIBLE
//            val et = EditText(context)
//            val p = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
//            et.layoutParams = p
//            et.setHint("Enter Food Name")
//           // et.id = numberOfLines + 1
//            //numberOfLines++
//            itemView.EditTextContainer.addView(et)
            createOneFullRow(itemView.EditTextContainer)//linear layout

        }
    }

    private fun setTextViews(linearLayout_textview: LinearLayout) {
        var i =0;
        while (i < editTextList.size) {
            val textView = TextView(context)
            textView.text = editTextList[i].text.toString() + ", "
            al_food_items.add(editTextList[i].text.toString())
            linearLayout_textview.addView(textView)
            i++;
        }
    }

    open fun createOneFullRow(linearLayout: LinearLayout): LinearLayout {
        linearLayout.addView(editText(rowId.toString()))
        return linearLayout
    }

    open fun editText(hint: String): EditText {
        val editText = EditText(context)
        editText.id = Integer.valueOf(hint)!!
        editText.hint = "Enter Food"

        editTextList.add(editText)
        return editText
    }


//    override fun getItemViewType(position: Int): Int {
//        if(position == foodItemArrayList.size)
//            return 0
//        else
//            return 1
//
//    }

    private fun loadImage(imageIcon: Int, context: Context, imageView: ImageView) {

        Picasso.with(context).load(imageIcon).into(imageView)
    }

    companion object {
        var EXTRA_MESSAGE = ""
        var num = 1
        lateinit var al_food_items: ArrayList<String>
        var editTextList: ArrayList<EditText> = ArrayList()
        var rowId = 1
        var context: Context? = null
    }

}

class AddMeal(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindData(context: Context, openAddMeal: CustomMealAdaptor.onAddMeal) {
        itemView.addcustomMeal.setOnClickListener{

            openAddMeal.openAddMealDailog()

        }
    }
}