package com.example.itstym.perbmember.Network.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by itstym on 6/12/17.
 */

data class Member(
        @SerializedName("status")
        @Expose
        val status:Boolean,

        @SerializedName("message")
        @Expose
        val message:String,


        @SerializedName("member_id")
        @Expose
        val member_id:Int,

        @SerializedName("plan_id")
        @Expose
        val plan_id:Int,

        @SerializedName("trainer_id")
        @Expose
        val trainer_id:Int,


        @SerializedName("gym_time")
        @Expose
        val gym_time:String,

        @SerializedName("profile_pic_url")
        @Expose
        val profile_pic_url:String,

        @SerializedName("gender_id")
        @Expose
        val gender_id:Int,


        @SerializedName("phone")
        @Expose
        val phone:String,

        @SerializedName("email")
        @Expose
        val email:String,

        @SerializedName("name")
        @Expose
        val name:String,

        @SerializedName("doj")
        @Expose
        val dateOfJoining:String,

        @SerializedName("full_address")
        @Expose
        val fullAddress:String

)