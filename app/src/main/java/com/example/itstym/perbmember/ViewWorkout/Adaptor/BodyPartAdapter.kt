package com.example.itstym.perbmember.ViewWorkout.Adaptor

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.ViewWorkout.Model.BodyPart
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.body_part_item.view.*

/**
 * Created by itstym on 6/12/17.
 */


class BodyPartAdapter(var context: Context, var bodyPartData: ArrayList<BodyPart>, var bodyPartSelection: BodyPartSelection, var abc: Int, b: Boolean, var todayBodypartid: Int): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
         var position:Int = -1
        var firstTime :Boolean = true
        var prevBodyPart = ""
    }



    interface BodyPartSelection{

        fun onbodyPartClicked(bodyPart: BodyPart,selected:Boolean)
    }

    override fun getItemCount(): Int {
        return bodyPartData.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        // Log.i("Adaptor viewtype ", Integer.toString(viewType))

        firstTime = true

        val v = LayoutInflater.from(context).inflate(R.layout.body_part_item, parent, false)
        return Item(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {

        (holder as Item).bindData(context, bodyPartData[position], bodyPartSelection = bodyPartSelection, selectedDay = abc, adapter = this@BodyPartAdapter,todayBodypartid = todayBodypartid)
    }


    fun update(position: Int,firstTime:Boolean){

        BodyPartAdapter.position = position

        BodyPartAdapter.firstTime = firstTime



        notifyDataSetChanged()
    }

    fun updatePrevBodyPart(bodypart:String){
        BodyPartAdapter.prevBodyPart = bodypart


    }


}

class Item(itemView: View) : RecyclerView.ViewHolder(itemView) {


    fun bindData(context: Context, bodyPart: BodyPart, bodyPartSelection: BodyPartAdapter.BodyPartSelection,selectedDay: Int,adapter: BodyPartAdapter, todayBodypartid: Int) {
        Log.e("Today Body Part",todayBodypartid.toString())

        if (!BodyPartAdapter.prevBodyPart.equals(bodyPart.body_part_name)) {
            itemView.bodyPartName.text = bodyPart.body_part_name



            if (BodyPartAdapter.firstTime) {
                Log.e("first time", bodyPart.selected_days.toString())


                for (i in bodyPart.selected_days) {
                    Log.e("first time", i.toString())

                }


                if (bodyPart.body_part_id==todayBodypartid) {
                    itemView.relativeLayout.isSelected = true
                    Log.e("first time", BodyPartAdapter.firstTime.toString())

                    itemView.body_part_image.setColorFilter(Color.argb(255, 255, 255, 255))
                    bodyPartSelection.onbodyPartClicked(bodyPart = bodyPart, selected = itemView.relativeLayout.isSelected)

                }
                else{
                    itemView.relativeLayout.visibility = View.GONE
                    itemView.bodyPartName.visibility =View.GONE
                }
            }


            Log.e("Image uri", bodyPart.body_part_url)

            loadImage(bodyPart.body_part_url, context, imageView = itemView.body_part_image)





            adapter.updatePrevBodyPart(bodyPart.body_part_name)
        }
        else{
            itemView.relativeLayout.visibility = View.GONE
            itemView.bodyPartName.visibility =View.GONE
        }
    }
    private fun loadImage(imageIcon: String, context: Context, imageView: ImageView) {

        Picasso.with(context).load(imageIcon).into(imageView)

    }


}