package com.example.itstym.perbmember.Water.WaterLog.Presenter

import android.content.Context
import android.widget.RelativeLayout
import com.example.itstym.perbmember.Base.Activity.Presenter.MvpPresenter
import com.example.itstym.perbmember.Water.WaterLog.View.WaterLogView

/**
 * Created by itstym on 10/12/17.
 */


interface WaterLogMvpPresenter<in V: WaterLogView> : MvpPresenter<V> {

    fun updateWaterLog(context: Context, numWaterGlass: Int, plusRelativeLayout: RelativeLayout)

    fun getWaterLogs(context: Context)
}