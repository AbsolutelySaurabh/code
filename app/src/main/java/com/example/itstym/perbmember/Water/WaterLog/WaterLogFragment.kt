package com.example.itstym.perbmember.Water.WaterLog

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import com.example.itstym.perbmember.Base.Fragment.BaseFragment
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.SharedPrefsHelper
import com.example.itstym.perbmember.Utils.HelperUtils
import com.example.itstym.perbmember.Water.WaterActivity
import com.example.itstym.perbmember.Water.WaterAnalysis.WaterAnalysisFragment
import com.example.itstym.perbmember.Water.WaterLog.Presenter.WaterLogPresenter
import com.example.itstym.perbmember.Water.WaterLog.View.WaterLogView
import com.facebook.shimmer.ShimmerFrameLayout
import kotlinx.android.synthetic.main.water_log_fragment_layout.*
import java.util.*

/**
 * Created by itstym on 10/12/17.
 */


class WaterLogFragment: BaseFragment(), WaterLogView {
    override fun updateLog() {
    }


    override fun updateView(numWaterGlass: Int) {

        //waterShimmer.startShimmerAnimation()
        val calender= Calendar.getInstance()

        totalWaterGlassDone=numWaterGlass
        numWaterGlassTextView.text=numWaterGlass.toString()+" of 12 Glasses"
        val todayDateInMonth=calender.get(Calendar.DAY_OF_MONTH)
        val currentDate= todayDateInMonth.toString()+ HelperUtils.getDayOfMonthSuffix(todayDateInMonth)+" "+ HelperUtils.getDateInFormat(calender.timeInMillis,"MMMM, yyyy")
        Log.e("Date",currentDate.toString())
        todayText.setText(currentDate.toString())
        todayText.setBackgroundResource(0)
        numWaterGlassTextView.setBackgroundResource(0)
        waterTextSlogan.setText("Have Water!! It's Necessary")
        waterTextSlogan.setBackgroundResource(0)
        waterTextSlogan2.setText("Drink up in every 60 minutes")
        waterTextSlogan2.setBackgroundResource(0)
        waterProgressBar.progress=numWaterGlass*10
        plusRelativeLayout.setBackgroundDrawable(resources.getDrawable(R.drawable.circle_with_border))
        minusRelativeLayout.setBackgroundDrawable(resources.getDrawable(R.drawable.circle_with_border))

        if (numWaterGlass >= 12) {
            waterDropLayout.setBackgroundResource(R.drawable.bluedrop)



            imageView11.visibility = View.GONE
            plusImage.visibility = View.GONE
            plusRelativeLayout.visibility = View.GONE

        } else {

            waterDropLayout.setBackgroundResource(R.drawable.water_circle)
            imageView11.visibility = View.VISIBLE
            plusImage.visibility = View.VISIBLE
            plusRelativeLayout.visibility = View.VISIBLE

            if(numWaterGlass<=0){
                minusImage.visibility = View.GONE
                minusRelativeLayout.visibility = View.GONE
            }
            else{
                minusImage.visibility = View.VISIBLE
                minusRelativeLayout.visibility = View.VISIBLE

            }


        }
        shimmerWater.stopShimmerAnimation()

    }
    override fun onResume() {
        super.onResume()

        plusRelativeLayout.setOnClickListener {

            if (totalWaterGlassDone >= 12) {
                //nothingwillhappen
            } else {
                //for plus button
                plusRelativeLayout.isEnabled = false
                mWaterLogPresenter.updateWaterLog(context!!, totalWaterGlassDone + 1,plusRelativeLayout)

               // updateView(totalWaterGlassDone + 1)

                //invokeWaterAnalyisFragment update data

                val fragmentWaterAnal = activity!!
                        .supportFragmentManager
                        .findFragmentByTag((activity as WaterActivity).tag ) as WaterAnalysisFragment
                fragmentWaterAnal.updateData()

            }

        }

        minusRelativeLayout.setOnClickListener {
            //for minus button

            if (totalWaterGlassDone==0){
                //nothing will happen
            }else{
                minusRelativeLayout.isEnabled = false
                mWaterLogPresenter.updateWaterLog(context!!, totalWaterGlassDone-1, minusRelativeLayout)

                val fragmentWaterAnal = activity!!
                        .supportFragmentManager
                        .findFragmentByTag((activity as WaterActivity).tag ) as WaterAnalysisFragment
                fragmentWaterAnal.updateData()

                updateView(totalWaterGlassDone - 1)
            }
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.water_log_fragment_layout, container, false)
        setup(view)

        return view
    }

    lateinit var mWaterLogPresenter:WaterLogPresenter<WaterLogView>
    lateinit var plusRelativeLayout:RelativeLayout
    lateinit var minusRelativeLayout:RelativeLayout
    lateinit var waterProgressBar:ProgressBar
    var totalWaterGlassDone=0

    lateinit var numWaterGlassTextView:TextView
    lateinit var todayText:TextView

    lateinit var waterTextSlogan:TextView
    lateinit var waterTextSlogan2:TextView
    lateinit var imageDrop:ImageView
    lateinit var waterDropLayout:RelativeLayout
    lateinit var plusImage:ImageView
    lateinit var minusImage:ImageView




    lateinit var shimmerWater:ShimmerFrameLayout




    override fun setup(view: View?) {

        Log.e(WaterLogFragment.TAG,"setup")

        val dataManager= DataManager(SharedPrefsHelper(context!!))

        mWaterLogPresenter = WaterLogPresenter<WaterLogView>(dataManager)
        mWaterLogPresenter.onAttach(this)


        //set today date
       // val calender= Calendar.getInstance()

     //   val todayDate= view?.findViewById<TextView>(R.id.todayDate) as TextView



        //get all items
         plusRelativeLayout= view!!.findViewById<RelativeLayout>(R.id.plus_layout) as RelativeLayout
         minusRelativeLayout= view.findViewById<RelativeLayout>(R.id.minus_layout) as RelativeLayout
         numWaterGlassTextView= view.findViewById<TextView>(R.id.numWaterGlassTextView) as TextView
        waterTextSlogan= view.findViewById<TextView>(R.id.waterSlogan1) as TextView
        waterTextSlogan2= view.findViewById<TextView>(R.id.waterSlogan2) as TextView
        waterDropLayout = view.findViewById<RelativeLayout>(R.id.waterDropLayout) as RelativeLayout
        imageDrop = view.findViewById<ImageView>(R.id.imageView11) as ImageView
        plusImage = view.findViewById<ImageView>(R.id.plus_image) as ImageView
        minusImage = view.findViewById<ImageView>(R.id.minus_Image) as ImageView




        shimmerWater= view.findViewById<TextView>(R.id.waterShimmer) as ShimmerFrameLayout



        waterProgressBar= view.findViewById<ProgressBar>(R.id.waterProgress) as ProgressBar



        todayText = view.findViewById<TextView>(R.id.todayDate) as TextView

        //fetch water glass done for today

        todayText.setText("")

                todayText.setBackgroundColor(resources.getColor(R.color.shimmersColor))
        numWaterGlassTextView.setText("")
        numWaterGlassTextView.setBackgroundColor(resources.getColor(R.color.shimmersColor))
        waterTextSlogan.setText("")
        waterTextSlogan.setBackgroundColor(resources.getColor(R.color.shimmersColor))
        waterTextSlogan2.setText("")
        waterTextSlogan2.setBackgroundColor(resources.getColor(R.color.shimmersColor))
        waterDropLayout.setBackgroundDrawable(resources.getDrawable(R.drawable.watershimmer))

        imageDrop.visibility = View.GONE
        shimmerWater.startShimmerAnimation()
        mWaterLogPresenter.getWaterLogs(context!!)


    }

    companion object {

        fun newInstance(): WaterLogFragment {
            val args= Bundle()
            val waterFragment: WaterLogFragment = WaterLogFragment();
            waterFragment.arguments=args
            return waterFragment
        }

        val TAG="WaterLogFragment"

    }

}