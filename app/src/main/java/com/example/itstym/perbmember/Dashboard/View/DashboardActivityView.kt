package com.example.itstym.perbmember.Dashboard.View

import android.content.Context
import com.example.itstym.perbmember.Base.Activity.View.MvpView

/**
 * Created by itstym on 6/12/17.
 */


interface DashboardActivityView : MvpView {

    fun ToolbarSetup( fragmentTag:Int)

    fun showHomeFragment()

    fun showChatFragment()

    fun showDashBoardBottomDialog()

    fun showProfileFragment()

    fun showSettingFragment()

    fun openWorkOutActivity()

    fun openMealActivity()

    fun openTrackWaterActivity()

    fun openWeightActivity()


}