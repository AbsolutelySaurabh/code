package com.example.itstym.perbmember.Profile.View

import com.example.itstym.perbmember.Base.Activity.View.MvpView

/**
 * Created by itstym on 11/12/17.
 */


interface SettingView:MvpView{

    fun openAboutUs()

    fun feedback()

    fun termsOfService()

    fun tprivacyPolicy()

    fun callUS()

    fun pushNotification()

    fun logout()


}