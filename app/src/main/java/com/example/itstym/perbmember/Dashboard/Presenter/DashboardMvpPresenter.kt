package com.example.itstym.perbmember.Dashboard.Presenter

import android.content.Context
import com.example.itstym.perbmember.Base.Activity.Presenter.MvpPresenter
import com.example.itstym.perbmember.Dashboard.View.DashboardActivityView
import com.example.itstym.perbmember.Network.Model.BottomSheetModel
import java.util.*

/**
 * Created by itstym on 6/12/17.
 */


interface DashboardMvpPresenter<in V: DashboardActivityView> : MvpPresenter<V> {

    fun onHomeClicked()

    fun onChatClicked()

    fun onDashBoardPlusButtonClicked()

    fun onProfileClicked()

    fun onSettingClicked()

    fun getBottomSheetData(): ArrayList<BottomSheetModel>

    fun decideNextActivity(position:Int)

    fun sendFirebaseToken(context:Context,token:String)
}
