package com.example.itstym.perbmember.Water.Presenter

import android.content.Context
import android.widget.ImageView
import com.example.itstym.perbmember.Base.Activity.Presenter.MvpPresenter
import com.example.itstym.perbmember.Splash.View.SplashMvpView
import com.example.itstym.perbmember.Water.View.WaterView

/**
 * Created by itstym on 10/12/17.
 */

interface WaterMvpPresenter<in V: WaterView> : MvpPresenter<V> {


}