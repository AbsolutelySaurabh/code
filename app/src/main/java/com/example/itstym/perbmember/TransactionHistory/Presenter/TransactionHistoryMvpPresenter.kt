package com.example.itstym.perbmember.TransactionHistory.Presenter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import com.example.itstym.perbmember.Base.Activity.Presenter.MvpPresenter
import com.example.itstym.perbmember.TransactionHistory.View.TransactionHistoryView

/**
 * Created by itstym on 11/12/17.
 */


interface TransactionHistoryMvpPresenter<in V: TransactionHistoryView> : MvpPresenter<V> {

    fun setUpRecyclerView(context: Context, recyclerView: RecyclerView, verticleBar: View)

}