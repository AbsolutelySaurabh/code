package com.example.itstym.perbmember.Profile.View

import com.example.itstym.perbmember.Base.Activity.View.MvpView

/**
 * Created by itstym on 11/12/17.
 */


interface ProfileView:MvpView{

    fun openPersonalDetailsActivity()

    fun openWorkoutActivity()

    fun openMealActivity()

    fun transactionHistoryActivity()




}