package com.example.itstym.perbmember.Workout.Presenter

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.design.widget.BottomSheetDialog
import android.support.v7.widget.RecyclerView
import android.widget.Button
import android.widget.TextView
import com.example.itstym.perbmember.Base.Activity.Presenter.MvpPresenter
import com.example.itstym.perbmember.Workout.Model.Exercise
import com.example.itstym.perbmember.Workout.View.WorkoutView
import me.zhanghai.android.materialprogressbar.MaterialProgressBar

/**
 * Created by itstym on 6/12/17.
 */


interface WorkoutMvpPresenter<in V: WorkoutView> : MvpPresenter<V> {

    fun getAllWorkout(memberId: Int, context: Context, bodyPartRecyclerView: RecyclerView, exerciseRecyclerView: RecyclerView, errorEmpty: TextView, get: Int)

    fun workOutSubmitButtonClicked(workoutSubmitButton: Button)

    fun getDays():ArrayList<String>

    fun onConfirmButtonClicked(context: Context, distinctArray: java.util.ArrayList<Int>, progressbar: MaterialProgressBar, mBottomSheetDialog: BottomSheetDialog, selectedExercise: java.util.ArrayList<Exercise>, confirmButton: ConstraintLayout)

    fun setTodayDay()

    fun upDateWorkout(selectedDay:Int,context: Context)


}