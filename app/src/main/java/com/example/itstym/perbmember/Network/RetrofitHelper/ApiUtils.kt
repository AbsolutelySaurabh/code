package com.example.itstym.perbmember.Network.RetrofitHelper

/**
 * Created by itstym on 22/11/17.
 */

class ApiUtils(){

    companion object {

        val BASE_URL = "http://35.163.186.51/";
    //    val BASE_URL = "http://localhost:3000/";

        fun getAPIService():ApiService {

            return RetrofitClient.Companion.getClient(BASE_URL)!!.create(ApiService::class.java)

        }

    }
}