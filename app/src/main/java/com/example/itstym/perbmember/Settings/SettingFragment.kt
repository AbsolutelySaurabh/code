package com.example.itstym.perbmember.Profile

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.itstym.perbmember.Base.Fragment.BaseFragment
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.Login.LoginActivity
import com.example.itstym.perbmember.Profile.View.SettingView
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.SharedPrefsHelper


/**
 * Created by itstym on 11/12/17.
 */


class SettingFragment: BaseFragment(), SettingView {
    override fun openAboutUs() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun feedback() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun termsOfService() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun tprivacyPolicy() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun callUS() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun pushNotification() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun logout() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    interface SettingFragmentListener{

        fun onPersonalDetailsActivity()

        fun onTransactionHistoryActivity()
    }


    override fun onResume() {
        super.onResume()


    }




    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.settings, container, false)
        setup(view)

        return view
    }

    lateinit var mSettingFragment: SettingFragment
    lateinit var aboutUs:ConstraintLayout
    lateinit var feedBack:ConstraintLayout
    lateinit var termsOfService:ConstraintLayout
    lateinit var privacyPolicy:ConstraintLayout
    lateinit var Callus:ConstraintLayout
    lateinit var pushNotificaton:ConstraintLayout
    lateinit var Logout:ConstraintLayout



    override fun setup(view: View?) {

        Log.e(TAG,"setup")

        val dataManager= DataManager(SharedPrefsHelper(context!!))

        mSettingFragment = SettingFragment()
        mSettingFragment.onAttach(context!!)
        aboutUs= view?.findViewById<ConstraintLayout>(R.id.aboutUs) as ConstraintLayout
        feedBack= view?.findViewById<ConstraintLayout>(R.id.feedBack) as ConstraintLayout
        termsOfService= view?.findViewById<ConstraintLayout>(R.id.termsOfService) as ConstraintLayout
        privacyPolicy= view?.findViewById<ConstraintLayout>(R.id.privacyPolicy) as ConstraintLayout
        Callus= view?.findViewById<ConstraintLayout>(R.id.Callus) as ConstraintLayout
        pushNotificaton= view?.findViewById<ConstraintLayout>(R.id.pushNotificaton) as ConstraintLayout
        Logout= view?.findViewById<ConstraintLayout>(R.id.Logout) as ConstraintLayout

        val toolbar = activity!!.findViewById<View>(R.id.toolbar) as Toolbar
        toolbar.setTitle("Settings")








        aboutUs.setOnClickListener{
            Log.e(TAG,"setup")

            val uri = Uri.parse("http://www.google.com")
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)


        }
        feedBack.setOnClickListener{
            Log.e(TAG,"setup")
            val uri = Uri.parse("http://www.google.com")
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)

        }
        termsOfService.setOnClickListener{
            Log.e(TAG,"setup")
            val uri = Uri.parse("http://www.google.com")
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)

        }
        privacyPolicy.setOnClickListener{
            Log.e(TAG,"setup")
            val uri = Uri.parse("http://www.google.com")
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)

        }
        Callus.setOnClickListener{
            val uri = Uri.parse("http://www.google.com")
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)

        }
        pushNotificaton.setOnClickListener {
            Log.e(TAG,"setup")
            val uri = Uri.parse("http://www.google.com")
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)

        }
        Logout.setOnClickListener {

            Toast.makeText(context,"showSettingFragment",Toast.LENGTH_LONG).show()
        dataManager.setLoggedOut()
        startActivity(Intent(LoginActivity.getStartIntent(context!!)))
        activity!!.finish()

        }



    }



    companion object {

        fun newInstance(): SettingFragment {
            val args= Bundle()
            val settingFragment: SettingFragment = SettingFragment();
            settingFragment.arguments=args
            return settingFragment
        }

        val TAG="settingFragment"

    }



}
