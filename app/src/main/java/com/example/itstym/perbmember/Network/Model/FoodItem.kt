package com.example.itstym.perbmember.Network.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by itstym on 12/12/17.
 */

data class FoodItem(

        @SerializedName("comment")
        @Expose
        val comment:String,

        @SerializedName("member_name")
        @Expose
        val member_name:String

        )