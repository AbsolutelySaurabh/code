package com.example.itstym.perbmember.Water.WaterAnalysis.View

import com.example.itstym.perbmember.Base.Activity.View.MvpView
import com.example.itstym.perbmember.Network.Model.Water

/**
 * Created by itstym on 10/12/17.
 */



interface WaterAnalysisView:MvpView{

    fun updateData(waterWeekdata:HashMap<Int, Water>)
}



