package com.example.itstym.perbmember.Meal.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by mukul on 1/10/18.
 */
data class FoodItems(

        val foodId:Int,

        val foodName:String
)