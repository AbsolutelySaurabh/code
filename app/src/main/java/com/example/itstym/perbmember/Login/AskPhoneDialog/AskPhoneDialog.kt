package com.example.itstym.perbmember.Login.AskPhoneDialog

import android.content.Context
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import com.example.itstym.perbmember.Base.Dialog.BaseDialog
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.Login.AskPhoneDialog.Presenter.AskPhoneDialogPresenter
import com.example.itstym.perbmember.Login.AskPhoneDialog.View.AskPhoneView
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.Utils.PermissionUtils
import kotlinx.android.synthetic.main.mobile_dialog_layout.*


/**
 * Created by itstym on 6/12/17.
 */


class AskPhoneDialog: BaseDialog, AskPhoneView ,ActivityCompat.OnRequestPermissionsResultCallback,PermissionUtils.PermissionResultCallback{
    override fun NeverAskAgain(request_code: Int) {

        Log.e("Permission Granted","Permission")

    }

    override fun PermissionGranted(request_code: Int) {

        Log.e("Permission Granted","Permission")

    }

    override fun PartialPermissionGranted(request_code: Int, granted_permissions: java.util.ArrayList<String>?) {
        Log.e("Permission Granted","Permission")

    }

    override fun PermissionDenied(request_code: Int) {
        Log.e("Permission Granted","Permission")

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {

        Log.e("Permission Granted","Permission")
        mPermissionUtils.onRequestPermissionsResult(requestCode,permissions,grantResults);


    }

    override fun permissionCallback(granted: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    interface askPermissionForMessage {

        fun onMessageReceived()
    }
    lateinit var mPermissionUtils: PermissionUtils
    lateinit var permissions:ArrayList<String>


    override fun setup(){
        super.setup()

        mAskPhoneDialogPresenter= AskPhoneDialogPresenter()



        mAskPhoneDialogPresenter.onAttach(this)

        //to focus  the cursor after country code
        userPhone.setSelection(userPhone.text.length)


        //automatically attempt login once user enter the number
        userPhone.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                // TODO Auto-generated method stub
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (s.toString().length < 4) {
                    userPhone.setText("+91 ")
                    userPhone.setSelection(4)
                }


                if(userPhone.text.length==14){
                    mPermissionUtils = PermissionUtils(mContext)

                    permissions = ArrayList()

                    permissions.add(android.Manifest.permission.READ_SMS)
                    permissions.add(android.Manifest.permission.RECEIVE_SMS)




                    Log.e("Permission",mPermissionUtils.check_permission(permissions, "Need Permission for getting otp sms", 1).toString())


                    mAskPhoneDialogPresenter.attemptLogin(userPhone,mContext,progressBarLogin,this@AskPhoneDialog)
                }

            }
        })


        continueButton.setOnClickListener {

            mAskPhoneDialogPresenter.attemptLogin(userPhone, mContext, progressBarLogin, this)
        }

    }

    lateinit var mAskPhoneDialogPresenter:AskPhoneDialogPresenter<AskPhoneDialog>

    constructor(context: Context) : super(context) {}

    constructor(context: Context, themeResId: Int, dataManager: DataManager) : super(context, themeResId, dataManager)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.mobile_dialog_layout)// use your layout in place of this.

        setup()

    }

}