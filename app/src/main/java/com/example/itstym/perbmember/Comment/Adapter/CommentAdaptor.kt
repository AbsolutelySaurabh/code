package com.example.itstym.perbmember.Comment.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.itstym.perbmember.Network.Model.Comment
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.Utils.HelperUtils
import kotlinx.android.synthetic.main.comment_item.view.*
import kotlinx.android.synthetic.main.commentshimmer.view.*

/**
 * Created by itstym on 8/12/17.
 */


class CommentAdaptor(var context: Context, var commentDetails:ArrayList<Comment>, var isLoading:Boolean): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int {
        Log.e("Size",commentDetails.size.toString())
        return commentDetails.size
    }


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        // Log.i("Adaptor viewtype ", Integer.toString(viewType))
        if(isLoading){
            return Item(LayoutInflater.from(context).inflate(R.layout.commentshimmer, parent, false))

        }
        else
        return Item(LayoutInflater.from(context).inflate(R.layout.comment_item, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {

        (holder as Item).bindData(commentDetails[position],context,isLoading)

    }

    class Item(itemView: View) : RecyclerView.ViewHolder(itemView) {


        fun bindData(comment: Comment, context: Context, loading: Boolean) {

            if(loading)
            itemView.commentShimmer.startShimmerAnimation()
            else{

            itemView.memberName.text=comment.member_name
                if(comment.profile_pic!= null){ if(
                !comment.profile_pic.equals(""))
            HelperUtils.loadImage(comment.profile_pic,context,itemView.memberPic)
                }

            itemView.commentContent.text=comment.comment
            }
        }



    }


}