package com.example.itstym.perbmember.Utils;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

/**
 * Created by Anil Kumar on 8/21/2017.
 */

public class FocusSwitchingTextWatcher implements TextWatcher {

    private final View nextViewToFocus;

    public FocusSwitchingTextWatcher(View nextViewToFocus) {
        this.nextViewToFocus = nextViewToFocus;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (charSequence.length() ==1) {
            nextViewToFocus.requestFocus();
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
