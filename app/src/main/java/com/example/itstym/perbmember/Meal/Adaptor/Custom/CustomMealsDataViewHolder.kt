package com.example.absolutelysaurabh.perbmember.MealsData

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.example.itstym.perbmember.Meal.Model.MealPlanObject
import com.example.itstym.perbmember.Meal.Model.MealViewPlan
import com.example.itstym.perbmember.R

import java.util.ArrayList

/**
 * Created by absolutelysaurabh on 12/1/18.
 */

class CustomMealsDataViewHolder(from: LayoutInflater, viewGroup: ViewGroup, position: Int, todayBookingsList: ArrayList<MealViewPlan>) : RecyclerView.ViewHolder(from.inflate(R.layout.customfooditem, viewGroup, false)) {

    var meal_type: TextView
    var meal_time: TextView
    var food_name_first: TextView
    var add_more_items: TextView
    //public RecyclerView recyclerView_edit_text;
    var linearLayout_editText: LinearLayout
    var linearLayout_textview: LinearLayout
    var done: TextView

    init {

        this.meal_time = itemView.findViewById(R.id.mealTime)
        this.meal_type = itemView.findViewById(R.id.mealType)
        this.add_more_items = itemView.findViewById(R.id.addfooditems)
        // this.recyclerView_edit_text = itemView.findViewById(R.id.recyclerview_food_edit_text);
        this.linearLayout_editText = itemView.findViewById(R.id.EditTextContainer)
        this.linearLayout_textview = itemView.findViewById(R.id.linear_layout_textview)
        this.done = itemView.findViewById(R.id.editordone)
       // this.edit_text = itemView.findViewById(R.id.food_name_1_edit_text)

        this.food_name_first = itemView.findViewById(R.id.food_name_first)
    }
}
