package com.example.itstym.perbmember.Dashboard.Presenter

import android.app.Activity
import android.content.Context
import android.support.design.widget.BottomNavigationView
import android.util.Log
import com.example.itstym.perbmember.Base.Activity.Presenter.BasePresenter
import com.example.itstym.perbmember.Dashboard.View.DashboardActivityView
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.Network.Model.BottomSheetModel
import com.example.itstym.perbmember.Network.Model.FirebaseResponse
import com.example.itstym.perbmember.Network.RetrofitHelper.ApiUtils
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.Utils.HelperUtils
import retrofit2.Call
import retrofit2.Response
import java.util.*

/**
 * Created by itstym on 6/12/17.
 */


class DashboardPresenter<V : DashboardActivityView>(dataManager: DataManager) : BasePresenter<V>(dataManager), DashboardMvpPresenter<V> {
    override fun sendFirebaseToken(context: Context, token: String) {


        val mApiService = ApiUtils.getAPIService()

        mApiService.addToken(token,1,dataManager.getMemberId())
                .enqueue(object : retrofit2.Callback<FirebaseResponse> {

                    override fun onFailure(call: Call<FirebaseResponse>?, t: Throwable?) {

                        Log.e("Issueftft ",t?.message)

//                        HelperUtils.closeDialog(alertDialog)
                        if(HelperUtils.isConnected()){
                            HelperUtils.showSnackBar("Network Issue", context as Activity)}
                        else{
                            HelperUtils.showSnackBar("No internet Available", context as Activity)}
                    }

                    override fun onResponse(call: Call<FirebaseResponse>?, response: Response<FirebaseResponse>?) {

                        if (response!!.isSuccessful) {

                            if (response.body()!!.status){
                                Log.e("Firebase issue",response.body()!!.message)



                            }else {
                               // Log.e("Firebase gyigissue",response!!.message())
                              //  Log.e("Firebase issue",response.body()!!.message)




                                // HelperUtils.showSnackBar(response.message(), context as Activity)
                            }
                        } else {

                            HelperUtils.showSnackBar("Member Not Exists", context as Activity)
                        }

                    }
                })
    }


    override fun onHomeClicked() {
        mvpView?.showHomeFragment()
    }

    override fun onChatClicked() {
        mvpView?.showChatFragment()
    }

    override fun onDashBoardPlusButtonClicked() {
        mvpView?.showDashBoardBottomDialog();
    }

    override fun onProfileClicked() {
        mvpView?.showProfileFragment()
    }

    override fun onSettingClicked() {
        mvpView?.showSettingFragment()
    }

     var previd:String = ""

    val bottomNavigationListener= BottomNavigationView.OnNavigationItemSelectedListener { item ->

        Log.e("Selected item is ",item.itemId.toString())

        when (item.itemId) {

            R.id.action_home -> {

                //previd Stores previous pressed item so then item wont click twice
                if(!previd.equals(item.itemId.toString())){
                onHomeClicked()
                previd = item.itemId.toString()}
                true
            }

            R.id.action_chat ->{
                if(!previd.equals(item.itemId.toString())){
                    onChatClicked()
                    previd = item.itemId.toString()}
                true
            }

            R.id.action_profile ->{
                //openGlobalDIalogView()
                if(!previd.equals(item.itemId.toString())){
                    onProfileClicked()
                    previd = item.itemId.toString()}
                true
            }

            R.id.action_setting ->{
                if(!previd.equals(item.itemId.toString())){
                    onSettingClicked()
                    previd = item.itemId.toString()}
                true
            }

            else -> {

                onDashBoardPlusButtonClicked()
                true
            }

        }



    }

    override fun getBottomSheetData():ArrayList<BottomSheetModel> {

        val arrayList= ArrayList<BottomSheetModel>()

        val BottomSheetItemOne=BottomSheetModel(R.drawable.ic_action_add_member,"Add/Edit Workout")
        arrayList.add(BottomSheetItemOne)

        val BottomSheetItemTwo=BottomSheetModel(R.drawable.ic_action_add_member,"Add Meal")
        arrayList.add(BottomSheetItemTwo)

        val BottomSheetItemThree=BottomSheetModel(R.drawable.ic_action_add_member,"Track Water")
        arrayList.add(BottomSheetItemThree)

        val BottomSheetItemFour=BottomSheetModel(R.drawable.ic_action_add_member,"Track Weight")
        arrayList.add(BottomSheetItemFour)

        val BottomSheetItemFive=BottomSheetModel(0,"")
        arrayList.add(BottomSheetItemFive)


        return arrayList;
    }

    override fun decideNextActivity(position: Int) {

        when(position){

            0 -> mvpView?.openWorkOutActivity()

            1 -> mvpView?.openMealActivity()

            2 -> mvpView?.openTrackWaterActivity()

            3 -> mvpView?.openWeightActivity()

            else ->{
                Log.e("Wrong","Activity")
            }

        }
    }
}
