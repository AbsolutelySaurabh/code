package com.example.itstym.perbmember.Meal.Adaptor.Custom

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.example.itstym.perbmember.Meal.Adaptor.ItemCombatmeal.Companion.context
import com.example.itstym.perbmember.Meal.Model.MealViewPlan
import com.example.itstym.perbmember.R
import java.util.ArrayList

/**
 * Created by absolutelysaurabh on 17/1/18.
 */

class AddMealViewHolder(from: LayoutInflater, viewGroup: ViewGroup, position: Int, todayBookingsList: ArrayList<MealViewPlan>) : RecyclerView.ViewHolder(from.inflate(R.layout.addmealtextview, viewGroup, false)) {

    var addmeal: TextView
    var meal_time: TextView

    init {

        this.meal_time = itemView.findViewById(R.id.mealTime)
        this.addmeal = itemView.findViewById(R.id.addcustomMeal)

    }
}
