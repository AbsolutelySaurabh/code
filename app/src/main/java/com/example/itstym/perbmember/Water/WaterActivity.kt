package com.example.itstym.perbmember.Water

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.example.itstym.perbmember.Base.Activity.BaseActivity
import com.example.itstym.perbmember.PerbMemberApplication
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.Water.Adapter.ViewPagerAdapter
import com.example.itstym.perbmember.Water.Presenter.WaterPresenter
import com.example.itstym.perbmember.Water.View.WaterView
import kotlinx.android.synthetic.main.water_activity_layout.*

/**
 * Created by itstym on 10/12/17.
 */


class WaterActivity: BaseActivity(), WaterView {
    lateinit var tag: String

    fun setTabFragmentB(t: String) {
        tag = t
    }

    fun getTabFragmentB(): String {
        return tag
    }

    override fun setUpToolbar() {

    }
    override fun setUp() {
        val dataManager = (application as PerbMemberApplication).getDataManager()

        mWaterPresenter = WaterPresenter(dataManager)

        mWaterPresenter.onAttach(this)

        viewPagerAdapter= ViewPagerAdapter(fragmentManager = supportFragmentManager)
        viewPager.adapter=viewPagerAdapter

        tabLayout.setupWithViewPager(viewPager)

    }

    companion object {
        fun getStartIntent(context: Context) : Intent = Intent(context,WaterActivity::class.java)
    }


    lateinit var viewPagerAdapter: ViewPagerAdapter


    lateinit var  mWaterPresenter: WaterPresenter<WaterView>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.water_activity_layout)

        setUp()
    }

    override fun onResume() {
        super.onResume()

        closeActivity.setOnClickListener {
            finish()
        }
    }


}