package com.example.itstym.perbmember.Feed.Presenter

import android.content.Context
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView
import com.example.itstym.perbmember.Base.Activity.Presenter.MvpPresenter
import com.example.itstym.perbmember.Feed.View.FeedView

/**
 * Created by itstym on 8/12/17.
 */

interface FeedMvpPresenter<in V: FeedView> : MvpPresenter<V> {

    fun getAllFeed(feedRecyclerView: RecyclerView, context: Context, swipetoRefresh: SwipeRefreshLayout)

}