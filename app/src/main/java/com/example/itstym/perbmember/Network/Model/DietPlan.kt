package com.example.itstym.perbmember.Network.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by itstym on 12/12/17.
 */

data class DietPlan(

        @SerializedName("diet_plan_id")
        @Expose
        val dietPlanId:Int,

        @SerializedName("diet_plan")
        @Expose
        val dietPlan:String
)