package com.example.itstym.perbmember.Meal.Adaptor

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.example.itstym.perbmember.Meal.Model.MealViewPlan
import com.example.itstym.perbmember.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.food_item.view.*

/**
 * Created by itstym on 12/12/17.
 */


class FoodItemAdaptor(var context: Context, var foodItemArrayList:ArrayList<MealViewPlan>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int {
        return foodItemArrayList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        // Log.i("Adaptor viewtype ", Integer.toString(viewType))

        val v = LayoutInflater.from(context).inflate(R.layout.food_item, parent, false)
        return ItemCombat(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {

        (holder as ItemCombat).bindData(context,foodItemArrayList[position])
    }

}

class ItemCombat(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindData(context: Context, mealItem:MealViewPlan) {

        itemView.mealType.text=mealItem.mealType
        itemView.mealTime.text=mealItem.alarmTime
        var foodItems:String = ""

        for(i in mealItem.fooditems!!){
            if(mealItem.fooditems!!.indexOf(i)==mealItem.fooditems!!.size-1)
                foodItems = foodItems +" "+i.foodName
            else{
                foodItems = foodItems +i.foodName+", "

            }
        }

        if(!foodItems.equals(""))
            itemView.foodItems.text = foodItems

    }

    private fun loadImage(imageIcon: Int, context: Context, imageView: ImageView) {

        Picasso.with(context).load(imageIcon).into(imageView)
    }

}