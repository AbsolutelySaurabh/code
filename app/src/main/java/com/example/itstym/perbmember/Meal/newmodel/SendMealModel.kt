package com.example.itstym.perbmember.Meal.newmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SendMealModel {

    @SerializedName("gym_id")
    @Expose
    var gymId: Int? = null
    @SerializedName("diet_name")
    @Expose
    var dietName: String? = null
    @SerializedName("meal_type_array")
    @Expose
    var mealTypeArray: ArrayList<String>? = null
    @SerializedName("member_id")
    @Expose
    var memberId: Int? = null
    @SerializedName("days")
    @Expose
    var days: ArrayList<Int>? = null

    /**
     * No args constructor for use in serialization
     *
     */





    constructor(i: Int, dietName: String, finaldietMealsRecylerViewArray: ArrayList<String>, memberId: Int, selectedDays: ArrayList<Int>){
        this.gymId = i
        this.dietName = dietName
        this.mealTypeArray = finaldietMealsRecylerViewArray
        this.memberId = memberId
        this.days = selectedDays
    }


}
