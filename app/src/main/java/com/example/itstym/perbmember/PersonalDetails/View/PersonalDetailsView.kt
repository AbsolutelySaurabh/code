package com.example.itstym.perbmember.PersonalDetails.View

import com.example.itstym.perbmember.Base.Activity.View.MvpView

/**
 * Created by itstym on 11/12/17.
 */


interface PersonalDetailsView:MvpView{

    fun showPersonalInfo(memberName:String, memberPhone:String, memberProfileurl:String, memberEmail:String, memberGener:String, memberDOj:String, memberAddress:String)
}