package com.example.itstym.perbmember.Base.Activity

import android.support.v7.app.AppCompatActivity
import com.example.itstym.perbmember.Base.Activity.View.MvpView
import com.example.itstym.perbmember.Base.Fragment.BaseFragment

/**
 * Created by itstym on 6/12/17.
 */


abstract class BaseActivity: AppCompatActivity(), MvpView, BaseFragment.Callback {
    protected abstract fun setUp()


    protected abstract fun setUpToolbar()
    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {

    }
}