package com.example.itstym.perbmember.Network.Model

/**
 * Created by itstym on 6/12/17.
 */

data class BottomSheetModel (

        val itemIcon:Int,
        val itemName:String
)
