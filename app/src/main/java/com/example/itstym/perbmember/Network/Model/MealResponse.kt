package com.example.itstym.perbmember.Network.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by itstym on 13/12/17.
 */

data class MealResponse(

        @SerializedName("status")
        @Expose
        val status:Boolean,

        @SerializedName("diet_details")
        @Expose
        val dietPlanArrayList:ArrayList<Meal>
)