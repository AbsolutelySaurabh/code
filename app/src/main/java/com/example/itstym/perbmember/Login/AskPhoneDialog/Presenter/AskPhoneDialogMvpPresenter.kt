package com.example.itstym.perbmember.Login.AskPhoneDialog.Presenter

import android.content.Context
import android.widget.EditText
import com.example.itstym.perbmember.Base.Dialog.Presenter.DialogMvpPresenter
import com.example.itstym.perbmember.Login.AskPhoneDialog.AskPhoneDialog
import com.example.itstym.perbmember.Login.AskPhoneDialog.View.AskPhoneView
import me.zhanghai.android.materialprogressbar.MaterialProgressBar

/**
 * Created by itstym on 6/12/17.
 */


interface AskPhoneDialogMvpPresenter<in V: AskPhoneView> : DialogMvpPresenter<V> {

    fun attemptLogin(userPhone: EditText, context: Context, progressBarLogin: MaterialProgressBar, askPhoneDialog: AskPhoneDialog)

    fun checkIfMemberExists(userPhone: String, context: Context, progressBarLogin: MaterialProgressBar, askPhoneDialog: AskPhoneDialog)

}