package com.example.itstym.perbmember.Meal.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by itstym on 11/1/18.
 */


data class DietPlanObject(
        @SerializedName("diet_id")
        @Expose
        val dietId:Int,

        @SerializedName("diet_name")
        @Expose
        val dietPlanName:String
)