package com.example.itstym.perbmember.PersonalDetails

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import com.example.itstym.perbmember.Base.Activity.BaseActivity
import com.example.itstym.perbmember.PerbMemberApplication
import com.example.itstym.perbmember.PersonalDetails.Presenter.PersonalDetailsPresenter
import com.example.itstym.perbmember.PersonalDetails.View.PersonalDetailsView
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.Utils.HelperUtils
import kotlinx.android.synthetic.main.personal_details_activity_layout.*

/**
 * Created by itstym on 11/12/17.
 */



class PersonalDetailsActivity: BaseActivity(), PersonalDetailsView {

    override fun setUpToolbar() {

    }

    companion object {
        fun getStartIntent(context: Context) : Intent = Intent(context,PersonalDetailsActivity::class.java)
    }

    override fun setUp() {
        val dataManager = (application as PerbMemberApplication).getDataManager()

        mPersonalPresenter = PersonalDetailsPresenter(dataManager)

        mPersonalPresenter.onAttach(this)

        setSupportActionBar(toolbarPersonalDetail)
        supportActionBar!!.setTitle("Personal Details")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_action_back)
        }

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)



        //set all the personal details
        mPersonalPresenter.setPersonalInfo()

    }

    override fun showPersonalInfo(memberNameText: String, memberPhoneText: String, memberProfileurlText: String, memberEmailText: String, memberGenerText: String, memberDOjText: String, memberAddressText: String) {

        memberNamePersonal.text=memberNameText
        memberPhonePersonal.text=memberPhoneText
        memberEmail.text=memberEmailText
        memberGender.text=memberGenerText
        memberDojDate.text=HelperUtils.getDateInFormat(HelperUtils.getTimeInMills(memberDOjText.split("T")[0],"yyyy-MM-dd"),"yyyy-MM-dd")
        memberAddress.text=memberAddressText

    }


    lateinit var  mPersonalPresenter: PersonalDetailsPresenter<PersonalDetailsView>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.personal_details_activity_layout)

        setUp()
    }

    override fun onResume() {
        super.onResume()


    }

   override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                // API 5+ solution
                onBackPressed()
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }


}
