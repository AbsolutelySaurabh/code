package com.example.itstym.perbmember.ViewWorkout

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.constraint.ConstraintLayout
import android.support.design.widget.BottomSheetDialog
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.example.itstym.perbmember.Base.Activity.BaseActivity
import com.example.itstym.perbmember.Dashboard.DashboardActivity
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.PerbMemberApplication
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.ViewWorkout.Adaptor.BodyPartShimmer
import com.example.itstym.perbmember.ViewWorkout.Adaptor.DaysBottomSheetAdapter
import com.example.itstym.perbmember.ViewWorkout.Model.BodyPart
import com.example.itstym.perbmember.ViewWorkout.Model.Exercise
import com.example.itstym.perbmember.ViewWorkout.Presenter.WorkoutPresenter
import com.example.itstym.perbmember.ViewWorkout.View.WorkoutView
import kotlinx.android.synthetic.main.days.*
import kotlinx.android.synthetic.main.workout_activity_layout.*
import kotlinx.android.synthetic.main.workout_activity_layouts.*
import me.zhanghai.android.materialprogressbar.MaterialProgressBar

/**
 * Created by itstym on 6/12/17.
 */


class WorkoutActivity: BaseActivity(), WorkoutView {


    override fun showToast(message: String) {

        Toast.makeText(this@WorkoutActivity, message,Toast.LENGTH_LONG).show()

    }
    override fun addWorkouttButtonClicked() {
        //open bottom sheet

        val mBottomSheetDialog= BottomSheetDialog(this@WorkoutActivity)

        val sheetView= layoutInflater.inflate(R.layout.workout_bottom_sheet,null)

        val dayRecyclerView=sheetView.findViewById<View>(R.id.daysRecyclerView) as RecyclerView
        val numDaysSelected=sheetView.findViewById<View>(R.id.numSelectedDays) as TextView
        //val dayRecyclerView=sheetView.findViewById<View>(R.id.daysRecyclerView) as RecyclerView

        val numdays=(mWorkoutPresenter.selectedDays.size)-1
        val distinctArray : ArrayList<Int> = ArrayList()
        distinctArray.clear()
        var before = 0
        for(i in  mWorkoutPresenter.selectedDays){
            if(i!=before){
                distinctArray.add(i)
                before = i
            }
        }

        dayRecyclerView.setHasFixedSize(true)
        dayRecyclerView.layoutManager=LinearLayoutManager(this@WorkoutActivity, LinearLayoutManager.HORIZONTAL, false)

        dayRecyclerView.adapter= DaysBottomSheetAdapter(this@WorkoutActivity, mWorkoutPresenter.getDays(), object :DaysBottomSheetAdapter.DaysSelected{
            override fun onDaysSelected(position: Int) {

                Log.e("Days Selected",position.toString())




                for(i in  mWorkoutPresenter.selectedDays){
                    Log.e("days before", i.toString()
                    )
                }
                //check if already added or not
                if(distinctArray.contains(position+1)){
                    distinctArray.remove(position+1)
                }else
                    distinctArray.add(position+1)



                val numdays=(distinctArray.size)

                numDaysSelected.text="Selected "+numdays+" Days"

                for(i in  mWorkoutPresenter.selectedDays){
                    Log.e("days after", i.toString()
                    )
                }
            }

        }, mWorkoutPresenter.selectedDays[0])

        val confirmButton=sheetView.findViewById<ConstraintLayout>(R.id.confirmButton) as ConstraintLayout
        val progressbar = sheetView.findViewById<MaterialProgressBar>(R.id.progressBarbottomsheet) as MaterialProgressBar

        confirmButton.setOnClickListener {

            //move to next bottom
            progressbar.visibility = View.VISIBLE

            mWorkoutPresenter.onConfirmButtonClicked(this@WorkoutActivity ,distinctArray,progressbar,mBottomSheetDialog)
        }

        mBottomSheetDialog.setContentView(sheetView)

        mBottomSheetDialog.show()
    }

    override fun showConfirmDialog() {

        val mBottomSheetDialog= BottomSheetDialog(this@WorkoutActivity)

        val sheetView= layoutInflater.inflate(R.layout.workout_confirm_layout,null)

        mBottomSheetDialog.setContentView(sheetView)

        Handler().postDelayed( {
            mBottomSheetDialog.dismiss()

            startActivity(DashboardActivity.getStartIntent(this@WorkoutActivity))
            finish()
        }, 3000);

        mBottomSheetDialog.show()
    }

    override fun onResume() {
        super.onResume()

        workoutSubmitButton.setOnClickListener {

            Log.e("Selected days ",mWorkoutPresenter.selectedDays.toString())

            mWorkoutPresenter.workOutSubmitButtonClicked()
        }

        //
        mon.setOnClickListener {
            setAllDaysDisable()
            setUpTextColor(mon,R.color.colorAccent)
            mWorkoutPresenter.upDateWorkout(1,this@WorkoutActivity)
        }

        tue.setOnClickListener {
            setAllDaysDisable()
            setUpTextColor(tue,R.color.colorAccent)
            mWorkoutPresenter.upDateWorkout(2,this@WorkoutActivity)
        }

        wed.setOnClickListener {
            setAllDaysDisable()
            setUpTextColor(wed,R.color.colorAccent)
            mWorkoutPresenter.upDateWorkout(3,this@WorkoutActivity)
        }

        thu.setOnClickListener {
            setAllDaysDisable()
            setUpTextColor(thu,R.color.colorAccent)
            mWorkoutPresenter.upDateWorkout(4,this@WorkoutActivity)
        }

        fri.setOnClickListener {
            setAllDaysDisable()
            setUpTextColor(fri,R.color.colorAccent)
            mWorkoutPresenter.upDateWorkout(5,this@WorkoutActivity)
        }

        sat.setOnClickListener {
            setAllDaysDisable()
            setUpTextColor(sat,R.color.colorAccent)

            mWorkoutPresenter.upDateWorkout(6,this@WorkoutActivity)
        }

        sun.setOnClickListener {
            setAllDaysDisable()
            setUpTextColor(sun,R.color.colorAccent)
            mWorkoutPresenter.upDateWorkout(7,this@WorkoutActivity)
        }






    }

    private fun setAllDaysDisable() {

        setUpTextColor(mon,R.color.newColorIntroducedDesigner)
        setUpTextColor(tue,R.color.newColorIntroducedDesigner)
        setUpTextColor(wed,R.color.newColorIntroducedDesigner)
        setUpTextColor(thu,R.color.newColorIntroducedDesigner)
        setUpTextColor(fri,R.color.newColorIntroducedDesigner)
        setUpTextColor(sat,R.color.newColorIntroducedDesigner)
        setUpTextColor(sun,R.color.newColorIntroducedDesigner)

    }


    override fun openDaysBottomSheet() {


    }

    lateinit var dataManager: DataManager
    private lateinit var mWorkoutPresenter: WorkoutPresenter<WorkoutView>

    override fun setUp() {

        setUpToolbar()

        dataManager = (application as PerbMemberApplication).getDataManager()
        mWorkoutPresenter = WorkoutPresenter(dataManager)
        mWorkoutPresenter.onAttach(this)
        workoutSubmitButton.visibility = View.GONE
        textView5.text = "Workout"
        errorEmpty.text="No Workout available\n" +
                "please update from add workout section"

        recyclerViewBodyPart.layoutManager = LinearLayoutManager(this@WorkoutActivity,LinearLayoutManager.HORIZONTAL,false)
        recyclerViewExercise.layoutManager = LinearLayoutManager(this@WorkoutActivity,LinearLayoutManager.VERTICAL,false)

        val demoBodyPart :ArrayList<BodyPart> = ArrayList()
        val demoExercise:ArrayList<Exercise> = ArrayList()
        for(i in 1..3){
            demoBodyPart.add(BodyPart(0,"","",false, ArrayList()))
            demoExercise.add(Exercise(0,"","",0,false, ArrayList()))

        }
        recyclerViewBodyPart.adapter = BodyPartShimmer(this@WorkoutActivity,demoBodyPart)
       // recyclerViewExercise.adapter = ExerciseAdapterShimmer(this@WorkoutActivity,demoExercise)




        mWorkoutPresenter.getAllWorkout(dataManager.getMemberId(), this@WorkoutActivity,recyclerViewBodyPart,recyclerViewExercise,errorEmpty);

        mWorkoutPresenter.setTodayDay()
    }

    override fun highLightTodayDay(todayDays: Int) {
        Log.e("Today day",todayDays.toString())

        when(todayDays-1){

            0 -> setUpTextColor(sun,R.color.colorAccent)

            1 -> setUpTextColor(mon,R.color.colorAccent)

            2 -> setUpTextColor(tue,R.color.colorAccent)

            3 -> setUpTextColor(wed,R.color.colorAccent)

            4 -> setUpTextColor(thu,R.color.colorAccent)

            5 -> setUpTextColor(fri,R.color.colorAccent)

            6 -> setUpTextColor(sat,R.color.colorAccent)
        }


    }

    private fun setUpTextColor(textView: TextView, colorCode: Int){

        textView.setTextColor(ContextCompat.getColor(this@WorkoutActivity, colorCode))
    }


    override fun setUpToolbar() {

        toolbar.title="View Workout".toUpperCase()
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
       // supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
    }



    companion object {
        fun getStartIntent(context: Context) : Intent = Intent(context, WorkoutActivity::class.java)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.workout_activity_layouts)

        setUp()
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                // API 5+ solution
                onBackPressed()
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }
}