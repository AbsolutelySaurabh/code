package com.example.itstym.perbmember.ViewMeal.Adaptor

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.example.itstym.perbmember.Network.Model.Diet
import com.example.itstym.perbmember.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.diet_plan_data.view.*

/**
 * Created by itstym on 12/12/17.
 */

class DietPlanAdaptor(var context: Context, var dietPlanData: ArrayList<Diet>, var dietPlanSelection: DietPlanSelection, var abc: Int, var firstTime: Boolean, var pos: Int, var selected: Boolean, var SelectedditePlan: String): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    interface DietPlanSelection{

        fun onDietPlanSelection(dietPlan: Diet,position: Int,selected:Boolean)
    }

    override fun getItemCount(): Int {
        return dietPlanData.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        // Log.i("Adaptor viewtype ", Integer.toString(viewType))

        val v = LayoutInflater.from(context).inflate(R.layout.diet_plan_data, parent, false)
        return Item(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {

        (holder as Item).bindData(context, dietPlanData[position], dietPlanSelection = dietPlanSelection, selectedDay = abc, position = position , firstTime = firstTime, selectedPOS = pos, selected = selected, dietSelectedName = dietPlanSelection,SelectedditePlan = SelectedditePlan)
    }

}

class Item(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindData(context: Context, dietPlan: Diet, dietPlanSelection: DietPlanAdaptor.DietPlanSelection, selectedDay: Int, position: Int, firstTime:Boolean, selectedPOS:Int, selected: Boolean, dietSelectedName: DietPlanAdaptor.DietPlanSelection,SelectedditePlan:String) {


        var positionSelected :Boolean =selected
        itemView.diet_plan_name.text= dietPlan.dietName


            if(dietPlan.dietName.equals(SelectedditePlan)){

                itemView.relativeLayout.isSelected = true
                positionSelected=true
                itemView.diet_plan_name.isSelected=true
                itemView.diet_plan_name.setTextColor(ContextCompat.getColor(context, android.R.color.white))
                dietPlanSelection.onDietPlanSelection(dietPlan,position,false)



            }else{

                itemView.relativeLayout.visibility =View.GONE
            }




    }

    private fun loadImage(imageIcon: Int, context: Context, imageView: ImageView) {

        Picasso.with(context).load(imageIcon).into(imageView)
    }

}