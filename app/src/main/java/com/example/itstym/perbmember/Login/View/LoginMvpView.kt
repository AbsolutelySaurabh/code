package com.example.itstym.perbmember.Login.View

import android.content.Context
import android.content.Intent
import android.view.View
import com.example.itstym.perbmember.Base.Activity.View.MvpView

/**
 * Created by itstym on 6/12/17.
 */


interface LoginMvpView : MvpView {

    fun openLoginDialog(context: Context, loginBackground: View)
    fun openGmailApp(intent: Intent)

    fun openCallView(intent: Intent)

}