package com.example.itstym.perbmember.Meal.Adaptor

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.itstym.perbmember.Network.Model.Diet
import com.example.itstym.perbmember.R
import kotlinx.android.synthetic.main.dietplanshimmer.view.*

/**
 * Created by mukul on 1/3/18.
 */
class DietPlanShimmer(var context: Context, var dietPlanData: ArrayList<Diet>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {



    override fun getItemCount(): Int {
        return dietPlanData.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        // Log.i("Adaptor viewtype ", Integer.toString(viewType))

        val v = LayoutInflater.from(context).inflate(R.layout.dietplanshimmer, parent, false)
        return ItemDietShimmer(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {

        (holder as ItemDietShimmer).bindData(context, dietPlanData[position])
    }

}

class ItemDietShimmer(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindData(context: Context, dietPlan: Diet ) {

        itemView.shimmerDietPlan.startShimmerAnimation()


    }



}