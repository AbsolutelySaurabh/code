package com.example.itstym.perbmember.Network.RetrofitHelper

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by itstym on 22/11/17.
 */


class RetrofitClient {


    companion object {

        private var retrofit: Retrofit?=null

        fun getClient(baseUrl:String): Retrofit? {

            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY

            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor(logging)
            httpClient.retryOnConnectionFailure(true)

            if (retrofit == null)
            {
                retrofit = Retrofit.Builder()
                        .baseUrl(baseUrl)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(httpClient.build())
                        .build()
            }
            return retrofit
        }
    }

}