package com.example.itstym.perbmember.TransactionHistory.Presenter

import android.app.Activity
import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import com.example.itstym.perbmember.Base.Activity.Presenter.BasePresenter
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.Network.Model.TransactionResponse
import com.example.itstym.perbmember.Network.RetrofitHelper.ApiUtils
import com.example.itstym.perbmember.TransactionHistory.Adaptor.TransactionHistoryAdaptor
import com.example.itstym.perbmember.TransactionHistory.View.TransactionHistoryView
import com.example.itstym.perbmember.Utils.HelperUtils
import retrofit2.Call
import retrofit2.Response

/**
 * Created by itstym on 11/12/17.
 */


class TransactionHistoryPresenter<V : TransactionHistoryView>(dataManager: DataManager) : BasePresenter<V>(dataManager), TransactionHistoryMvpPresenter<V> {

    override fun setUpRecyclerView(context: Context, recyclerView: RecyclerView, verticleBar: View) {
        Log.e("set up","transaction")

      //  val alertDialog = HelperUtils.showDataSendingDialog(context, "Fetching Transaction History ... ")

        val mApiService = ApiUtils.getAPIService()

        mApiService.getTransactionHistory(dataManager.getMemberId()).enqueue(object : retrofit2.Callback<TransactionResponse> {
            override fun onFailure(call: Call<TransactionResponse>?, t: Throwable?) {

              //  HelperUtils.closeDialog(alertDialog)
                HelperUtils.showSnackBar("Network Issue", context as Activity)
            }

            override fun onResponse(call: Call<TransactionResponse>?, response: Response<TransactionResponse>?) {

               // HelperUtils.closeDialog(alertDialog)

                if (response!!.body()!!.status){

                    recyclerView.layoutManager=LinearLayoutManager(context)
                    recyclerView.adapter=TransactionHistoryAdaptor(context, response.body()!!.transactionHistoryArrayList)
                    verticleBar.visibility = View.VISIBLE

                }
            }
        })
    }

}
