package com.example.itstym.perbmember.Feed.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.itstym.perbmember.Network.Model.Blog
import com.example.itstym.perbmember.R
import com.google.android.youtube.player.YouTubePlayer
import kotlinx.android.synthetic.main.blogshimmer.view.*

/**
 * Created by mukul on 12/27/17.
 */
class ShimmerAdaptor(var context: Context, var blogDetails:ArrayList<Blog>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    private val YPlayer: YouTubePlayer? = null
    private val YoutubeDeveloperKey = "xyz"
    private val RECOVERY_DIALOG_REQUEST = 1

    override fun getItemCount(): Int {
        return blogDetails.size
    }

    interface feedItem{

        fun onClickBlog(blogData: Blog)

        fun onCommentClicked(blogData: Blog)

    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        // Log.i("Adaptor viewtype ", Integer.toString(viewType))

        val v = LayoutInflater.from(context).inflate(R.layout.blogshimmer, parent, false)
        return Item(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {

        (holder as Item).bindData(blogDetails[position]!!,context)

    }

    class Item(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindData(blogData: Blog, context: Context) {


            itemView.blogShimmer.startShimmerAnimation()







        }

    }

}


