package com.example.itstym.perbmember.Meal.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DietDetail {

    @SerializedName("diet_id")
    @Expose
    var dietId: Int? = null
    @SerializedName("diet_name")
    @Expose
    var dietName: String? = null
    @SerializedName("gym_id")
    @Expose
    var gymId: Int? = null
    @SerializedName("meal_id")
    @Expose
    var mealId: Int? = null
    @SerializedName("meal_name")
    @Expose
    var mealName: String? = null
    @SerializedName("alarm_time")
    @Expose
    var alarmTime: String? = null
    @SerializedName("food_id")
    @Expose
    var foodId: Int? = null
    @SerializedName("food_item")
    @Expose
    var foodItem: String? = null

}
