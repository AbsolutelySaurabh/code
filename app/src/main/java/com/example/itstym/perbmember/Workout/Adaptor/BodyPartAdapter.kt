package com.example.itstym.perbmember.Workout.Adaptor

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.Workout.Model.BodyPart
import com.example.itstym.perbmember.Workout.Presenter.WorkoutPresenter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.body_part_item.view.*

/**
 * Created by itstym on 6/12/17.
 */


class BodyPartAdapter(var context: Context, var bodyPartData: ArrayList<BodyPart>, var bodyPartSelection: BodyPartSelection, var abc: Int, var firsttime: Boolean, var todayBodypartid: Int, b1: Boolean,var selectedBodypartId: Int, var bodyPartSelected: Boolean): RecyclerView.Adapter<RecyclerView.ViewHolder>() {




    interface BodyPartSelection{

        fun onbodyPartClicked(firsttime:Boolean,bodyPart: BodyPart,selected:Boolean)
    }

    override fun getItemCount(): Int {
        return bodyPartData.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        // Log.i("Adaptor viewtype ", Integer.toString(viewType))


        val v = LayoutInflater.from(context).inflate(R.layout.body_part_item, parent, false)
        return Item(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {

        (holder as Item).bindData(context, bodyPartData[position], bodyPartSelection = bodyPartSelection, selectedDay = abc, adapter = this@BodyPartAdapter,todayBodypartid = todayBodypartid, firstTime = firsttime,selectedBodypartId = selectedBodypartId,isBodypartSelected = bodyPartSelected)
    }




}

class Item(itemView: View) : RecyclerView.ViewHolder(itemView) {


    fun bindData(context: Context, bodyPart: BodyPart, bodyPartSelection: BodyPartAdapter.BodyPartSelection, selectedDay: Int, adapter: BodyPartAdapter, todayBodypartid: Int,firstTime:Boolean,selectedBodypartId: Int,isBodypartSelected:Boolean) {
        Log.e("Today Body Part",todayBodypartid.toString())

            itemView.bodyPartName.text = bodyPart.body_part_name






            if (WorkoutPresenter.firstTime1) {


                Log.e("First time","True")

                if (bodyPart.body_part_id==todayBodypartid) {

                   // adapter.update(adapterPosition, false,bodyPart.body_part_id,itemView.relativeLayout.isSelected)

                    itemView.relativeLayout.isSelected = true
                   // Log.e("first time", BodyPartAdapter.firstTime.toString())

                    itemView.body_part_image.setColorFilter(Color.argb(255, 255, 255, 255))
                    bodyPartSelection.onbodyPartClicked(true,bodyPart = bodyPart, selected = itemView.relativeLayout.isSelected)


                }
                else{
                    itemView.relativeLayout.isSelected = false
                    itemView.body_part_image.setColorFilter(Color.parseColor("#e0e0e0"))
                }
            }else{

                if(bodyPart.body_part_id==WorkoutPresenter.selectedBodyPartid){
                    Log.e("currentstatus",WorkoutPresenter.isBodyBodyPartSelected.toString())

                    if(WorkoutPresenter.isBodyBodyPartSelected){
                        itemView.relativeLayout.isSelected = true
                        Log.e("currentstatus",WorkoutPresenter.isBodyBodyPartSelected.toString())
                        itemView.body_part_image.setColorFilter(Color.argb(255, 255, 255, 255))


                    }
                    else{
                        itemView.relativeLayout.isSelected = false
                        itemView.body_part_image.setColorFilter(Color.parseColor("#e0e0e0"))

                        // Log.e("first time", BodyPartAdapter.firstTime.toString())

                    }
                }
                else{
                    itemView.relativeLayout.isSelected = false
                    itemView.body_part_image.setColorFilter(Color.parseColor("#e0e0e0"))
                }

            }

            Log.e("Image uri", bodyPart.body_part_url)

            loadImage(bodyPart.body_part_url, context, imageView = itemView.body_part_image)

           // adapter.updatePrevBodyPart(bodyPart.body_part_name)




            itemView.setOnClickListener {

                if (itemView.relativeLayout.isSelected) {
                    itemView.relativeLayout.isSelected = false
                    Log.e(bodyPart.body_part_id.toString(),itemView.relativeLayout.isSelected.toString() )
                    bodyPartSelection.onbodyPartClicked(false,bodyPart = bodyPart, selected = false)
                    WorkoutPresenter.upadteStaticValuesforRecyclerView(false,bodyPart.body_part_id,false)
                    itemView.body_part_image.setColorFilter(Color.parseColor("#e0e0e0"))
                } else {
                    itemView.relativeLayout.isSelected = true
                    Log.e(bodyPart.body_part_id.toString(),itemView.relativeLayout.isSelected.toString() )

                    bodyPartSelection.onbodyPartClicked(false,bodyPart = bodyPart, selected = true)
                    WorkoutPresenter.upadteStaticValuesforRecyclerView(false,bodyPart.body_part_id,true)
                    itemView.body_part_image.setColorFilter(Color.argb(255, 255, 255, 255))
                }

                adapter.notifyDataSetChanged()


            }


    }
    private fun loadImage(imageIcon: String, context: Context, imageView: ImageView) {

        Picasso.with(context).load(imageIcon).into(imageView)

    }


}