package com.example.itstym.perbmember.Network.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by itstym on 8/12/17.
 */


data class Blog(

        @SerializedName("blog_id")
        @Expose
        val blog_id:Int,

        @SerializedName("title")
        @Expose
        val title:String,

        @SerializedName("content")
        @Expose
        val content:String,

        @SerializedName("blog_type")
        @Expose
        val blog_type:String,

        @SerializedName("blog_author")
        @Expose
        val blog_author:String,

        @SerializedName("thumbnail_image")
        @Expose
        val thumbnail_image:String,

        @SerializedName("video_url")
        @Expose
        val video_url:String,

        @SerializedName("blog_publish_date")
        @Expose
        val blog_publish_date:String

        )