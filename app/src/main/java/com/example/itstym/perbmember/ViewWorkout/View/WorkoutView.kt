package com.example.itstym.perbmember.ViewWorkout.View

import com.example.itstym.perbmember.Base.Activity.View.MvpView

/**
 * Created by itstym on 6/12/17.
 */

interface WorkoutView : MvpView {

    fun openDaysBottomSheet()

    fun addWorkouttButtonClicked()

    fun showConfirmDialog()

    fun highLightTodayDay(todayDays: Int)

    fun showToast(message:String)
}