package com.example.itstym.perbmember.Meal

import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Gravity
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.Meal.Model.MealViewPlan
import com.example.itstym.perbmember.Meal.Presenter.MealPresenter
import com.example.itstym.perbmember.Meal.View.MealView
import com.example.itstym.perbmember.R
import kotlinx.android.synthetic.main.custommealdailog.*
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by mukul on 1/10/18.
 */
class CustomMealDailog: Dialog {


    var mContext: Context? = null
    lateinit var uri: String




    private lateinit var mMealPresenter: MealPresenter<MealView>


    constructor(context: Context, uri: String,dataManager: DataManager) : super(context) {

        mMealPresenter = MealPresenter(dataManager)
        mContext = context
        this.uri = uri
    }

//    var selectedPromotion: Promotion? = null

//    constructor(context: Context, themeResId: Int) : super(context, themeResId) {
//
//        //selectedPromotion = promotion
//        mContext = context
//
//    }

    var isCustomText = false


    override fun onCreate(savedInstanceState: Bundle?) {
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
        super.onCreate(savedInstanceState)

        setContentView(R.layout.custommealdailog)// use your layout in place of this.
        setUp()

    }

    var message: String? = null
    private fun setUp() {

        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));

        val windowlp = window.attributes

        windowlp.gravity = Gravity.CENTER

        window.attributes = windowlp

        textInputTime.isEnabled =false
        addMealButton.isEnabled = false

        textInputText.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }
            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                if (s.toString().length>0){
                    textInputTime.isEnabled =true
                    addMealButton.isEnabled = true
                    addMealButton.setBackgroundColor(Color.parseColor("#ee3443"))


                }
                else{
                    textInputTime.isEnabled =false
                    addMealButton.isEnabled = false

                    addMealButton.setBackgroundColor(Color.parseColor("#f1f1f1"))

                }
            }
        })


            textInputTime.setOnClickListener{

                var hour = Calendar.getInstance().get(Calendar.HOUR)
                var minute = Calendar.getInstance().get(Calendar.MINUTE)

               var time= TimePickerDialog(context, TimePickerDialog.OnTimeSetListener
                { timePicker, selectedHour, selectedMinute -> textInputTime.setText(selectedHour.toString()
                        + ":" + selectedMinute) }, hour, minute, true)
                time.setTitle("Select Time")
                time.show()

            }


            addMealButton.setOnClickListener{

                mMealPresenter.customMeal(context,MealViewPlan(1,textInputText.text.toString(),textInputTime.text.toString(), ArrayList()))

                /*mMealPresenter.addMeal(context,uri,-1,true, MealItems(

                        textInputText.text.toString(),-1,-1,null,textInputTime.text.toString(),null
                ))
*/

            }


       // textInputText.addTextChangedListener()

        window.setWindowAnimations(R.style.dialog_animation_fadereal)


//
//        Picasso.with(context).load(uri).into(workoutImage)
//        closeDailog.setOnClickListener {
//            dismiss()
//        }
//
//        PhotoViewAttacher(workoutImage).update()


    }


    override fun onDetachedFromWindow() {
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
        super.onDetachedFromWindow()
    }
}