package com.example.itstym.perbmember.Profile

import android.app.Activity
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.example.itstym.perbmember.Base.Fragment.BaseFragment
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.Profile.View.ProfileView
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.SharedPrefsHelper
import com.example.itstym.perbmember.Utils.HelperUtils
import java.util.*


/**
 * Created by itstym on 11/12/17.
 */


class ProfileFragment: BaseFragment(), ProfileView {

    interface ProfileFragmentListener{

        fun onPersonalDetailsActivity()

        fun onTransactionHistoryActivity()

        fun onMealActivity()

        fun onWorkoutActivity()


    }


    override fun onResume() {
        super.onResume()

        mPersonalDetailsLayout.setOnClickListener {
            //open personal activity
            openPersonalDetailsActivity()
        }

        mTransactionHistoryLayout.setOnClickListener {
            transactionHistoryActivity()
        }

        mWorkoutLayout.setOnClickListener {
            HelperUtils.showSnackBar("Workout section Comming soom", context as Activity)
           // openWorkoutActivity()
        }

        mMealLayout.setOnClickListener{
            HelperUtils.showSnackBar("Meal section Comming soom", context as Activity)

            //  openMealActivity()
        }

    }


    override fun openPersonalDetailsActivity() {

        (context as ProfileFragmentListener).onPersonalDetailsActivity()
    }

    override fun openWorkoutActivity() {
        (context as ProfileFragmentListener).onWorkoutActivity()

    }

    override fun openMealActivity() {
        (context as ProfileFragmentListener).onMealActivity()

        //To change body of created functions use File | Settings | File Templates.
    }

    override fun transactionHistoryActivity() {

        (context as ProfileFragmentListener).onTransactionHistoryActivity()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.profile_fragment_layout, container, false)
        setup(view)

        return view
    }

    lateinit var mProfileFragmentPresenter: ProfileFragment
    lateinit var mPersonalDetailsLayout: ConstraintLayout
    lateinit var mTransactionHistoryLayout: ConstraintLayout
    lateinit var mMealLayout:ConstraintLayout
    lateinit var mWorkoutLayout:ConstraintLayout
    lateinit var firstChar:TextView
    lateinit var profileBackground:RelativeLayout
    lateinit var profileImage:ImageView



    override fun setup(view: View?) {

        Log.e(TAG,"setup")

        val dataManager= DataManager(SharedPrefsHelper(context!!))

        mProfileFragmentPresenter = ProfileFragment()
        mProfileFragmentPresenter.onAttach(context!!)

        val memberNameTextView= view?.findViewById<TextView>(R.id.memberName) as TextView
        val memberPhoneTextView= view?.findViewById<TextView>(R.id.memberPhone) as TextView
         mPersonalDetailsLayout= view?.findViewById<ConstraintLayout>(R.id.personalDetailsLayout) as ConstraintLayout
         mTransactionHistoryLayout= view?.findViewById<ConstraintLayout>(R.id.transactionHistoryLayout) as ConstraintLayout
        //set up basic require ment

        mMealLayout= view?.findViewById<ConstraintLayout>(R.id.mealLayout) as ConstraintLayout
        mWorkoutLayout= view?.findViewById<ConstraintLayout>(R.id.workoutLayout) as ConstraintLayout
        firstChar= view?.findViewById<TextView>(R.id.nameFirstChar) as TextView
        Log.e("Profile pic url",dataManager.getMemberProfilePic())
        firstChar.text = dataManager.getMemberName().get(0).toString()
        profileBackground= view?.findViewById<RelativeLayout>(R.id.profileBackground) as RelativeLayout
        profileImage= view?.findViewById<ImageView>(R.id.profileImage) as ImageView
        val toolbar = activity!!.findViewById<View>(R.id.toolbar) as Toolbar
        toolbar.setTitle("Profile")

        if(!dataManager.getMemberProfilePic().equals("new.jpg")||
                !dataManager.getMemberProfilePic().equals("abc.jpg")||
                !dataManager.getMemberProfilePic().equals("123.jpg")||
                !dataManager.getMemberProfilePic().equals("-1.jpg")||
                !dataManager.getMemberProfilePic().equals("")
                ){
         //   Picasso.with(context).load(dataManager.getMemberProfilePic()).resize(500,500).centerCrop().transform(Circletransform()).into(profileImage)


        }




        val r = Random()
        val red = r.nextInt(255 - 0 + 1) + 0
        val green = r.nextInt(255 - 0 + 1) + 0
        val blue = r.nextInt(255 - 0 + 1) + 0

        val draw = GradientDrawable()
        draw.shape = GradientDrawable.OVAL
        draw.setColor(Color.rgb(red, green, blue))
        profileBackground.setBackground(draw)






        memberNameTextView.text=dataManager.getMemberName()
        memberPhoneTextView.text=dataManager.getMemberPhone()

    }



    companion object {

        fun newInstance(): ProfileFragment {
            val args= Bundle()
            val profileFragment: ProfileFragment = ProfileFragment();
            profileFragment.arguments=args
            return profileFragment
        }

        val TAG="profileFragment"

    }



}
