package com.example.itstym.perbmember.Meal.Presenter

import android.content.Context
import android.support.design.widget.BottomSheetDialog
import android.support.v7.widget.RecyclerView
import com.example.itstym.perbmember.Base.Activity.Presenter.MvpPresenter
import com.example.itstym.perbmember.Meal.MealActivity
import com.example.itstym.perbmember.Meal.Model.MealViewPlan
import com.example.itstym.perbmember.Meal.View.MealView
import com.example.itstym.perbmember.Meal.newmodel.SendMealModel
import me.zhanghai.android.materialprogressbar.MaterialProgressBar

/**
 * Created by itstym on 12/12/17.
 */



interface MealMvpPresenter<in V: MealView> : MvpPresenter<V> {


    fun getAllMeals(context: Context, dietPlanRecyclerView: RecyclerView, dietFoodItemRecyclerView: RecyclerView, i: Int)

    //fun setTodayDay()

    fun mealSubmitClicked(mealActivity: MealActivity)

    //fun onConfirmButtonClicked(context: Context)

    fun getDays():ArrayList<String>

    //fun upDateDietPlanView(day:Int, context: Context)

    //fun addMeal(context: Context, addMeal:String, customDietPlanId:Int, alreadyAdded:Boolean, mealItem: MealItems?)

    fun sendMeal(context: Context, meal: SendMealModel, progressBar: MaterialProgressBar, mBottomSheetDialog: BottomSheetDialog)

    fun customMeal(context:Context,meal:MealViewPlan)
}