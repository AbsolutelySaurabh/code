package com.example.itstym.perbmember.Workout

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import com.example.itstym.perbmember.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.workoutimagedailog.*
import uk.co.senab.photoview.PhotoViewAttacher

/**
 * Created by mukul on 1/3/18.
 */
class WorkoutImage: Dialog {


    var mContext: Context? = null
    lateinit var uri:String

    constructor(context: Context) : super(context) {
        mContext = context

    }

    constructor(context: Context,uri:String, themeResId: Int) : super(context,themeResId) {
        mContext = context
        this.uri = uri
    }

//    var selectedPromotion: Promotion? = null

//    constructor(context: Context, themeResId: Int) : super(context, themeResId) {
//
//        //selectedPromotion = promotion
//        mContext = context
//
//    }

    var isCustomText = false




    override fun onCreate(savedInstanceState: Bundle?) {
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
        super.onCreate(savedInstanceState)

        setContentView(R.layout.workoutimagedailog)// use your layout in place of this.
        setUp()

    }
    var message: String? = null
    private fun setUp() {

        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));

        val windowlp = window.attributes

        windowlp.gravity = Gravity.CENTER

        window.attributes = windowlp


        window.setWindowAnimations(R.style.dialog_animation_fadereal)



        Picasso.with(context).load(uri).into(workoutImage)
        closeDailog.setOnClickListener{
            dismiss()
        }

        PhotoViewAttacher(workoutImage).update()


    }



    override fun onDetachedFromWindow() {
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
        super.onDetachedFromWindow()
    }
}