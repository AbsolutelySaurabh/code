package com.example.itstym.perbmember.Dashboard.Adaptor

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.itstym.perbmember.Network.Model.BottomSheetModel
import com.example.itstym.perbmember.R
import kotlinx.android.synthetic.main.bottom_sheet_item.view.*

/**
 * Created by itstym on 6/12/17.
 */


class BottomSheetDialogAdaptor(var context: Context, var bottomSheetItems:ArrayList<BottomSheetModel>, var bottomSheetDialogListener:BottomSheetListener): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    interface BottomSheetListener{

        fun onBottomItemClick(position: Int)
    }

    override fun getItemCount(): Int {
        return bottomSheetItems.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        // Log.i("Adaptor viewtype ", Integer.toString(viewType))

        val v = LayoutInflater.from(context).inflate(R.layout.bottom_sheet_item, parent, false)
        return Item(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {

        (holder as Item).bindData(bottomSheetItems.get(position),context,bottomSheetDialogListener)

    }

}

class Item(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindData(bottomItem: BottomSheetModel, context: Context, bottomSheetDialogListener:BottomSheetDialogAdaptor.BottomSheetListener) {


//        if (bottomItem.itemIcon!=0)
//            Picasso.with(context).load(bottomItem.itemIcon).into(itemView.imageIcon)

        when(bottomItem.itemIcon){

            1 -> itemView.imageIcon.setImageResource(R.drawable.addworkout)
            2 -> itemView.imageIcon.setImageResource(R.drawable.addmeal)
            3 -> itemView.imageIcon.setImageResource(R.drawable.trackwater)
            4 -> itemView.imageIcon.setImageResource(R.drawable.trackweight)

        }


        //set the name
        itemView.title.text=bottomItem.itemName

        //on click listener
        itemView.setOnClickListener {
            bottomSheetDialogListener.onBottomItemClick(position = adapterPosition)
        }

    }

}