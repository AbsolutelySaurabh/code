package com.example.itstym.perbmember.PersonalDetails.Presenter

import android.content.Context
import android.widget.ImageView
import com.example.itstym.perbmember.Base.Activity.Presenter.MvpPresenter
import com.example.itstym.perbmember.PersonalDetails.View.PersonalDetailsView
import com.example.itstym.perbmember.Splash.View.SplashMvpView

/**
 * Created by itstym on 11/12/17.
 */


interface PersonalDetailsMvpPresenter<in V: PersonalDetailsView> : MvpPresenter<V> {

    fun setPersonalInfo()
}
