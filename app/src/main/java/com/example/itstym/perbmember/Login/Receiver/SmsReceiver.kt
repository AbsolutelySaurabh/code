package com.example.itstym.perbmember.Login.Receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.provider.Telephony
import android.support.v4.content.LocalBroadcastManager
import android.telephony.SmsMessage
import android.util.Log
import java.util.*


/**
 * Created by itstym on 14/12/17.
 */

open class SmsReceiver : BroadcastReceiver() {

    companion object {

        var smsListener: SmsListener? = null

        fun bindListener(listener: SmsListener) {
            smsListener = listener
        }
    }

    override fun peekService(myContext: Context?, service: Intent?): IBinder {
        return super.peekService(myContext, service)
    }

    override fun onReceive(context: Context?, intent: Intent?) {

        var bundleExtras = intent?.extras

        if (bundleExtras != null) {

            val pdus = bundleExtras.get("pdus") as Array<Object>

            Log.e("pdus ", pdus.toString())

            for (i in 0 until pdus.size) {

                var smsMessage: SmsMessage? = null


                if (Build.VERSION.SDK_INT > 19) {
                    smsMessage = Telephony.Sms.Intents.getMessagesFromIntent(intent)[i]
                } else {
                    smsMessage = SmsMessage.createFromPdu(pdus[0] as ByteArray)
                }

                Log.e("Sms ", smsMessage.toString())


                val sender = smsMessage.getDisplayOriginatingAddress()
                //You must check here if the sender is your provider and not another one with same text.

                var messageBody = smsMessage.getDisplayMessageBody()
                messageBody = messageBody.substring(0, messageBody.length)
                val intent1 = Intent("otp")
                intent1.putExtra("smsMessage", messageBody)

                LocalBroadcastManager.getInstance(context!!).sendBroadcast(intent1)

                if (sender.contains("HP-perbin", true)) {

                    smsListener?.onMessageRecevied(messageBody, sender)
                }

            }
        }
    }

    interface onOtpSmsListener {

        fun onOtpReceived(format: String, smsBundle: Objects)
    }


}