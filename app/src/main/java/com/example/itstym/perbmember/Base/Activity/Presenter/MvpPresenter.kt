package com.example.itstym.perbmember.Base.Activity.Presenter

import com.example.itstym.perbmember.Base.Activity.View.MvpView

/**
 * Created by itstym on 6/12/17.
 */

interface MvpPresenter<in V : MvpView> {

    fun onAttach(mvpView: V)

    fun onDetach()

    fun setUserAsLoggedOut()

}
