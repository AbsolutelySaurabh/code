package com.example.itstym.perbmember.Feed

import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerView

/**
 * Created by mukul on 12/25/17.
 */

class YoutubeInitialise : YouTubeBaseActivity() {

   // lateinit var youTubePlayerView: YouTubePlayerView

    companion object {


        fun setYouTubePlayerView(youTubePlayerView: YouTubePlayerView, videoUrl: String, devID: String) {

            youTubePlayerView.initialize(devID, object : YouTubePlayer.OnInitializedListener {
                override fun onInitializationSuccess(provider: YouTubePlayer.Provider, youTubePlayer: YouTubePlayer, b: Boolean) {
                    youTubePlayer.loadVideo(videoUrl)
                }

                override fun onInitializationFailure(provider: YouTubePlayer.Provider, youTubeInitializationResult: YouTubeInitializationResult) {

                }
            })

            //this.youTubePlayerView = youTubePlayerView
        }
        //    YoutubeInitialise(
        //
        //            YouTubePlayerView youTubePlayerView){
        //
        //        this.youTubePlayerView = youTubePlayerView;
        //
        //    }

    }
}
