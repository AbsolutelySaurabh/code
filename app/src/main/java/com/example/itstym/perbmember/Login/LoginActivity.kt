package com.example.itstym.perbmember.Login

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import com.example.itstym.perbmember.Base.Activity.BaseActivity
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.Login.AskPhoneDialog.AskPhoneDialog
import com.example.itstym.perbmember.Login.Presenter.LoginPresenter
import com.example.itstym.perbmember.Login.View.LoginMvpView
import com.example.itstym.perbmember.PerbMemberApplication
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.Utils.PermissionUtils
import kotlinx.android.synthetic.main.login_activity.*


/**
 * Created by itstym on 6/12/17.
 */


class LoginActivity : BaseActivity(), LoginMvpView, AskPhoneDialog.askPermissionForMessage, PermissionUtils.PermissionResultCallback {


    override fun openGmailApp(intent: Intent) {
        startActivity(intent)

    }

    override fun openCallView(intent: Intent) {
        startActivity(intent)
    }


    override fun PermissionGranted(request_code: Int) {


    }

    override fun PartialPermissionGranted(request_code: Int, granted_permissions: ArrayList<String>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun PermissionDenied(request_code: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun NeverAskAgain(request_code: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onMessageReceived() {

        Log.e("Asking message ", "ues")

        //ask for permission

    }
    override fun setUpToolbar() {
        //nothing need to done
    }
    override fun onBackPressed() {
        super.onBackPressed()
        Log.e("Back pressed ","login")
    }

    override fun onResume() {
        super.onResume()

        loginButton.setOnClickListener {


            //open login dialog

            openLoginDialog(context = this@LoginActivity, loginBackground = loginBackground)
        }

        textView3.setOnClickListener {
            //open gmail app
            mLoginPresenter.openGmail(" teamperb@gmail.com", "Unable to Login")
        }


        textView4.setOnClickListener {

            permissions = ArrayList()


            permissions.add(android.Manifest.permission.CALL_PHONE)
        //    if(mPermissionUtils.check_permission(permissions, "Need Permission for getting otp sms", 1)){

            //open phone dialer
            mLoginPresenter.makeACall("+91-8826863734")
        }
    }


    companion object {

        fun getStartIntent(context: Context) : Intent = Intent(context,LoginActivity::class.java)

    }
    @SuppressLint("ResourceType")
    override fun openLoginDialog(context: Context, loginBackground: View) {
        loginBackground.visibility =View.VISIBLE
        loginBackground.startAnimation(AnimationUtils.loadAnimation(context, R.anim.fadein))

        val askPhoneDialog= AskPhoneDialog(context, android.R.style.Theme_Material_Light_NoActionBar_Fullscreen,dataManager!!)


        askPhoneDialog.setOnDismissListener {
            loginBackground.startAnimation(AnimationUtils.loadAnimation(context, R.anim.fadeout))

            loginBackground.visibility = View.GONE

            }

        // Register the onClick listener with the implementation above



        askPhoneDialog.setCancelable(true)
        askPhoneDialog.show()



    }



    lateinit var dataManager:DataManager
    lateinit var mLoginPresenter:LoginPresenter<LoginMvpView>
    lateinit var mPermissionUtils: PermissionUtils
    lateinit var permissions: ArrayList<String>

    override fun setUp() {

        dataManager = (application as PerbMemberApplication).getDataManager()
        mLoginPresenter = LoginPresenter(dataManager)
        mLoginPresenter.onAttach(this)


        mPermissionUtils = PermissionUtils(this@LoginActivity)


        // HelperUtils.LoginClassContext=this@LoginActivity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)

        setUp()

    }
}