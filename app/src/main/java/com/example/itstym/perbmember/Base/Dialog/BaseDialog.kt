package com.example.itstym.perbmember.Base.Dialog

import android.app.Dialog
import android.content.Context
import android.view.WindowManager
import com.example.itstym.perbmember.Base.Dialog.View.DialogView
import com.example.itstym.perbmember.DataManager

/**
 * Created by itstym on 6/12/17.
 */


abstract class BaseDialog:Dialog, DialogView{

    constructor(context: Context) : super(context) {}

    lateinit var mContext:Context
    lateinit var mDataManager:DataManager

    constructor(context: Context, themeResId: Int, dataManager: DataManager) : super(context, themeResId) {
        this.mContext=context
        this.mDataManager=dataManager;
    }

    override fun setup(){
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
    }

}