
package com.example.itstym.perbmember.Meal.newmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MealTypeArray {

    @SerializedName("meal_type")
    @Expose
    private String mealType;
    @SerializedName("alarm_time")
    @Expose
    private String alarmTime;
    @SerializedName("food_items")
    @Expose
    private List<String> foodItems = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public MealTypeArray() {
    }

    /**
     * 
     * @param foodItems
     * @param alarmTime
     * @param mealType
     */
    public MealTypeArray(String mealType, String alarmTime, List<String> foodItems) {
        super();
        this.mealType = mealType;
        this.alarmTime = alarmTime;
        this.foodItems = foodItems;
    }

    public String getMealType() {
        return mealType;
    }

    public void setMealType(String mealType) {
        this.mealType = mealType;
    }

    public String getAlarmTime() {
        return alarmTime;
    }

    public void setAlarmTime(String alarmTime) {
        this.alarmTime = alarmTime;
    }

    public List<String> getFoodItems() {
        return foodItems;
    }

    public void setFoodItems(List<String> foodItems) {
        this.foodItems = foodItems;
    }

}
