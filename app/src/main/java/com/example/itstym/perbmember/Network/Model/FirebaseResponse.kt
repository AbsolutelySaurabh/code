package com.example.itstym.perbmember.Network.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by mukul on 1/8/18.
 */
data class FirebaseResponse(

        @SerializedName("status")
        @Expose
        val status:Boolean,

        @SerializedName("message")
        @Expose
        val message:String
)