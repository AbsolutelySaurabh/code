package com.example.itstym.perbmember.Dashboard

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.design.widget.BottomSheetBehavior
import android.util.Log
import android.widget.Toast
import com.example.itstym.perbmember.Base.Activity.BaseActivity
import com.example.itstym.perbmember.Blog.BlogActivity
import com.example.itstym.perbmember.Comment.CommentFragment
import com.example.itstym.perbmember.Dashboard.Presenter.DashboardPresenter
import com.example.itstym.perbmember.Dashboard.View.DashboardActivityView
import com.example.itstym.perbmember.DashboardBottomDialog.DashboardDialog
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.Feed.FeedFragment
import com.example.itstym.perbmember.Meal.MealActivity
import com.example.itstym.perbmember.Network.Model.Blog
import com.example.itstym.perbmember.PerbMemberApplication
import com.example.itstym.perbmember.PersonalDetails.PersonalDetailsActivity
import com.example.itstym.perbmember.Profile.ProfileFragment
import com.example.itstym.perbmember.Profile.SettingFragment
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.TransactionHistory.TransactionHistoryActivity
import com.example.itstym.perbmember.Utils.BottomNavigationViewHelper
import com.example.itstym.perbmember.Utils.HelperUtils
import com.example.itstym.perbmember.ViewWorkout.WorkoutActivity
import com.example.itstym.perbmember.Water.WaterActivity
import com.example.itstym.perbmember.Weight.WeightActivity
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.content_home.*
import kotlinx.android.synthetic.main.dashboard_activity_layout.*

/**
 * Created by itstym on 6/12/17.
 */

class DashboardActivity:BaseActivity(), DashboardActivityView, FeedFragment.feedOptions, ProfileFragment.ProfileFragmentListener ,ChangeTitle{
    override fun changeTitleToolbar(title: String) {
        supportActionBar!!.setTitle(title)
    }

    override fun onMealActivity() {

        startActivity(MealActivity.getStartIntent(this@DashboardActivity))

    }

    override fun onWorkoutActivity() {
        startActivity(WorkoutActivity.getStartIntent(this@DashboardActivity))


    }


    override fun onPersonalDetailsActivity() {
        startActivity(PersonalDetailsActivity.getStartIntent(this@DashboardActivity))
    }

    override fun onTransactionHistoryActivity() {
        //open transaction history
        startActivity(TransactionHistoryActivity.getStartIntent(this@DashboardActivity))
    }

    override fun onBlogClicked(blog: Blog) {

        //open blog activity
        if (blog.blog_type=="text"){

            val intent= BlogActivity.getStartIntent(this@DashboardActivity)
            intent.putExtra("blog_title",blog.title)
            intent.putExtra("blog_content",blog.content)
            intent.putExtra("blog_image_url",blog.thumbnail_image)
            startActivity(intent)
        }
    }

    override fun onCommentSectionClicked(blog: Blog) {

        //open comment fragment
        val commentFragment=CommentFragment.newInstance()
        val bundle=Bundle()
        bundle.putInt("blog_id",blog.blog_id)
        commentFragment.arguments=bundle


        supportFragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_left, R.anim.slide_right)
                .replace(R.id.mainFrame, commentFragment, CommentFragment.TAG)
                .addToBackStack("comment")
                .commit()

        ToolbarSetup(6)
    }

    override  fun setUpToolbar() {

    }
    override fun openWorkOutActivity() {

        val intent = WorkoutActivity.getStartIntent(this)
        startActivity(intent)
    }



    override fun openMealActivity() {

        Toast.makeText(this@DashboardActivity, "Meal Section coming soon ", Toast.LENGTH_LONG).show()

        //startActivity(Intent(MealActivity.getStartIntent(this@DashboardActivity)))
    }

    override fun openTrackWaterActivity() {
        startActivity(WaterActivity.getStartIntent(this@DashboardActivity))
    }

    override fun openWeightActivity() {
        startActivity(WeightActivity.getStartIntent(this@DashboardActivity))
    }

    override fun showHomeFragment() {
        supportActionBar!!.title = "Home"

        Toast.makeText(this@DashboardActivity,"Home",Toast.LENGTH_LONG).show()

        supportFragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.fadein, 0)
                .replace(R.id.mainFrame, FeedFragment.newInstance(), FeedFragment.TAG)
                .commit()
    }

    override fun showChatFragment() {
        HelperUtils.showSnackBar("Chat Feature Comming Soon",this@DashboardActivity)
    }

    lateinit var dashboardBottomSheetBehaviour:BottomSheetBehavior<ConstraintLayout>
    lateinit var dashBoardDailog:DashboardDialog




    override fun showDashBoardBottomDialog() {

        dashBoardDailog = DashboardDialog(this@DashboardActivity,R.style.ThemeDialog)
        dashBoardDailog.show()

        //val bottomSheetView= bottomSheet
        //Log.e("bottom", bottomSheetView.toString())



//        val bottomSheetView= findViewById<ConstraintLayout>(R.id.bottomSheet) as ConstraintLayout
//        //val dailogBackground = findViewById<View>(R.id.dailogBackground) as View
//
//        dashboardBottomSheetBehaviour = BottomSheetBehavior.from(bottomSheetView)
//        dashboardBottomSheetBehaviour.isHideable=true
//        dashboardBottomSheetBehaviour.peekHeight=0
//
//        if (dashboardBottomSheetBehaviour.state!=BottomSheetBehavior.STATE_EXPANDED){
//
//            dashboardBottomSheetBehaviour.state=BottomSheetBehavior.STATE_EXPANDED
//
//            fabAdd.setImageDrawable(ContextCompat.getDrawable(this@DashboardActivity,R.drawable.cross_red))
//
//            bottomSheetRecyclerview.layoutManager= LinearLayoutManager(this@DashboardActivity)
//            bottomSheetRecyclerview.adapter= BottomSheetDialogAdaptor(this@DashboardActivity, mDashboardPresenter.getBottomSheetData(),  object :BottomSheetDialogAdaptor.BottomSheetListener{
//                override fun onBottomItemClick(position: Int) {
//
//                    Log.e("Item clicked ",position.toString())
//
//                    when(position){
//
//                        0 -> openWorkOutActivity()
//
//                        1 -> openMealActivity()
//
//                        2 -> openTrackWaterActivity()
//
//                        3 -> openWeightActivity()
//                    }
//                }
//
//
//            })
//
//        }else{
//            Log.e("Close","Bottom SHeet")
//
//            fabAdd.setImageDrawable(ContextCompat.getDrawable(this@DashboardActivity,R.drawable.red_plus))
//            dashboardBottomSheetBehaviour.state=BottomSheetBehavior.STATE_HIDDEN
//        }

    }

    override fun showProfileFragment() {

        supportActionBar!!.title = "Profile"

        Toast.makeText(this@DashboardActivity,"showProfileFragment",Toast.LENGTH_LONG).show()
        //log out
       /* dataManager.setLoggedOut()
        startActivity(Intent(LoginActivity.getStartIntent(this@DashboardActivity)))
        finish()*/

        supportFragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_left, R.anim.slide_right)
                .replace(R.id.mainFrame, ProfileFragment.newInstance(), ProfileFragment.TAG)
                .addToBackStack("ProfileFragment")
                .commit()

    }

    override fun showSettingFragment() {

        supportActionBar!!.title = "Settings"
        supportFragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.fadein, 0)
                .replace(R.id.mainFrame, SettingFragment.newInstance(), FeedFragment.TAG)
                .commit()

//        Toast.makeText(this@DashboardActivity,"showSettingFragment",Toast.LENGTH_LONG).show()
//        dataManager.setLoggedOut()
//        startActivity(Intent(LoginActivity.getStartIntent(this@DashboardActivity)))
//        finish()
    }


    override fun ToolbarSetup(fragmentTag: Int) {

        setSupportActionBar(toolbar)


        when(fragmentTag){

            1 -> supportActionBar!!.title= getString(R.string.home)

            2 -> supportActionBar!!.title=getString(R.string.chats)


            4 -> supportActionBar!!.title=getString(R.string.profile)

            5 -> supportActionBar!!.title=getString(R.string.settings)

            6 -> supportActionBar!!.title="Comments"
        }

    }


    lateinit var dataManager:DataManager
    private lateinit var mDashboardPresenter:DashboardPresenter<DashboardActivityView>


    override fun setUp() {

        ToolbarSetup(1)

        dataManager = (application as PerbMemberApplication).getDataManager()
        mDashboardPresenter = DashboardPresenter(dataManager)
        mDashboardPresenter.onAttach(this)

        Log.e("Firebase Token",FirebaseInstanceId.getInstance().getToken()!!)

        mDashboardPresenter.sendFirebaseToken(this@DashboardActivity,FirebaseInstanceId.getInstance().getToken()!!)






        setUpBottomNavigationView()

        mDashboardPresenter.onHomeClicked()

    }

    private fun setUpBottomNavigationView() {

        BottomNavigationViewHelper.disableShiftMode(bottomNavigation)
        bottomNavigation.setOnNavigationItemSelectedListener(mDashboardPresenter.bottomNavigationListener)
    }


    companion object {
        fun getStartIntent(context: Context) : Intent = Intent(context,DashboardActivity::class.java)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dashboard_activity_layout)

        setUp()



    }

    override fun onResume() {
        supportActionBar!!.setTitle("Home")
        super.onResume()
    }


}