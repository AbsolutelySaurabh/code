package com.example.itstym.perbmember.ViewMeal.Presenter

import android.content.Context
import android.support.v7.widget.RecyclerView
import com.example.itstym.perbmember.Base.Activity.Presenter.MvpPresenter
import com.example.itstym.perbmember.ViewMeal.View.MealView1

/**
 * Created by itstym on 12/12/17.
 */



interface MealMvpPresenter1<in V: MealView1> : MvpPresenter<V> {


    fun getAllMeals(context: Context, dietPlanRecyclerView: RecyclerView, dietFoodItemRecyclerView: RecyclerView, i: Int)

    fun setTodayDay()



    fun getDays():ArrayList<String>

    fun upDateDietPlanView(day:Int, context: Context)

}