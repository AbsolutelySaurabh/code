package com.example.itstym.perbmember.Feed.View

import com.example.itstym.perbmember.Base.Activity.View.MvpView
import com.example.itstym.perbmember.Network.Model.Blog

/**
 * Created by itstym on 8/12/17.
 */


interface FeedView : MvpView{

    fun openCommentFragment(blog: Blog)
    fun openBlogTextActivity(blog: Blog)
}
