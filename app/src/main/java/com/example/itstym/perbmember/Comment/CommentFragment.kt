package com.example.itstym.perbmember.Comment

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.itstym.perbmember.Base.Fragment.BaseFragment
import com.example.itstym.perbmember.Comment.Adapter.CommentAdaptor
import com.example.itstym.perbmember.Comment.Presenter.CommentPresenter
import com.example.itstym.perbmember.Comment.View.CommentView
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.Network.Model.Comment
import com.example.itstym.perbmember.R
import com.example.itstym.perbmember.SharedPrefsHelper
import kotlinx.android.synthetic.main.fragment_comment.*

/**
 * Created by itstym on 8/12/17.
 */

class CommentFragment:BaseFragment(),CommentView{


    override fun onResume() {
        super.onResume()

        sendButton.setOnClickListener {
            //send comment
            mCommentPresenter.sendComment(dataManager.getMemberId(),arguments!!.getInt("blog_id"),writeComment.text.toString(), context!!)
            writeComment.setText("")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_comment, container, false)
        setup(view)

        return view
    }


    companion object {

        fun newInstance(): CommentFragment {
            val args= Bundle()
            val commentFragment: CommentFragment = CommentFragment();
            commentFragment.arguments=args
            return commentFragment
        }

        val TAG="CommentFragment"

    }
    lateinit var mCommentPresenter:CommentPresenter<CommentView>
    lateinit var dataManager:DataManager

    override fun setup(view: View?) {


        val bundle=arguments


        val toolbar = activity!!.findViewById<View>(R.id.toolbar) as Toolbar
        toolbar.setTitle("Comments")


        dataManager= DataManager(SharedPrefsHelper(context!!))

        mCommentPresenter = CommentPresenter(dataManager)
        mCommentPresenter.onAttach( this)

        val demoCommentData:ArrayList<Comment> = ArrayList()
            demoCommentData.add(Comment("","",""))
            demoCommentData.add(Comment("","",""))
        val commentRecylerView=view!!.findViewById<View>(R.id.commentRecyclerView) as RecyclerView
        commentRecylerView.layoutManager= LinearLayoutManager(context)
        commentRecylerView.adapter = CommentAdaptor(context!!,demoCommentData,true)

        mCommentPresenter.getAllPreviousComment(commentRecylerView, context!!,arguments?.getInt("blog_id"))
    }



}