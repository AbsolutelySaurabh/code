package com.example.itstym.perbmember.Meal.View

import com.example.itstym.perbmember.Base.Activity.View.MvpView
import com.example.itstym.perbmember.Meal.Model.MealViewPlan
import com.example.itstym.perbmember.Meal.newmodel.SendMealModel

/**
 * Created by itstym on 12/12/17.
 */


interface MealView : MvpView {

    fun highLightTodayDay(weekDay:Int)

    fun addMemberDietPlan(selectedDietPlanGlobalObject: SendMealModel?)
    //fun addCustomMeal()

    fun showConfirmDialog(selectedDietPlanGlobalObject: MealViewPlan?)

    fun  showToast(msg:String)
}