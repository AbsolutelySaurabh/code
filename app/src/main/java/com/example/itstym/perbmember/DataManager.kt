package com.example.itstym.perbmember

import android.util.Log

/**
 * Created by itstym on 6/12/17.
 */


class DataManager {

    var mSharedPrefHelper:SharedPrefsHelper

    constructor(sharedPrefsHelper: SharedPrefsHelper){
        Log.v("Data Manager ","constructor called")
        mSharedPrefHelper=sharedPrefsHelper;
    }

    fun clear(){
        mSharedPrefHelper.clear()
    }

    fun saveEmailId(email:String){
        mSharedPrefHelper.putEmail(email)
    }

    fun getEmailId():String{
        return mSharedPrefHelper.getEmail()
    }

    fun setLoggedIn(){
        mSharedPrefHelper.setLoggedInMode(true)
    }

    fun getLoggedInMode(): Boolean? {
        return mSharedPrefHelper.getLoggedInMode()
    }

    fun saveMemberId(id:Int){
        mSharedPrefHelper.putMemberId(id)
    }

    fun getMemberId():Int{
        return mSharedPrefHelper.getMemberId()
    }

    fun saveMemberName(name:String){
        mSharedPrefHelper.putMemberName(name)
    }

    fun getMemberName():String{
        return mSharedPrefHelper.getMembername()
    }


    fun saveMemberProfilePic(profile_pic:String){
        mSharedPrefHelper.putMemberProfilePic(profile_pic)
    }

    fun getMemberProfilePic():String{
        return mSharedPrefHelper.getMemberProfilePic()
    }

    fun setLoggedOut():Boolean{
        mSharedPrefHelper.setLogOut()
        return true
    }

    fun saveMemberPhone(phone:String){
        mSharedPrefHelper.putMemberPhone(phone)

    }

    fun getMemberPhone():String{
        return mSharedPrefHelper.getMemberPhone()
    }


    fun saveMemberGender(gender:String){
        mSharedPrefHelper.putMemberGender(gender)

    }

    fun getMemberGender():String{
        return mSharedPrefHelper.getMemeberGender()
    }


    fun saveMemberDOj(doj:String){
        mSharedPrefHelper.putDOJ(doj)

    }

    fun getMemberDoj():String{
        return mSharedPrefHelper.getDOJ()
    }


    fun saveMemberAddress(address:String){
        mSharedPrefHelper.putMemberAddress(address)

    }

    fun getMemberAddress():String{
        return mSharedPrefHelper.getMemberAddress()
    }



}