package com.example.itstym.perbmember.Network.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by itstym on 6/12/17.
 */

data class Workout(

        @SerializedName("exercise_id")
        @Expose
        val exercise_id:Int,

        @SerializedName("exercise_name")
        @Expose
        val exercise_name:String,


        @SerializedName("exercise_url")
        @Expose
        val exercise_url:String,

        @SerializedName("body_part_id")
        @Expose
        val body_part_id:Int,

        @SerializedName("body_part_name")
        @Expose
        val body_part_name:String,

        @SerializedName("body_part_url")
        @Expose
        val body_part_url:String,

        @SerializedName("selected")
        @Expose
        val selected:Boolean,

        @SerializedName("days")
        @Expose
        val days:ArrayList<Int>

        )