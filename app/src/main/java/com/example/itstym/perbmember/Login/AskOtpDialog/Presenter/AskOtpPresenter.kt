package com.example.itstym.perbmember.Login.AskOtpDialog.Presenter

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.View
import com.example.itstym.perbmember.Base.Dialog.Presenter.DialogPresenter
import com.example.itstym.perbmember.Dashboard.DashboardActivity
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.Login.AskOtpDialog.View.AskOtpView
import com.example.itstym.perbmember.Network.Model.CommonApiResponse
import com.example.itstym.perbmember.Network.RetrofitHelper.ApiUtils
import com.example.itstym.perbmember.SharedPrefsHelper
import com.example.itstym.perbmember.Utils.HelperUtils
import me.zhanghai.android.materialprogressbar.MaterialProgressBar
import retrofit2.Call
import retrofit2.Response

/**
 * Created by itstym on 6/12/17.
 */

class AskOtpPresenter<V : AskOtpView> : DialogPresenter<V>(), AskOtpMvpPresenter<V> {

    override fun verifyOtp(otp: Int, userPhoneNo: String, context: Context, progressBarOTP: MaterialProgressBar) {


       // val alertDialog = HelperUtils.showDataSendingDialog(context, "Verifying OTP... ")

        val mApiService = ApiUtils.getAPIService()

        mApiService.verifyOTp(userPhoneNo,otp = otp).enqueue(object : retrofit2.Callback<CommonApiResponse> {
            override fun onFailure(call: Call<CommonApiResponse>?, t: Throwable?) {

               // HelperUtils.closeDialog(alertDialog)
                HelperUtils.showSnackBar("Network Issue", context as Activity)
            }

            override fun onResponse(call: Call<CommonApiResponse>?, response: Response<CommonApiResponse>?) {

                //HelperUtils.closeDialog(alertDialog)

                if (response!!.isSuccessful) {

                    Log.e(" OTP Response body ",response.body()!!.status.toString())

                    if(response.body()!!.status){
                        //otp match go to main activity

                        val dataManager: DataManager = DataManager(sharedPrefsHelper = SharedPrefsHelper(context))

                       Log.e("member_id",HelperUtils.loggingButNotVerifiedMemberDetails.member_id.toString())
                       Log.e("name ",HelperUtils.loggingButNotVerifiedMemberDetails.name)
                       Log.e("Profile pic ",HelperUtils.loggingButNotVerifiedMemberDetails.profile_pic_url)
                        Log.e("Email ",HelperUtils.loggingButNotVerifiedMemberDetails.email)
                       Log.e("Address",HelperUtils.loggingButNotVerifiedMemberDetails.fullAddress)
                       Log.e("Gender",HelperUtils.loggingButNotVerifiedMemberDetails.gender_id.toString())
                       Log.e("Doj",HelperUtils.loggingButNotVerifiedMemberDetails.dateOfJoining)
                       Log.e("Member phone",HelperUtils.loggingButNotVerifiedMemberDetails.phone)


                        dataManager.saveMemberId(HelperUtils.loggingButNotVerifiedMemberDetails.member_id)
                        dataManager.saveMemberName(HelperUtils.loggingButNotVerifiedMemberDetails.name)
                        dataManager.saveMemberProfilePic(HelperUtils.loggingButNotVerifiedMemberDetails.profile_pic_url)
                        dataManager.saveEmailId(HelperUtils.loggingButNotVerifiedMemberDetails.email)
                        dataManager.saveMemberAddress(HelperUtils.loggingButNotVerifiedMemberDetails.fullAddress)
                        dataManager.saveMemberGender(HelperUtils.getGenderById(HelperUtils.loggingButNotVerifiedMemberDetails.gender_id))
                        dataManager.saveMemberDOj(HelperUtils.loggingButNotVerifiedMemberDetails.dateOfJoining)
                        dataManager.saveMemberPhone(HelperUtils.loggingButNotVerifiedMemberDetails.phone)
                        progressBarOTP.visibility = View.GONE

                        context.startActivity(DashboardActivity.getStartIntent(context))
                        (context as Activity).finish()

                    }else{
                        progressBarOTP.visibility = View.GONE
                        HelperUtils.showSnackBar("OTP NOT MATCHED", context as Activity)
                    }


                } else {
                    HelperUtils.showSnackBar("Network issue while verifying otp", context as Activity)
                }
            }
        })
    }
}