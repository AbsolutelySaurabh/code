package com.example.itstym.perbmember.Weight.View

import com.example.itstym.perbmember.Base.Activity.View.MvpView

/**
 * Created by itstym on 11/12/17.
 */


interface WeightView:MvpView{

    fun weightShown(weight: Int)
}