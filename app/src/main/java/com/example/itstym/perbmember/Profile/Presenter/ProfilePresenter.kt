package com.example.itstym.perbmember.Profile.Presenter

import android.app.Activity
import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.example.itstym.perbmember.Base.Activity.Presenter.BasePresenter
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.Feed.Adapter.FeedAdaptor
import com.example.itstym.perbmember.Feed.Presenter.FeedMvpPresenter
import com.example.itstym.perbmember.Feed.View.FeedView
import com.example.itstym.perbmember.Network.Model.Blog
import com.example.itstym.perbmember.Network.Model.BlogsResponse
import com.example.itstym.perbmember.Network.RetrofitHelper.ApiUtils
import com.example.itstym.perbmember.Profile.View.ProfileView
import com.example.itstym.perbmember.Utils.HelperUtils
import retrofit2.Call
import retrofit2.Response

/**
 * Created by itstym on 11/12/17.
 */


class ProfilePresenter<V : ProfileView>(dataManager: DataManager) : BasePresenter<V>(dataManager), ProfileMvpPresenter<V> {



}