package com.example.itstym.perbmember.Network.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by itstym on 13/12/17.
 */

data class Diet(

        @SerializedName("diet_plan_id")
        @Expose
        val dietPlanId:Int,

        @SerializedName("diet_name")
        @Expose
        val dietName:String

)