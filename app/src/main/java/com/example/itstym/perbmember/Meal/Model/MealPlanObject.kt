package com.example.itstym.perbmember.Meal.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by itstym on 11/1/18.
 */



data class MealPlanObject(

        val mealId:Int,

        val mealType:String,
        val alarmTime:String
)