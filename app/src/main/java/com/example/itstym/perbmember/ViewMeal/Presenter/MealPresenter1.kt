//package com.example.itstym.perbmember.ViewMeal.Presenter
//
//import android.app.Activity
//import android.content.Context
//import android.support.v7.widget.LinearLayoutManager
//import android.support.v7.widget.RecyclerView
//import android.util.Log
//import com.example.itstym.perbmember.Base.Activity.Presenter.BasePresenter
//import com.example.itstym.perbmember.DataManager
//import com.example.itstym.perbmember.Meal.Adaptor.FoodItemAdaptor
//import com.example.itstym.perbmember.Meal.Model.AddMeal
//import com.example.itstym.perbmember.Network.Model.Diet
//import com.example.itstym.perbmember.Network.Model.Meal
//import com.example.itstym.perbmember.Network.Model.MealResponse
//import com.example.itstym.perbmember.Network.RetrofitHelper.ApiUtils
//import com.example.itstym.perbmember.Utils.HelperUtils
//import com.example.itstym.perbmember.ViewMeal.Adaptor.DietPlanAdaptor
//import com.example.itstym.perbmember.ViewMeal.View.MealView1
//import retrofit2.Call
//import retrofit2.Response
//import java.util.*
//import kotlin.collections.ArrayList
//import kotlin.collections.HashMap
//
///**
// * Created by itstym on 12/12/17.
// */
//
//
//class MealPresenter1<V : MealView1>(dataManager: DataManager) : BasePresenter<V>(dataManager), MealMvpPresenter1<V> {
//
//    var selectedDietPlanGlobalObject:AddMeal?=null
//    var selectedMemberMealDate:Int?=null
//
//    var firstTime:Boolean = false
//    override fun getAllMeals(context: Context, dietPlanRecyclerView: RecyclerView, dietFoodItemRecyclerView: RecyclerView, selectedDay: Int) {
//        firstTime = true
//
//       // val alertDialog = HelperUtils.showDataSendingDialog(context, "Fetching Workout Details... ")
//
//        val mApiService = ApiUtils.getAPIService()
//
//        mApiService.getAllMeals(0,dataManager.getMemberId()).enqueue(object : retrofit2.Callback<MealResponse> {
//            override fun onFailure(call: Call<MealResponse>?, t: Throwable?) {
//                Log.e("faliure ",t?.localizedMessage)
//              //  HelperUtils.closeDialog(alertDialog)
//                HelperUtils.showSnackBar("Network Issue", context as Activity)
//            }
//
//            override fun onResponse(call: Call<MealResponse>?, response: Response<MealResponse>?) {
//
//                //HelperUtils.closeDialog(alertDialog)
//
//                Log.e("Response ",response!!.body()!!.dietPlanArrayList.toString())
//
//                if (response.isSuccessful) {
//
//                    if(response.body()!!.status){
//
//                        val arrangeTheData=datArrangeMentInProgress(response.body()!!.dietPlanArrayList)
//                        //call  diet plan
//                        dietPlanRecyclerView.layoutManager=LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
//
//                        //get diet plan data
//                        val tempDietPlanData:ArrayList<Diet> = getDietPlanData(arrangeTheData)
//
//                        //get the selected diet plan by default it is first one
//
//
//                        selectedMemberMealDate=HelperUtils.getTodayDay()
//
//
//
//
//
//                        var selectedDietPlanforRecyler:String = ""
//
//                        //get food item
//                        for(i in response.body()!!.dietPlanArrayList){
//
//
//
//
//                                    if (i.days==selectedDay){
//                                    Log.e("selected Day status", i.days.toString() + " dbwygu" +i.dietPlanName)
//                                        selectedDietPlanforRecyler = i.dietPlanName
//                                }
//
//
//
//
//                        }
//
//
//                        UpdateRecyler(context, tempDietPlanData, true, dietPlanRecyclerView, 0,false,dietFoodItemRecyclerView,arrangeTheData, selectedDietPlanforRecyler,selectedDay)
//
//
//                    }
//
//                } else {
//                    HelperUtils.showSnackBar("Network issue while verifying otp", context as Activity)
//                }
//            }
//        })
//    }
//
//    private fun UpdateRecyler(context: Context, tempDietPlanData: ArrayList<Diet>, b: Boolean, dietPlanRecyclerView: RecyclerView, position1: Int, b1: Boolean, dietFoodItemRecyclerView: RecyclerView, arrangeTheData: HashMap<Diet, ArrayList<Meal>>, i: String, selectedDay: Int) {
//
//        dietPlanRecyclerView.adapter= DietPlanAdaptor(context, tempDietPlanData,object : DietPlanAdaptor.DietPlanSelection{
//            override fun onDietPlanSelection(dietPlan:Diet,position:Int,selected:Boolean) {
//
//                firstTime = false
//
//                Log.e("DietPlan",dietPlan.dietName  + "giyehduwh")
//
//             //   UpdateRecyler(context, tempDietPlanData, false, dietPlanRecyclerView, position, selected, dietFoodItemRecyclerView, arrangeTheData, i, selectedDay)
//
//                val selectedDietPlan=tempDietPlanData[position]
//                val tempFoodPlanData= getFoodData(arrangeTheData,selectedDietPlan)
//                var todayNumber:Int = 0
//                var days:ArrayList<Int> = ArrayList()
//
//                dietFoodItemRecyclerView.adapter= FoodItemAdaptor(context, tempFoodPlanData)
//
//
//
//                 days.add(selectedDay)
//
//
//                //do someting awesom
//
//                selectedDietPlanGlobalObject= AddMeal(dataManager.getMemberId(),0,dietPlan.dietPlanId,days)
//            }
//
//        },abc = HelperUtils.getTodayDay(), firstTime = b, pos = position1, selected = b1, SelectedditePlan = i)
//    }
//
//    private fun getFoodData(arrangeTheData: HashMap<Diet, ArrayList<Meal>>, selectedDietPlan: Diet):  ArrayList<Meal> {
//
//        return arrangeTheData.get(selectedDietPlan)!!
//    }
//
//    private fun getDietPlanData(arrangeTheData: HashMap<Diet, ArrayList<Meal>>): ArrayList<Diet> {
//
//        val tempDietPlanData:ArrayList<Diet> = ArrayList()
//
//
//        val dietPlanDataAllKeys=arrangeTheData.keys
//        val dietPlanDatas=dietPlanDataAllKeys.iterator()
//
//        while (dietPlanDatas.hasNext()){
//            tempDietPlanData.add(dietPlanDatas.next())
//        }
//
//        return tempDietPlanData;
//
//    }
//
//    private fun datArrangeMentInProgress(dietPlanArrayList: ArrayList<Meal>): HashMap<Diet,ArrayList<Meal>> {
//
//
//        val completeDietPlanObject:HashMap<Diet,ArrayList<Meal>> = HashMap()
//
//        for (i in  0 until dietPlanArrayList.size){
//
//                val tempDiet= Diet(dietPlanArrayList[i].dietPlanId,dietName = dietPlanArrayList[i].dietPlanName)
//
//                if (completeDietPlanObject.containsKey(tempDiet)){
//                    val oldKeyData= completeDietPlanObject[tempDiet]
//                    oldKeyData?.add(dietPlanArrayList[i])
//                    completeDietPlanObject.put(tempDiet,oldKeyData!!)
//                }else{
//                    //no old data found, new key
//                    val tempData=ArrayList<Meal>()
//                    tempData.add(dietPlanArrayList[i])
//                    completeDietPlanObject.put(tempDiet,tempData)
//                }
//        }
//
//        return completeDietPlanObject
//
//    }
//
//
//    override fun setTodayDay() {
//
//        val todayCalender=Calendar.getInstance()
//        mvpView?.highLightTodayDay( todayCalender.get(Calendar.DAY_OF_WEEK))
//    }
//
//
//
//
//
//    override fun getDays(): ArrayList<String> {
//        val dayArrayList=ArrayList<String>()
//
//        dayArrayList.add("Mon")
//        dayArrayList.add("Tue")
//        dayArrayList.add("Wed")
//        dayArrayList.add("Thu")
//        dayArrayList.add("Fri")
//        dayArrayList.add("Sat")
//        dayArrayList.add("Sun")
//
//        return dayArrayList
//    }
//
//    override fun upDateDietPlanView(day: Int, context: Context) {
//
//    }
//
//
//}
