package com.example.itstym.perbmember.Workout.Presenter

import android.app.Activity
import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.design.widget.BottomSheetDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.example.itstym.perbmember.Base.Activity.Presenter.BasePresenter
import com.example.itstym.perbmember.DataManager
import com.example.itstym.perbmember.Network.Model.CommonApiResponse
import com.example.itstym.perbmember.Network.Model.Workout
import com.example.itstym.perbmember.Network.Model.WorkoutResponse
import com.example.itstym.perbmember.Network.RetrofitHelper.ApiUtils
import com.example.itstym.perbmember.Utils.HelperUtils
import com.example.itstym.perbmember.Workout.Adaptor.BodyPartAdapter
import com.example.itstym.perbmember.Workout.Adaptor.ExerciseAdapter
import com.example.itstym.perbmember.Workout.Model.BodyPart
import com.example.itstym.perbmember.Workout.Model.Exercise
import com.example.itstym.perbmember.Workout.View.WorkoutView
import me.zhanghai.android.materialprogressbar.MaterialProgressBar
import retrofit2.Call
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


/**
 * Created by itstym on 6/12/17.
 */

class WorkoutPresenter<V : WorkoutView>(dataManager: DataManager) : BasePresenter<V>(dataManager), WorkoutMvpPresenter<V> {


    lateinit var exerciseRecycleAdapter: ExerciseAdapter
    lateinit var bodyPartRecyclerAdaper: BodyPartAdapter
    lateinit var allSortedWorkoutData: HashMap<Int, HashMap<BodyPart, ArrayList<Exercise>>>
    lateinit var exerciseRecyclerViewGlobal: RecyclerView
    lateinit var bodyPartRecyclerViewGlobalObject: RecyclerView
    lateinit var errorEmpty: TextView


    lateinit var selectedExercise: ArrayList<Exercise>
    lateinit var selectedDays: ArrayList<Int>

    public companion object {
        var firstTime1:Boolean = true
        var selectedBodyPartid:Int=-1
        var isBodyBodyPartSelected:Boolean= false


         fun upadteStaticValuesforRecyclerView(firstTime:Boolean,selectedBodypartId: Int,isBodyPartSelected1: Boolean){
            WorkoutPresenter.firstTime1 = firstTime
            WorkoutPresenter.selectedBodyPartid = selectedBodypartId
            WorkoutPresenter.isBodyBodyPartSelected = isBodyPartSelected1

        }

    }



    override fun getAllWorkout(memberId: Int, context: Context, bodyPartRecyclerView: RecyclerView, exerciseRecyclerView: RecyclerView, errorEmpty: TextView, selectedDay: Int) {

        //val alertDialog = HelperUtils.showDataSendingDialog(context, "Fetching Workout Details... ")
        exerciseRecyclerViewGlobal = exerciseRecyclerView
        bodyPartRecyclerViewGlobalObject = bodyPartRecyclerView
        this.errorEmpty = errorEmpty

        val mApiService = ApiUtils.getAPIService()

        Log.e("member id", memberId.toString())

        mApiService.getWorkout(memberId).enqueue(object : retrofit2.Callback<WorkoutResponse> {
            override fun onFailure(call: Call<WorkoutResponse>?, t: Throwable?) {

                // HelperUtils.closeDialog(alertDialog)
                HelperUtils.showSnackBar("Network Issue", context as Activity)
            }

            override fun onResponse(call: Call<WorkoutResponse>?, response: Response<WorkoutResponse>?) {

                // HelperUtils.closeDialog(alertDialog)

                selectedExercise = ArrayList()
                selectedDays = ArrayList()
                selectedDays.clear()
                selectedExercise.clear()

                Log.e("Response ", response!!.body()!!.workout.toString())

                if (response.isSuccessful) {

                    if (response.body()!!.status) {

                        for (i in response.body()!!.workout) {

                            Log.d("Workout", i.exercise_name + " " + i.selected)
                        }

                        //sort the data
                        allSortedWorkoutData = separateData(response.body()!!.workout)

                        //get today day
                        val todayCalender = Calendar.getInstance()
                        val today = todayCalender.get(Calendar.DAY_OF_WEEK) - 1;

                        //get selected days bodypart-exercise combination
                        val getSelectedDayData = allSortedWorkoutData.get(today)

                        //now get body part data
                        val getSelectedDayBodyPartData = getSelectedDayBodyPartData(todayCalender.get(Calendar.DAY_OF_WEEK) - 1)

                        for(i in getSelectedDayBodyPartData){

                            Log.e("Data",i.selected_days.toString() + i.body_part_name )
                        }

                        //now get all exercise

                        val getSelectedDayExerciseData = getExerciseData(today, null)

                        //setting the body part recyclerview
                        bodyPartRecyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                        bodyPartRecyclerViewGlobalObject = bodyPartRecyclerView


                        for(i in getSelectedDayBodyPartData){

                            Log.e(i.body_part_name,i.body_part_id.toString())

                        }


                        WorkoutPresenter.upadteStaticValuesforRecyclerView(true,-1,false)
                        updateRecylerViews(context,getSelectedDayBodyPartData,today,true,getTodayBodypartid(today),bodyPartRecyclerView,getSelectedDayData,selectedDay,exerciseRecyclerView,-1,false)


                        errorEmpty.visibility = View.VISIBLE

                        //setting the exercise recyclerview
//                        exerciseRecyclerView.layoutManager=LinearLayoutManager(context)
//                        exerciseRecyclerViewGlobal=exerciseRecyclerView
//                        exerciseRecycleAdapter=ExerciseAdapter(context,getSelectedDayExerciseData!!, object: ExerciseAdapter.ExerciseUpdated{
//                            override fun actionOnExercise(exercise: Exercise, isSelected: Boolean) {
//
//                                selectedExercise.add(exercise)
//                                selectedDays.add(todayCalender.get(Calendar.DAY_OF_WEEK)-1)
//
//                            }
//
//
//                        },selectedDays = today )
//
//                        exerciseRecyclerView.adapter=exerciseRecycleAdapter

                    } else {
                        HelperUtils.showSnackBar("OTP NOT MATCHED", context as Activity)
                    }


                } else {
                    HelperUtils.showSnackBar("Network issue while verifying otp", context as Activity)
                }
            }
        })
    }

    private fun updateRecylerViews(context: Context, selectedDayBodyPartData: ArrayList<BodyPart>, today: Int, b: Boolean, todayBodypartid: Int, bodyPartRecyclerView: RecyclerView, selectedDayData: java.util.HashMap<BodyPart, java.util.ArrayList<Exercise>>?, selectedDay: Int, exerciseRecyclerView: RecyclerView, selectedBodypartId: Int, isBodyPartSelected: Boolean) {


        bodyPartRecyclerView.adapter = BodyPartAdapter(context, selectedDayBodyPartData, object : BodyPartAdapter.BodyPartSelection {
            override fun onbodyPartClicked(firstTime:Boolean,bodyPart: BodyPart, selected: Boolean) {

                //if(!firstTime)
              //  updateRecylerViews(context,selectedDayBodyPartData,today,false,getTodayBodypartid(today),bodyPartRecyclerView,selectedDayData,selectedDay,exerciseRecyclerView,bodyPart.body_part_id,selected)

                Log.e("Data referesh", "yes")
                //refreash exercise data

                //  Log.e("Data is",getSelectedDayData!!.get(bodyPart)!!.get)

                for (i in selectedDayData!!.get(bodyPart)!!) {

                    Log.d("TAG Mukul", i.exercise_name)

                }
                errorEmpty.visibility = View.GONE

                var deleteIndex=0

                if (selected) {
                    selectedExercise.clear()

                    exerciseRecycleAdapter = ExerciseAdapter(context, selectedDayData.get(bodyPart)!!, object : ExerciseAdapter.ExerciseUpdated {
                        override fun actionOnExercise(exercise: Exercise, isSelected: Boolean) {

                            Log.e("is Selected", exercise.exercise_name+ isSelected.toString())

                            if (isSelected == false) {

                                selectedExercise.add(exercise)

                            } else {
                                selectedExercise.remove(exercise)

                            }


                            selectedDays.add(selectedDay)

                        }


                    }, selectedDay)
                    exerciseRecyclerView.adapter = exerciseRecycleAdapter
                    exerciseRecycleAdapter.notifyDataSetChanged()
                } else {
                    errorEmpty.visibility = View.VISIBLE
                    selectedExercise.clear()

                }
            }


        }, today ,true,getTodayBodypartid(selectedDay),b,selectedBodypartId,isBodyPartSelected)



    }

    private fun separateData(workOutDetails: ArrayList<Workout>): HashMap<Int, HashMap<BodyPart, ArrayList<Exercise>>> {


        val workoutHashMap: HashMap<Int, HashMap<BodyPart, ArrayList<Exercise>>> = HashMap()

        //add empty hashmap with day key
        for (i in 1..7) {
            val emptyBodyPartExerciseHashMap: HashMap<BodyPart, ArrayList<Exercise>> = HashMap()
            workoutHashMap.put(i, emptyBodyPartExerciseHashMap)
        }

        val bodyPartExerciseHashMap: HashMap<BodyPart, ArrayList<Exercise>> = HashMap()
        val bodyPartDebug:ArrayList<BodyPart> = ArrayList()


        //make key-value pair of global exercise
        for (i in 0 until workOutDetails.size) {

            var dayForThatUserHaveSelected = ArrayList<Int>()
            // Log.d("TAG 2",workOutDetails[i].);
            if (workOutDetails[i].selected) {
                dayForThatUserHaveSelected = workOutDetails[i].days
            }

            Log.e("days  have selected",dayForThatUserHaveSelected.toString())

            val bodyPart = BodyPart(workOutDetails[i].body_part_id, workOutDetails[i].body_part_name, workOutDetails[i].body_part_url, false, dayForThatUserHaveSelected)
            val exercise = Exercise(workOutDetails[i].exercise_id, workOutDetails[i].exercise_name, workOutDetails[i].exercise_url, workOutDetails[i].body_part_id, false, dayForThatUserHaveSelected)

            //check if body part key already added or not
            Log.e("TAG 2", isBodyPartAlreadyAdded(bodyPartExerciseHashMap, bodyPart).toString() + "\n" +

                    bodyPart.body_part_name)


            if (isBodyPartAlreadyAdded(bodyPartExerciseHashMap, bodyPart)) {

                Log.e("hello", exercise.exercise_name + i.toString())


                //get all exercise


                //val getPreviousBodyPart= getAlreadyAddedBodyPart(bodyPart,bodyPartExerciseHashMap)


                for (i in bodyPartExerciseHashMap) {

                    if (i.key.body_part_id == bodyPart.body_part_id) {
                        //    val alreadyAddedExercise= getalreadyaddedExercise(i.key,bodyPartExerciseHashMap)
                        val alreadyAddedExercise = bodyPartExerciseHashMap.get(i.key)

                        alreadyAddedExercise?.add(exercise)

//                        for(j in alreadyAddedExercise!!){
//
//                            if(j.selected_days.size>0){
//
//                            i.key.selected_days.clear()
//                                for(k in j.selected_days ){
//                                    i.key.selected_days.add(k)
//                                }
//
//                            }
//                        }
                        Log.e("already added","dic")
                        bodyPartExerciseHashMap.put(i.key, alreadyAddedExercise!!)


                    }
                }


            } else {
                //not added
                val exerciseArrayList: ArrayList<Exercise> = ArrayList()
                exerciseArrayList.add(exercise)
                bodyPartExerciseHashMap.put(bodyPart, exerciseArrayList)
            }

        }


        Log.e("Sorted Exercise", bodyPartExerciseHashMap.toString())

        for (i in 1..7) {
            workoutHashMap.put(i, bodyPartExerciseHashMap)
        }

        Log.e("Current workouthasmap", "" + workoutHashMap)


        //now find the selected exercise and add them




        return workoutHashMap
    }



    private fun getAlreadyAddedBodyPart(bodyPart: BodyPart, bodyPartExerciseHashMap: HashMap<BodyPart, ArrayList<Exercise>>): Any? {


        return null
    }

    fun getalreadyaddedExercise(bodyPart: BodyPart, bodyPartExerciseHashMap: HashMap<BodyPart, ArrayList<Exercise>>): ArrayList<Exercise>? {


        for (i in bodyPartExerciseHashMap) {

            if (i.key.body_part_id == bodyPart.body_part_id) {

                return bodyPartExerciseHashMap.get(i.key)
            }
        }



        return ArrayList()
    }

    private fun isBodyPartAlreadyAdded(bodyPartExerciseHashMap: HashMap<BodyPart, ArrayList<Exercise>>, bodyPart: BodyPart): Boolean {

        var isAlreadyAdded = false


        val allKeys = bodyPartExerciseHashMap.keys
        val i = allKeys.iterator()
        while (i.hasNext()) {
            val keyBodyPart = i.next()

            if (keyBodyPart.body_part_id == bodyPart.body_part_id) {
                isAlreadyAdded = true
            }
        }

        return isAlreadyAdded
    }

    override fun workOutSubmitButtonClicked(workoutSubmitButton: Button) {

        if (selectedExercise.size > 0){
            workoutSubmitButton.isEnabled = true
            mvpView?.addWorkouttButtonClicked(selectedExercise,selectedDays)}
        else{
            mvpView?.showToast("Please select any exercise")
            workoutSubmitButton.isEnabled = true
        }
    }

    override fun getDays(): ArrayList<String> {
        val dayArrayList = ArrayList<String>()

        dayArrayList.add("Mon")
        dayArrayList.add("Tue")
        dayArrayList.add("Wed")
        dayArrayList.add("Thu")
        dayArrayList.add("Fri")
        dayArrayList.add("Sat")
        dayArrayList.add("Sun")

        return dayArrayList
    }

    override fun onConfirmButtonClicked(context: Context, distinctArray: java.util.ArrayList<Int>, progressbar: MaterialProgressBar, mBottomSheetDialog: BottomSheetDialog, finalselectedExercise: java.util.ArrayList<Exercise>, confirmButton: ConstraintLayout) {

        //send data to server

      //  val alertDialog = HelperUtils.showDataSendingDialog(context, "Checking Phone number... ")

        val mApiService = ApiUtils.getAPIService()

        val selectedExerciseId: ArrayList<Int> = ArrayList()

        Log.e("Exercise size", finalselectedExercise.size.toString())

        confirmButton.isEnabled = false

      //  (0 until selectedExercise.size).mapTo(selectedExerciseId) { selectedExercise[it].exercise_id }




        //remove duplicate days if any
        val dayUnique = HashSet<Int>()
        dayUnique.addAll(distinctArray)
        distinctArray.clear()
        distinctArray.addAll(dayUnique)
        for(i in finalselectedExercise){
            selectedExerciseId.add(i.exercise_id)
            selectedExerciseId.add(i.exercise_id)



        }
        Log.e("deatils",selectedExerciseId.toString())

        mApiService.addWorkOut(memberId = dataManager.getMemberId(), exerciseDetails = selectedExerciseId, daysDetails = distinctArray)
                .enqueue(object : retrofit2.Callback<CommonApiResponse> {

                    override fun onFailure(call: Call<CommonApiResponse>?, t: Throwable?) {

                        Log.e("Issue ", t?.message)

                       // HelperUtils.closeDialog(alertDialog)
                        progressbar.visibility =View.GONE
                        HelperUtils.showSnackBar("Network Issue", context as Activity)
                    }

                    override fun onResponse(call: Call<CommonApiResponse>?, response: Response<CommonApiResponse>?) {

                       // HelperUtils.closeDialog(alertDialog)

                        progressbar.visibility =View.GONE


                        if (response!!.isSuccessful) {

                            if (response.body()!!.status) {
                                mBottomSheetDialog.dismiss()
                                mvpView?.showConfirmDialog()
                                confirmButton.isEnabled = true


                            } else {
                              //  HelperUtils.showSnackBar("Member Not Exists", context as Activity)
                                Toast.makeText(context as Activity,"Try Again",Toast.LENGTH_SHORT).show()
                                confirmButton.isEnabled = true

                            }
                        } else {
                            confirmButton.isEnabled = true

                            Toast.makeText(context as Activity,"Try Again",Toast.LENGTH_SHORT).show()
                        }

                    }
                })

    }


    override fun setTodayDay() {

        val todayCalender = Calendar.getInstance()
        mvpView?.highLightTodayDay(todayCalender.get(Calendar.DAY_OF_WEEK))
    }

    fun getExerciseData(selectedDays: Int, bodyPart: BodyPart?): ArrayList<Exercise> {


        val getSelectedDayData = allSortedWorkoutData[selectedDays]

        if (selectedDays != 0) {
            //we know only selected days, then either one of the body part is selected or no one is selected
            //if nothing is selected we assume first is selected

            if (bodyPart != null) {
                return getSelectedDayData?.get(bodyPart)!!
            }
            val exerciseData = getSelectedDayData!!.keys.iterator()
            return getSelectedDayData.get(exerciseData.next())!!
        }

        return ArrayList()

        /*else{
            //we know body part
            return getSelectedDayData!!.get(bodyPart)!!
        }*/

    }

    override fun upDateWorkout(selectedDay: Int, context: Context) {

//        this.errorEmpty.visibility = View.VISIBLE
//
//        //update the workout
//
//        bodyPartRecyclerAdaper = BodyPartAdapter(context, getSelectedDayBodyPartData(selectedDay), object : BodyPartAdapter.BodyPartSelection {
//
//
//            override fun onbodyPartClicked(bodyPart: BodyPart, selected: Boolean) {
//                Log.e("Data referesh", "yes")
//                // refresh exercise data
//                errorEmpty.visibility = View.GONE
//
//                if (selected) {
//                    selectedExercise.clear()
//
//                    exerciseRecycleAdapter = ExerciseAdapter(context, getExerciseData(selectedDay, bodyPart), object : ExerciseAdapter.ExerciseUpdated {
//                        override fun actionOnExercise(exercise: Exercise, isSelected: Boolean) {
//                            Log.e("is Selected", exercise.exercise_name)
//
//
//                            selectedExercise.add(exercise)
//                            selectedDays.add(selectedDay)
//
//                        }
//
//
//                    }, selectedDay)
//                    exerciseRecyclerViewGlobal.adapter = exerciseRecycleAdapter
//                    exerciseRecycleAdapter.notifyDataSetChanged()
//                } else {
//                    errorEmpty.visibility = View.VISIBLE
//                    selectedExercise.clear()
//                }
//
//
//            }
//
//
//        }, selectedDay, true, getTodayBodypartid(selectedDay), b, selectedBodypartId, isBodyPartSelected)
//
//
//
//        bodyPartRecyclerViewGlobalObject.adapter = bodyPartRecyclerAdaper
//        bodyPartRecyclerAdaper.notifyDataSetChanged()
//
//        //refresh the exercise recyclerview
//        exerciseRecycleAdapter = ExerciseAdapter(context, getExerciseData(selectedDay, bodyPart = null), object : ExerciseAdapter.ExerciseUpdated {
//            override fun actionOnExercise(exercise: Exercise, isSelected: Boolean) {
//
//
//                selectedExercise.add(exercise)
//                selectedDays.add(selectedDay)
//
//            }
//
//
//        }, selectedDay)
//        exerciseRecyclerViewGlobal.adapter = exerciseRecycleAdapter
//        exerciseRecycleAdapter.notifyDataSetChanged()

        //update the bodypart recycler
    }

    fun getSelectedDayBodyPartData(selectedDay: Int): ArrayList<BodyPart> {

        //get selected days bodypart-exercise combination
        val getSelectedDayData = allSortedWorkoutData[selectedDay]


        //now get body part data

        val getSelectedDayBodyPartData: ArrayList<BodyPart> = ArrayList()

//        for(i in getSelectedDayData!!.keys){
//
//            getSelectedDayBodyPartData.add(i)
//
//
//        }

        val allKeys = getSelectedDayData!!.keys
        val bodyData = allKeys.iterator()
        while (bodyData.hasNext()) {
            val tempBodyPart = bodyData.next()
            tempBodyPart.selected_days
            Log.e("tempBodyData", tempBodyPart.selected_days.toString())



            getSelectedDayBodyPartData.add(tempBodyPart)
        }
      //  Log.e("get selected data",getSelectedDayBodyPartData.toString())


        return getSelectedDayBodyPartData
    }

    private fun getTodayBodypartid(today1:Int):Int{
        Log.e("Today BodyPArt",today1.toString())

        for(i in allSortedWorkoutData[today1]!!){
            for(j in i.value){
                Log.e("Today BodyPArt sele", j.selected_days.toString())

                for(k in j.selected_days!!){
                    if(k==today1){
                        return j.body_part_id
                    }

                }





            }


        }

        return -1
    }
}