package com.example.itstym.perbmember.Splash.View

import com.example.itstym.perbmember.Base.Activity.View.MvpView

/**
 * Created by itstym on 6/12/17.
 */


interface SplashMvpView : MvpView {

    fun openMainActivity()

    fun openLoginActivity()

    fun versionChecked()
}

